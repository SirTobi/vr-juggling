/*
 * 
 */

#ifndef MAINAPP_H
#define MAINAPP_H

#include <OgreSingleton.h>
#include <memory>
#include <vector>
#include "KinectManager.hpp"
#include "rapidxml.hpp"

class AppStateManager;
class KinectManager;

class MainApp : public Ogre::Singleton<MainApp>
{
public:
	MainApp();
	~MainApp();

	void start();

	std::unique_ptr<AppStateManager>& appStateManager() { return mAppStateManager; }
	std::unique_ptr<KinectManager>& kinectManager() { return mKinectManager; }
	
private:
	std::unique_ptr<AppStateManager> mAppStateManager;
	std::unique_ptr<KinectManager> mKinectManager;
};

#endif // MAINAPP_H
