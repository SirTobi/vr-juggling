/*
 * 
 */

#ifndef EXPLOSIONCONTROLLER_H
#define EXPLOSIONCONTROLLER_H

#include <OgreParticleSystem.h>
#include "physics/PhysicBall.hpp"


class ExplosionController: public PhysicBall::Controller
{
public:
	ExplosionController(Ogre::ParticleSystem* particleSys);
	
	void apply(PhysicBall* ball, double dt);
	
private:
	Ogre::ParticleSystem* mSystem;
};

#endif // EXPLOSIONCONTROLLER_H
