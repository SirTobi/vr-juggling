#include "SimpleControllers.hpp"


BallAtGroundController::BallAtGroundController(float ground)
	: ground(ground)
{
}


void BallAtGroundController::apply(PhysicBall* ball, double dt)
{
	if(ball->pos.y < ground)
	{
		ball->explode();
	}
}



BallToHandController::BallToHandController(Vec3* target, Vec3 offset)
	: target(target)
	, offset(offset)
	, newMovement(Vec3::ZERO)
{
}


void BallToHandController::apply(PhysicBall* ball, double dt)
{
	if(target)
		ball->pos = *target;
	ball->pos += offset;
	ball->movement = newMovement;
}
