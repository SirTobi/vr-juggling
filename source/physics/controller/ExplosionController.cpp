/*
 * 
 */

#include <cassert>
#include <OgreParticleEmitter.h>
#include "ExplosionController.hpp"


ExplosionController::ExplosionController(Ogre::ParticleSystem* particleSys)
	: mSystem(particleSys)
{
	assert(mSystem->isAttached());
	
	for(int i = 0; i < mSystem->getNumEmitters(); ++i)
		mSystem->getEmitter(i)->setEnabled(false);
}


void ExplosionController::apply(PhysicBall* ball, double dt)
{
	Ogre::SceneNode* node = mSystem->getParentSceneNode();
	node->setPosition(ball->pos);
	
	for(int i = 0; i < mSystem->getNumEmitters(); ++i)
	{
		mSystem->getEmitter(i)->setEnabled(true);
		mSystem->getEmitter(i)->setDirection(ball->movement);
	}
}
