#ifndef BALLLIFTCONTROLLER_H
#define BALLLIFTCONTROLLER_H

#include "physics/PhysicBall.hpp"

class BallAtGroundController: public PhysicBall::Controller
{
public:
	typedef Ogre::Vector3 Vec3;
public:
	BallAtGroundController(float ground);
	
	void apply(PhysicBall* ball, double dt);
	
	float ground;
};

class BallToHandController: public PhysicBall::Controller
{
public:
	typedef Ogre::Vector3 Vec3;
public:
	BallToHandController(Vec3* target, Vec3 offset);
	
	void apply(PhysicBall* ball, double dt);
	
	Vec3 offset;
	Vec3* target;
	Vec3 newMovement;
};

#endif // BALLLIFTCONTROLLER_H
