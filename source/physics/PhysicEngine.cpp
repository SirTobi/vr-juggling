/*
 * 
 */

#include "PhysicEngine.hpp"
#include "utils/EasySettings.hpp"


//****************************************************************************************************************************************************//
PhysicEngine::GravitationController::GravitationController(PhysicBall::Vec3* _grav)
	: mGravRef(_grav)
	, mGravitation(Vec3::ZERO)
{
	if(!mGravRef)
		mGravRef = &mGravitation;
	else
		mGravitation = *mGravRef;
}

const PhysicEngine::Vec3& PhysicEngine::GravitationController::gravitation() const
{
	return *mGravRef;
}


void PhysicEngine::GravitationController::specialize(const Vec3& g)
{
	mGravitation = g;
	mGravRef = &mGravitation;
}


void PhysicEngine::GravitationController::apply(PhysicBall* ball, double dt)
{
	ball->movement -= gravitation() * dt;
}



//****************************************************************************************************************************************************//

PhysicEngine::PhysicEngine(Ogre::SceneManager* sceneMgr, rapidxml::xml_node<>& settings)
	: leftHand(this, rxml::getnode(settings, "hands"))
	, rightHand(this, rxml::getnode(settings, "hands"))
	, mSceneMgr(sceneMgr)
{
	gravitation = SettingsValue<Vec3>(settings, "gravitation:value");
	
	leftHand.other = &rightHand;
	rightHand.other = &leftHand;
}

PhysicEngine::~PhysicEngine()
{

}

void PhysicEngine::update(double dt)
{
	leftHand.update(dt);
	rightHand.update(dt);
	for(PhysicBall& ball : balls)
	{
		ball.update(dt);
	}
}


PhysicBall* PhysicEngine::createBall(Ogre::SceneNode* node, float radius)
{
	balls.emplace_back(mSceneMgr, node, radius);
	PhysicBall& ball = balls.back();
	
	ball.controllers.push_back(std::make_shared<GravitationController>(&gravitation));
	
	return &ball;
}

void PhysicEngine::deleteBall(PhysicBall* ball)
{
	leftHand.removeFromHand(ball);
	rightHand.removeFromHand(ball);
	
	for(auto it = balls.begin(); it != balls.end(); ++it)
	{
		if(ball == &*it)
		{
			balls.erase(it);
			return;
		}
	}
}

