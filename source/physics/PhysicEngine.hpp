	/*
 * 
 */

#ifndef PHYSICENGINE_H
#define PHYSICENGINE_H

#include <list>
#include <stdexcept>
#include "rapidxml.hpp"
#include "OgreSceneNode.h"
#include "OgreVector3.h"

#include "PhysicBall.hpp"
#include "PhysicHand.hpp"

class PhysicEngine
{
public:
	typedef Ogre::Vector3 Vec3;
	
	struct GravitationController: public PhysicBall::Controller
	{
		GravitationController(Vec3* _grav = nullptr);
		virtual void apply(PhysicBall* ball, double dt);
		
		const Vec3& gravitation() const;
		void specialize(const Vec3& g);
	private:
		Vec3 mGravitation;
		Vec3* mGravRef;
	};
public:
	PhysicEngine(Ogre::SceneManager* sceneMgr, rapidxml::xml_node<>& settings);
	~PhysicEngine();
	
	void update(double dt);
	
	PhysicBall* createBall(Ogre::SceneNode* node, float radius);
	void deleteBall(PhysicBall* ball);
	
	
	Vec3 gravitation;
	PhysicHand rightHand;
	PhysicHand leftHand;
	std::list<PhysicBall> balls;
	
private:
	Ogre::SceneManager* mSceneMgr;
};

#endif // PHYSICSYSTEM_H
