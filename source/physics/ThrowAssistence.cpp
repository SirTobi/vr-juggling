/*
 * 
 */
#include "PhysicEngine.hpp"
#include "ThrowAssistence.hpp"

ThrowAssistence::ThrowAssistence(rapidxml::xml_node<>& settings)
	: ajust(rxml::getnode(settings, "ajustments"))
{

}

ThrowAssistence::Ajustment::Ajustment(rapidxml::xml_node< char >& settings)
	: mode (None)
{
	axisMod = SettingsValue<Vec3>(settings, "mode_axis/multiplier:value");
	planeAjust = SettingsValue<float>(settings, "mode_plane/plane_attraction:value");
	parableHeight = SettingsValue<float>(settings, "mode_parable/height:value");
	checkMovementForParable = SettingsValue<BoolAlpha>(settings, "mode_parable/movement_check:enabled");
	if(checkMovementForParable)
	{
		minSpeedForParable = SettingsValue<float>(settings, "mode_parable/movement_check/min_speed:value");
		angleForParable = SettingsValue<float>(settings, "mode_parable/movement_check/angle:value");
	}
}


void ThrowAssistence::apply(PhysicBall* ball, const Vec3& torsoDir, const Vec3& target_pos)
{
	switch(ajust.mode)
	{
	case Axis:
		ball->movement *= ajust.axisMod;
		break;
	case Plane:
		{
			Vec3 nTorsoDir = torsoDir.normalisedCopy();
			nTorsoDir.y = 0;
			Vec3 m = ball->movement;
			auto y = m.y;
			m.y = 0;
			
			
			ball->movement = m.dotProduct(nTorsoDir) * nTorsoDir;
			ball->movement.y = y;
		}break;
	case Parable:
		{
			// get gravitation from ball
			auto c = ball->controller_or_null<PhysicEngine::GravitationController>();
			
			const Vec3 g = c? c->gravitation() : Vec3::ZERO;
			
			// check if movement fullfills the requierments
			bool ajustMovement = true;
			if(ajust.checkMovementForParable)
			{
				const Vec3& m = ball->movement;
				if(m.length() < ajust.minSpeedForParable)
					ajustMovement = false;
				
				if(g.angleBetween(m).valueDegrees() > ajust.angleForParable)
					ajustMovement = false;
			}
			
			// calculate movemnet
			if(ajustMovement)
			{
				const Vec3 delta = target_pos - ball->pos;
				Vec3 direction = (g.crossProduct(delta).crossProduct(g));
				direction.normalise();
				
				Ogre::Real dirTimes = delta.dotProduct(direction);
				Ogre::Real gTimes = std::sqrt(2 * ajust.parableHeight / g.length());
				
				ball->movement = g * gTimes + direction * dirTimes;
			}
			
		}break;
	case None:
	default:
		break;
	}
}
