#ifndef PHYSICBALL_H
#define PHYSICBALL_H

#include "OgreSceneNode.h"
#include "OgreVector3.h"

class PhysicHand;

class PhysicBall
{
public:
	typedef Ogre::Vector3 Vec3;
	
	struct Controller
	{
		virtual void apply(PhysicBall* ball, double dt) = 0;
	};
	
	typedef std::shared_ptr<Controller> ControllerPtr;
public:
	PhysicBall(Ogre::SceneManager* sceneMgr, Ogre::SceneNode* _node, float _radius);
	~PhysicBall();
	
	void update(double dt);
	
	float radius;
	Vec3 movement;
	Vec3 pos;
	double update_interval;
	
	template<typename T>
	std::shared_ptr<T> controller() const
	{
		std::shared_ptr<T> c = controller_or_null<T>();
		if(!c)
			throw std::logic_error("There is no controller with type T!");
		
		return c;
	}
	
	template<typename T>
	std::shared_ptr<T> controller_or_null() const
	{
		for(ControllerPtr c : controllers)
		{
			auto ptr = std::dynamic_pointer_cast<T>(c);
			if(ptr)
				return ptr;
		}
		return nullptr;
	}
	
	template<typename T>
	bool removeController()
	{
		for(auto it = controllers.begin(); it != controllers.end(); ++it)
		{
			auto ptr = std::dynamic_pointer_cast<T>(*it);
			if(ptr)
			{
				controllers.erase(it);
				return true;
			}
		}
		return false;
	}
	
	std::list<ControllerPtr> controllers;
	std::list<ControllerPtr> explosion_controllers;
	
	void explode();
private:
	double mUpdateTimeLeft;
	Ogre::SceneNode* mNode;
	Ogre::SceneManager* mSceneMgr;
};

#endif // PHYSICBALL_H
