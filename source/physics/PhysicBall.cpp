/*
 * 
 */

#include <OgreSceneManager.h>
#include <OgreParticleSystem.h>
#include "PhysicBall.hpp"
#include "rxml/value.hpp"
#include "utils/LexicalCasts.hpp"


PhysicBall::PhysicBall(Ogre::SceneManager* sceneMgr, Ogre::SceneNode* _node, float _radius)
	: mNode(_node)
	, radius(_radius)
	, pos(_node->getPosition())
	, mSceneMgr(sceneMgr)
	, movement(0.0f)
	, update_interval(0.01)
	, mUpdateTimeLeft(0.0)
{
}

PhysicBall::~PhysicBall()
{
}


void PhysicBall::update(double dt)
{
	mUpdateTimeLeft += dt;
	
	while(mUpdateTimeLeft > 0.0)
	{
		for(ControllerPtr c : controllers)
		{
			c->apply(this, update_interval);
		}
		
		pos += movement * update_interval;
		mNode->setPosition(pos);
		
		mUpdateTimeLeft -= update_interval;
	}
}

void PhysicBall::explode()
{
	for(auto& c : explosion_controllers)
		c->apply(this, 0);
}
