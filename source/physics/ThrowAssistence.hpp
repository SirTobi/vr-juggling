/*
 * 
 */

#ifndef THROWASSISTENCE_H
#define THROWASSISTENCE_H

#include "PhysicBall.hpp"
#include "utils/EasySettings.hpp"


class ThrowAssistence
{
public:
	typedef Ogre::Vector3 Vec3;
	
	enum AjustmentMode {
		None = 0,
		Axis,
		Plane,
		Parable,
		_Modes
	};
public:
	ThrowAssistence(rapidxml::xml_node<>& settings);
	
	struct Ajustment {

		Ajustment(rapidxml::xml_node<>& settings);
		
		AjustmentMode mode;
		Vec3 axisMod;
		float planeAjust;
		
		float parableHeight;
		bool checkMovementForParable;
		float minSpeedForParable;
		float angleForParable;
		
		inline void nextMode()
		{
			switch(mode)
			{
			case None: mode = Axis; break;
			case Axis: mode = Plane; break;
			case Plane: mode = Parable; break;
			default:
			case Parable: mode = None; break;
			};
		}
	} ajust;
	
	void apply(PhysicBall* ball, const Vec3& torsoDir, const Vec3& target_pos);
};

#endif // THROWASSISTENCE_H
