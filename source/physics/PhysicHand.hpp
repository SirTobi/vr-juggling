#ifndef PHYSICHAND_H
#define PHYSICHAND_H

#include <list>
#include <stdexcept>
#include "rapidxml.hpp"
#include <OgreSceneNode.h>
#include <OgreEntity.h>
#include <functional>
#include <chrono>
#include "OgreVector3.h"


class PhysicBall;
class PhysicEngine;

class PhysicHand
{
public:
	class GrabController;
	typedef Ogre::Vector3 Vec3;
	typedef std::function<void(PhysicBall*, PhysicHand*)> ReleaseHandler;
	typedef std::function<void(PhysicBall*, PhysicHand*)> CatchHandler;
public:
	PhysicHand(PhysicEngine* engine, rapidxml::xml_node<>& settings);
	~PhysicHand();
	
	void update(double dt);
	
	void grab(bool _grab);
	void addBall(PhysicBall* ball, bool catched = false);
	bool isBallInHand(PhysicBall* ball);
	bool removeFromHand(PhysicBall* ball);
	
	void setSceneNode(Ogre::SceneNode* handNode, Ogre::Entity* handEntity);
	
	float ballOffset;
	Vec3 pos;
	Vec3 direction;
	float grabTolerance;
	double handTrakingHistoryLength;
	PhysicHand* other = nullptr;
	
	ReleaseHandler release_event;
	CatchHandler catch_event;
private:
	void checkGrap();
	
	void updateHandHistory(double dt);
	
	void checkReleaseBall();
	Vec3 calculateBallMovement() const;
	void releaseBall(const Vec3& movement);

private:
	float mGrabDuration;
	bool mGrab;
	PhysicBall* mReleaseBall;
	
	bool mLazyCatch;
	std::chrono::milliseconds mTimedLazyCatch;
	std::chrono::milliseconds mCurrentLazyCatchTimeLeft;
	
	std::list<PhysicBall*> mBalls;
	std::list<std::pair<double, Ogre::Vector3>> mLastHandPositions;
	PhysicEngine* mEngine;
	
	Ogre::SceneNode* mHandNode = nullptr;
	Ogre::Entity* mHandEntity = nullptr;
};

#endif // PHYSICHAND_H
