#include "PhysicHand.hpp"
#include "PhysicBall.hpp"
#include "PhysicEngine.hpp"
#include <cassert>
#include <OgreAnimation.h>
#include <OgreQuaternion.h>
#include <algorithm>
#include "utils/EasySettings.hpp"



struct PhysicHand::GrabController: public PhysicBall::Controller
{
	GrabController(PhysicHand* hand, float idx_dist)
		: hand(hand)
		, idx_dist(idx_dist)
	{}
	
	
	PhysicHand* hand;
	float idx_dist;
	
	void apply(PhysicBall* ball, double dt)
	{
		
		ball->pos = hand->pos + idx_dist * hand->direction;
		if(hand->mHandNode)
			ball->pos += hand->mHandNode->getOrientation() * Ogre::Vector3(0, 0.05, 0.05);
		ball->movement = Ogre::Vector3::ZERO;
	}
};



PhysicHand::PhysicHand(PhysicEngine* engine, rapidxml::xml_node<>& settings)
	: mGrab(false)
	, mEngine(engine)
	, mReleaseBall(nullptr)
{
	grabTolerance = SettingsValue<float>(settings, "grab_tolerance:value");
	handTrakingHistoryLength = SettingsValue<double>(settings, "tracking/history_length:value");
	mLazyCatch = SettingsValue<BoolAlpha>(settings, "lazy_catch:enabled");
	if(mLazyCatch)
	{
		mTimedLazyCatch = std::chrono::milliseconds(SettingsValue<unsigned int>(settings, "lazy_catch:timed"));
	}
	
	mGrabDuration = SettingsValue<float>(settings, "grab_duration:value");
	ballOffset = SettingsValue<float>(settings, "ball_offset:value");
}

PhysicHand::~PhysicHand()
{

}


void PhysicHand::update(double dt)
{
	namespace ch = std::chrono;
	if(mReleaseBall)
	{
		auto ball_movement = calculateBallMovement();
		auto old_hand_pos = mLastHandPositions.front().second;
		
		auto hand_delta = pos - old_hand_pos;
		
		// was hand to slow?
		if(/*ball_movement.length() < 0.1
			|| */hand_delta.length() / dt < ball_movement.length()
			)
		{
			releaseBall(ball_movement);
		}
		
	}
	
	updateHandHistory(dt);
	
	
	if((mBalls.size() || (mLazyCatch && mCurrentLazyCatchTimeLeft > std::chrono::milliseconds(0))) && mGrab)
	{
		checkGrap();
	}else{
		
		for(auto& ball : mEngine->balls)
		{
			// check if a ball collides!
		}
	}
	
	mCurrentLazyCatchTimeLeft -= ch::duration_cast<ch::milliseconds>(ch::duration<double>(dt));
	
	// update hand
	if(mHandEntity)
	{
		if(direction.isZeroLength())
		{
			direction = Vec3::NEGATIVE_UNIT_Z;
		}
		
		mHandNode->setDirection(direction, Ogre::Node::TS_WORLD, Vec3::UNIT_Z);
		mHandNode->setPosition(pos);
		
		Vec3 localY = mHandNode->getOrientation() * Vec3::UNIT_Y;
		Ogre::Quaternion quat = localY.getRotationTo(Vec3::UNIT_Y);
		mHandNode->rotate(quat, Ogre::Node::TS_WORLD);
		
		
		
		// animation
		auto* a = mHandEntity->getAnimationState("GrabAnimation");
		a->addTime(dt * (mGrab? 1.0 : -1.0) * (1.0 / mGrabDuration));
	}
}

void PhysicHand::grab(bool _grab)
{
	mGrab = _grab;
	
	if(_grab)
	{
		if(mReleaseBall)
		{
			releaseBall(calculateBallMovement());
		}
		mCurrentLazyCatchTimeLeft = mTimedLazyCatch;
		checkGrap();
	}else{
		// release!!!!
		if(mBalls.size() && !mReleaseBall)
		{
			mReleaseBall  = mBalls.back();
			mBalls.pop_back();
		}
	}
}

bool PhysicHand::isBallInHand(PhysicBall* ball)
{
	return std::find(mBalls.begin(), mBalls.end(), ball) != mBalls.end();
}


void PhysicHand::updateHandHistory(double dt)
{
	mLastHandPositions.push_front(std::make_pair(dt, pos));
	while (mLastHandPositions.size() >= handTrakingHistoryLength / dt)
	{
		mLastHandPositions.pop_back();
	}
}


/*
void PhysicHand::checkReleaseBall()
{
	if(!mReleaseBall)
		return;
	
	
}*/


PhysicHand::Vec3 PhysicHand::calculateBallMovement() const
{	
	double maxspeed = 0.0;
	auto it = mLastHandPositions.begin();
	auto maxIt = it;
	auto nxtIt = it;
	
	while((nxtIt = std::next(nxtIt)) != mLastHandPositions.end())
	{
		double dist = std::abs(it->second.distance(nxtIt->second));
		if(dist > maxspeed)
		{
			maxIt = it;
			maxspeed = dist;
		}
		
		it = nxtIt;
	}
	
	auto firstIt = mLastHandPositions.begin();
	auto m = firstIt->second - std::next(firstIt)->second;
	m.normalise();
	m *= maxspeed;
	m *= 1.0 / maxIt->first;
	
	return m;
}

void PhysicHand::releaseBall(const PhysicHand::Vec3& movement)
{
	assert(mReleaseBall);
	auto* ball = mReleaseBall;
	mReleaseBall = nullptr;
	
	ball->movement = movement;
	ball->removeController<GrabController>();
	
	std::cout << "Throw Ball [delta=" << movement << "| samples=" << mLastHandPositions.size() << "]" << std::endl;
	release_event(ball, this);
}

bool PhysicHand::removeFromHand(PhysicBall* ball)
{
	auto it = std::find(mBalls.begin(), mBalls.end(), ball);
	if(it != mBalls.end())
	{
		mBalls.erase(it);
	}
	else if(ball == mReleaseBall)
	{
		mReleaseBall = nullptr;
	}else
		return false;
	
	ball->removeController<GrabController>();
	ball->movement = Vec3(0, 0, 0);
	
	return true;
}


void PhysicHand::addBall(PhysicBall* ball, bool catched)
{
	if(ball->controller_or_null<GrabController>())
		return;
	
	std::cout << "Add ball"<< std::endl;
	ball->controllers.push_back(std::make_shared<GrabController>(this, float(mBalls.size()) * ballOffset));
	mBalls.push_back(ball);
	
	if(catch_event && catched)
		catch_event(ball, this);
}

void PhysicHand::setSceneNode(Ogre::SceneNode* handNode, Ogre::Entity* handEntity)
{
	mHandNode = handNode;
	mHandEntity = handEntity;
	
	auto* a = mHandEntity->getAnimationState("GrabAnimation");
	a->setLoop(false);
	a->setEnabled(true);
}


void PhysicHand::checkGrap()
{
	if(!mGrab)
		return;
	
	for(auto& ball : mEngine->balls)
	{
		auto d = ball.radius + grabTolerance;
		if(ball.pos.squaredDistance(pos) < d * d)
		{
			addBall(&ball, true);
		}
	}
}
