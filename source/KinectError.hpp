#ifndef _KINECT_ERROR_HPP
#define _KINECT_ERROR_HPP

#include <string>
#include <XnCppWrapper.h>

inline std::string to_string(xn::EnumerationErrors& errors)
{
	XnChar strError[1024];
	errors.ToString(strError, 1024);
	return std::string(strError);
}

inline void throw_error(std::string info)
{
	throw std::runtime_error(std::move(info));
}

inline void throw_error(std::string info, XnStatus status, xn::EnumerationErrors& errors)
{
	info += "(Status: ";
	info += xnGetStatusString(status);
	info += ")[";
	info += to_string(errors);
	info += ']';
	throw_error(info);
}

inline bool check_result(XnStatus status, const char* info, xn::EnumerationErrors& errors)
{
	if(status != XN_STATUS_OK)
	{
		throw_error(info, status, errors);
	}
}


inline void throw_error(std::string info, XnStatus status)
{
	info += "(Status: ";
	info += xnGetStatusString(status);
	info += ')';
	throw std::runtime_error(std::move(info));
}

inline bool check_result(XnStatus status, const char* info)
{
	if(status != XN_STATUS_OK)
	{
		throw_error(info, status);
	}
}

#endif