//|||||||||||||||||||||||||||||||||||||||||||||||
#include <fstream>
#include "rxml/value.hpp"
#include "rapidxml.hpp"
#include "framework/AppStateManager.hpp"
#include "framework/AdvancedOgreFramework.hpp"
#include "KinectManager.hpp"
#include "MainApp.hpp"

#include "states/CalibrationState.hpp"
#include "states/TestState.hpp"
#include "states/LaboratoryState.hpp"
#include "states/CourseState.hpp"

template<> MainApp* Ogre::Singleton<MainApp>::msSingleton = 0;
//|||||||||||||||||||||||||||||||||||||||||||||||

MainApp::MainApp()
{
	mAppStateManager = 0;
}

//|||||||||||||||||||||||||||||||||||||||||||||||

MainApp::~MainApp()
{
    delete OgreFramework::getSingletonPtr();
}

//|||||||||||||||||||||||||||||||||||||||||||||||

void MainApp::start()
{
	new OgreFramework();
	if(!OgreFramework::getSingletonPtr()->initOgre("VR-Juggling", 0, 0))
		return;

	OgreFramework::getSingletonPtr()->log()->logMessage("App initialized!");

	// load settings
	std::vector<char> file_data;
	std::ifstream file("application_settings.xml");
	file >> std::noskipws;
	std::copy(std::istream_iterator<char>(file), std::istream_iterator<char>(), std::back_inserter(file_data));
	file_data.push_back(0);
	rapidxml::xml_document<> doc;
	doc.parse<0>(file_data.data());
	rapidxml::xml_node<>& app_node = rxml::getnode(doc, "application");
	rapidxml::xml_node<>& settings_node = rxml::getnode(app_node, "settings");
	
	
	
	
	mAppStateManager.reset(new AppStateManager());

	//CalibrationState::create(mAppStateManager.get(), "CalibrationState", );
	TestState::create(mAppStateManager.get(), "TestState", rxml::getnode(app_node, "app_states/test_state"));
	LaboratoryState::create(mAppStateManager.get(), "LaboratoryState", rxml::getnode(app_node, "app_states/laboratory_state"));
	CourseState::create(mAppStateManager.get(), "CourseState", rxml::getnode(app_node, "app_states/course_state"));
	/*MenuState::create(m_pAppStateManager, "MenuState");
	GameState::create(m_pAppStateManager, "GameState");
    PauseState::create(m_pAppStateManager, "PauseState");*/
	AppState* start_state = mAppStateManager->findByName("CourseState");

	// init kinect
	mKinectManager.reset(new KinectManager(rxml::getnode(settings_node, "kinect")));
	
	mAppStateManager->start(start_state);
}

//|||||||||||||||||||||||||||||||||||||||||||||||