/*
 * 
 */

#include "CalibrationState.hpp"

CalibrationState::CalibrationState()
{

}

CalibrationState::~CalibrationState()
{

}

/**********************************************************/

void CalibrationState::enter()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Enter CalibrationState...");

    createScene();
}

/**********************************************************/

void CalibrationState::createScene()
{
}

/**********************************************************/

void CalibrationState::exit()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Leaving CalibrationState...");

    /*m_pSceneMgr->destroyCamera(m_pCamera);
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);*/

    OgreFramework::getSingletonPtr()->trayMgr()->clearAllTrays();
    OgreFramework::getSingletonPtr()->trayMgr()->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->trayMgr()->setListener(0);
}

/**********************************************************/

bool CalibrationState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	
    return OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
}

/**********************************************************/

bool CalibrationState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
}

/**********************************************************/

bool CalibrationState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseMove(evt))
		return true;
    return true;
}

/**********************************************************/


bool CalibrationState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Pressed " <<  id << std::endl;
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseDown(evt, id))
		return true;
    return true;
}

/**********************************************************/

bool CalibrationState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Released " <<  id << std::endl;
	
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseUp(evt, id))
		return true;
    return true;
}

/**********************************************************/

void CalibrationState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->trayMgr()->frameRenderingQueued(m_FrameEvent);
}

/**********************************************************/

void CalibrationState::buttonHit(OgreBites::Button *button)
{
}
