/*
 * 
 */
#include <OgreMeshManager.h>
#include "CourseState.hpp"
#include "MainApp.hpp"
#include "KinectManager.hpp"
#include "SkeletonProvider.hpp"
#include "Skeleton.hpp"
#include "utils/OgreASBinding.hpp"
#include "utils/EasySettings.hpp"
#include "utils/DynamicLines.hpp"
#include "utils/MovableText.hpp"
#include "utils/Arrow.hpp"
#include "OgreOculus.hpp"
#include "physics/PhysicEngine.hpp"
#include "physics/ThrowAssistence.hpp"
#include <scriptarray/scriptarray.h>
#include <../../SFML/include/SFML/Audio/Sound.hpp>
#include <SFML/Audio.hpp>
#include <ctime>

CourseState::CourseState(rapidxml::xml_node<>& config)
	: mFramework(OgreFramework::getSingletonPtr())
	, mConfig(config)
	, mCamera(nullptr)
	, mCameraMan(nullptr)
	, mThrowAssistence(rxml::getnode(config, "throw_assistance"))
	, mTimeFactor(1.0f)
{
}

CourseState::~CourseState()
{

}

/**********************************************************/

void CourseState::enter()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Enter CourseState...");

    mSceneMgr = mFramework->root()->createSceneManager(Ogre::ST_GENERIC, "CourseSceneMgr");
    createScene();
}

/**********************************************************/

void CourseState::createScene()
{
	// Camera
	bool use_oculus_rift = SettingsValue<BoolAlpha>(mConfig, "oculus_rift:enabled");
	if(use_oculus_rift &&
			(mOculus.reset(new Oculus()),
			mOculus->setupOculus()))
	{
		mOculus->setupOgre(mSceneMgr, mFramework->renderWindow());
		mOculus->getCameraNode()->setPosition(1, 2, -0.3);
	}else{
		mOculus.reset();
		mCamera = mSceneMgr->createCamera("FreeLookCam");
		mCamera->setPosition(-1, 2, 1);
		mCamera->lookAt(0, 1, 0);
		mCamera->setNearClipDistance(0.0001);
		mCamera->setAspectRatio(Ogre::Real(OgreFramework::getSingletonPtr()->viewport()->getActualWidth()) /
			Ogre::Real(mFramework->viewport()->getActualHeight()));
		
		mCameraMan = new OgreBites::SdkCameraMan(mCamera);
		mCameraMan->setStyle(OgreBites::CS_FREELOOK);
		mCameraMan->setTopSpeed(3);
		mFramework->viewport()->setCamera(mCamera);
	}
	
	
	// setup skeleton
	mSkeleton = new Skeleton(); 	
	mSkeleton->color = Ogre::ColourValue::Blue;
	
	mSkeleton->update();
	Ogre::SceneNode* skeletonNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Skeleton");
	skeletonNode->attachObject(mSkeleton);
	skeletonNode->setVisible(false);
	
	// setup physic engine
	mPhysicEngine.reset(new PhysicEngine(mSceneMgr, rxml::getnode(mConfig, "environment/physics")));
	
	// setup hands
	{
		Ogre::Vector3 handScale(SettingsValue<float>(mConfig, "hand_size:value"));
		
		// left hand
		auto e = mSceneMgr->createEntity("left hand", "Hand.mesh");
		auto sn = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(e);
		sn->setScale(handScale);
		sn->scale(-1, -1, 1);
		mPhysicEngine->leftHand.setSceneNode(sn, e);
		
		// right hand
		e = mSceneMgr->createEntity("right hand", "Hand.mesh");
		sn = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(e);
		sn->setScale(handScale);
		sn->scale(1, -1, 1);
		mPhysicEngine->rightHand.setSceneNode(sn, e);
	}
	
	// setup throw events
	{
		using std::placeholders::_1;
		using std::placeholders::_2;
		mPhysicEngine->rightHand.release_event = std::bind(&CourseState::ball_released, this, std::placeholders::_1, std::placeholders::_2);
		mPhysicEngine->leftHand.release_event = std::bind(&CourseState::ball_released, this, std::placeholders::_1, std::placeholders::_2);
		
		
		mPhysicEngine->rightHand.catch_event = std::bind(&CourseState::ball_catched, this, std::placeholders::_1, std::placeholders::_2);
		mPhysicEngine->leftHand.catch_event = std::bind(&CourseState::ball_catched, this, std::placeholders::_1, std::placeholders::_2);
	}
	
	
	mChamber.reset(new ScriptChamber(mSceneMgr));
	init_bindings();
	mChamber->start(rxml::value(mConfig, "test_driver:main"), rxml::value(mConfig, "test_driver:base"));
}

/**********************************************************/

void CourseState::exit()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Leaving CourseState...");

	if(mCameraMan)	
		delete mCameraMan;
	if(mCamera)
		mSceneMgr->destroyCamera(mCamera);
    if(mSceneMgr)
        mFramework->root()->destroySceneManager(mSceneMgr);


    OgreFramework::getSingletonPtr()->trayMgr()->clearAllTrays();
    OgreFramework::getSingletonPtr()->trayMgr()->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->trayMgr()->setListener(0);
}

/**********************************************************/

bool CourseState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	if(mCameraMan)
		mCameraMan->injectKeyDown(keyEventRef);
	
    return OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
}

/**********************************************************/

bool CourseState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	using Ogre::Vector3;
	
	if(mCameraMan)
		mCameraMan->injectKeyUp(keyEventRef);
	
	switch(keyEventRef.key)
	{
	case OIS::KC_ESCAPE:
		shutdown();
		break;
	case OIS::KC_SPACE:
		if(mOculus)
			mOculus->resetOrientation();	
	default:
		return OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
	};
	
	return true;
}

/**********************************************************/

bool CourseState::mouseMoved(const OIS::MouseEvent &evt)
{
	if(mCameraMan)
		mCameraMan->injectMouseMove(evt);
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseMove(evt))
		return true;
    return true;
}

/**********************************************************/


bool CourseState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Pressed " <<  id << std::endl;
	toggleGrab(id, true);
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseDown(evt, id))
		return true;
    return true;
}

/**********************************************************/

bool CourseState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Released " <<  id << std::endl;
	toggleGrab(id, false);	
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseUp(evt, id))
		return true;
    return true;
}

void CourseState::toggleGrab(const OIS::MouseButtonID id, bool grab)
{
	{
		PhysicHand* hand = nullptr;
		if(id == OIS::MouseButtonID::MB_Left)
			hand = &(mPhysicEngine->leftHand);
		
		if(id == OIS::MouseButtonID::MB_Right)
			hand = &(mPhysicEngine->rightHand);
		
		
		if(hand)
		{
			if(SettingsValue<BoolAlpha>(mConfig, "controls/flip_mouse_buttons:enabled"))
				hand = hand->other;
			hand->grab(grab);
		}
	}
}


/**********************************************************/

void CourseState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->trayMgr()->frameRenderingQueued(m_FrameEvent);
	
	// update kinect
	auto& skeleton_provider = MainApp::getSingleton().kinectManager()->skeleton_provider();
	
	if(skeleton_provider.update())
	{
		mSkeleton->apply(skeleton_provider);
		
		// update hands
		mPhysicEngine->rightHand.pos = mSkeleton->getHand(Skeleton::Right).pos;
		mPhysicEngine->rightHand.direction = mPhysicEngine->rightHand.pos - mSkeleton->getJoint(XN_SKEL_RIGHT_ELBOW).pos;
		mPhysicEngine->rightHand.direction.normalise();
		mPhysicEngine->leftHand.pos = mSkeleton->getHand(Skeleton::Left).pos;
		mPhysicEngine->leftHand.direction = mPhysicEngine->leftHand.pos - mSkeleton->getJoint(XN_SKEL_LEFT_ELBOW).pos;
		mPhysicEngine->leftHand.direction.normalise();
		
		if(mOculus)
		{
			Ogre::SceneNode* cam_node = mOculus->getCameraNode();
			
			//if(mSkeleton->getHead().confidence > 0.0f)
			cam_node->setPosition(mSkeleton->getHead().pos);
		}
	}
	
	// move camera
	if(!mOculus)
	{
		mCameraMan->frameRenderingQueued(m_FrameEvent);
	}else if(mOculus->isOculusReady())
	{
		Ogre::SceneNode* cam_node = mOculus->getCameraNode();
		cam_node->setOrientation(mOculus->getOrientation());
	}
	
	// update physics	
	mPhysicEngine->update(timeSinceLastFrame * mTimeFactor);
	
	if(!mChamber->update(timeSinceLastFrame * mTimeFactor))
		shutdown();
}

/**********************************************************/

void CourseState::buttonHit(OgreBites::Button *button)
{
}

void CourseState::destroyAllAttachedMovableObjects( Ogre::SceneNode* i_pSceneNode )
{
   if ( !i_pSceneNode )
   {
      assert( false );
      return;
   }

   // Destroy all the attached objects
   Ogre::SceneNode::ObjectIterator itObject = i_pSceneNode->getAttachedObjectIterator();

   while ( itObject.hasMoreElements() )
   {
      Ogre::MovableObject* pObject = static_cast<Ogre::MovableObject*>(itObject.getNext());
      i_pSceneNode->getCreator()->destroyMovableObject( pObject );
   }

   // Recurse to child SceneNodes
   Ogre::SceneNode::ChildNodeIterator itChild = i_pSceneNode->getChildIterator();

   while ( itChild.hasMoreElements() )
   {
      Ogre::SceneNode* pChildNode = static_cast<Ogre::SceneNode*>(itChild.getNext());
      destroyAllAttachedMovableObjects( pChildNode );
   }
}

void CourseState::ball_released(PhysicBall* ball, PhysicHand* hand)
{
	auto planeDir = mSkeleton->getHand(Skeleton::Right).pos - mSkeleton->getHand(Skeleton::Left).pos;
	mThrowAssistence.apply(ball, planeDir, hand->other->pos);
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Object registration

struct CourseState::ChamberController
{
	CourseState* mState;
	asIScriptContext* mCallbackContext;
	
	ChamberController(CourseState* state)
		: mState(state)
	{
		mCallbackContext = state->mChamber->getEngine()->CreateContext();
		
		// load some sounds
		mBufferdSounds["catch.ogg"].loadFromFile("media/sounds/catch.ogg");
		mBufferdSounds["ok.ogg"].loadFromFile("media/sounds/ok.ogg");
		mBufferdSounds["fail.ogg"].loadFromFile("media/sounds/fail.ogg");
		mBufferdSounds["up.ogg"].loadFromFile("media/sounds/up.ogg");
		mBufferdSounds["down.ogg"].loadFromFile("media/sounds/down.ogg");
		mBufferdSounds["teleport.ogg"].loadFromFile("media/sounds/teleport.ogg");
	}
	
	ScriptChamber* chamber() const {
		return mState->mChamber.get();
	}
	
	struct KinectWaiter: Action
	{
		virtual bool update(double dt) override
		{
			if(MainApp::getSingleton().kinectManager()->skeleton_provider().get_user_count() > 0)
			{
				return false;
			}
			return true;
		}
	};
	
	ActionToken initKinect()
	{
		return chamber()->add_action(new KinectWaiter());
	}
	
	Ogre::SceneNode* getSceneNodeByName(const std::string& name)
	{
		return mState->mSceneMgr->getSceneNode(name);
	}
	
	Ogre::SceneNode* createGround(const std::string& name, const std::string& material)
	{
		auto scm = mState->mSceneMgr;
		Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
	
		auto mesh_name = name + "_mesh";
		Ogre::MeshManager::getSingleton().createPlane(mesh_name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			plane, 500, 500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
	
		Ogre::Entity* entGround = scm->createEntity(name + "_entity", mesh_name);
		entGround->setMaterialName(material);	
		entGround->setCastShadows(false);
		
		auto* node = scm->getRootSceneNode()->createChildSceneNode(name);
		node->attachObject(entGround);
	
		return node;
	}
	
	Ogre::SceneNode* createMesh(const std::string& name, const std::string& mesh)
	{
		auto scm = mState->mSceneMgr;
		auto e = scm->createEntity(name, mesh);
		auto sn = scm->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(e);
		
		return sn;
	}
	
	Ogre::SceneNode* createLineEntity(const std::string& name, CScriptArray* lines, const std::string& material)
	{
		auto scm = mState->mSceneMgr;
		DynamicLines* dl = new DynamicLines();
		
		assert(lines->GetSize() % 2 == 0);
		
		for(int i = 0; i < lines->GetSize(); i += 2)
		{
			dl->addLine(
				*((Ogre::Vector3*) lines->At(i)),
				*((Ogre::Vector3*) lines->At(i + 1))
			);
		}
		
		dl->setMaterial(material);
		dl->update();
		
		auto sn = scm->getRootSceneNode()->createChildSceneNode(name);
		sn->attachObject(dl);
		
		return sn;
	}
	
	Ogre::SceneNode* createLight(const std::string& name)
	{
		auto scm = mState->mSceneMgr;
		Ogre::Light* light = scm->createLight(name);
		light->setType(Ogre::Light::LT_POINT);
		light->setDiffuseColour(Ogre::ColourValue(1, 1, 1));
		light->setPowerScale(3.0);
		light->setSpecularColour(Ogre::ColourValue(1, 1, 1));
		
		auto sn = scm->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(light);
		
		return sn;
	}
	
	Ogre::SceneNode* createText(const std::string& name, const std::string& font, bool staticOri)
	{
		auto scm = mState->mSceneMgr;
		auto text = new Ogre::MovableText(name, "[No Text]", font, 0.2);
		text->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER);
		text->setStaticOrientation(staticOri);
		auto text_node = scm->getRootSceneNode()->createChildSceneNode();
		text_node->attachObject(text);
		text_node->setPosition(0, 0, 0);
		
		return text_node;
	}
	
	void createSphereMesh(const std::string& name, float radius)
	{
		mState->createSphere(name, radius);
		
	}
	
	void changeText(Ogre::SceneNode* node, const std::string& txt)
	{
		auto text_entity = dynamic_cast<Ogre::MovableText*>(node->getAttachedObject(0));
		assert(text_entity);
		text_entity->setCaption(txt);
	}
	
	void changeAlign(Ogre::SceneNode* node, Ogre::MovableText::HorizontalAlignment ha, Ogre::MovableText::VerticalAlignment va)
	{
		auto text_entity = dynamic_cast<Ogre::MovableText*>(node->getAttachedObject(0));
		assert(text_entity);
		text_entity->setTextAlignment(ha, va);
	}
	
	void changeTextSize(Ogre::SceneNode* node, float size)
	{
		auto text_entity = dynamic_cast<Ogre::MovableText*>(node->getAttachedObject(0));
		assert(text_entity);
		text_entity->setCharacterHeight(size);
	}
	
	void changeMaterialName(Ogre::SceneNode* node, const std::string& txt)
	{
		auto entity = dynamic_cast<Ogre::Entity*>(node->getAttachedObject(0));
		assert(entity);
		entity->setMaterialName(txt);
	}
	
	void deleteSceneNode(Ogre::SceneNode* node)
	{
		std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
		mState->destroyAllAttachedMovableObjects(node);
	}
	
	void constructArrow(Arrow* arrow, const std::string& name, bool _3d)
	{
		auto scm = mState->mSceneMgr;
		Ogre::ManualObject* m = scm->createManualObject(name + "-mo");
		scm->getRootSceneNode()->createChildSceneNode(name)->attachObject(m);
		
		new(arrow) Arrow(m, _3d);
	}
	
	Ogre::SceneNode* arrow_getSceneNode(Arrow* arrow)
	{
		return arrow->getObject()->getParentSceneNode();
	}
	
	void arrow_linegen(Arrow* arrow, const Ogre::Vector3& _source, const Ogre::Vector3& _target)
	{
		arrow->simple_gen(_source, _target);
	}
	
	void arrow_circle_gen(Arrow* arrow, const Ogre::Vector3& _source, const Ogre::Vector3& _target, float height, const Ogre::Vector3& arch_dir, float middle)
	{
		const float ep = 0.0001;
		if(height > -ep && height < ep)
		{
			height = _source.distance(_target) / 2.0f;
		}
		
		auto gen = std::make_shared<ArrowFromCircle>(_source, _target, arch_dir, middle);
		gen->height = height;
		arrow->generator(gen);
	}
	
	
	PhysicBall* createBall(Ogre::SceneNode* node, float radius)
	{
		PhysicBall* ball = mState->mPhysicEngine->createBall(node, radius);
		return ball;
	}
	
	void ballToHand(PhysicBall* ball, bool right)
	{
		PhysicHand& hand = right? mState->mPhysicEngine->rightHand : mState->mPhysicEngine->leftHand;
		hand.addBall(ball);
	}
	
	bool isBallInHand(PhysicBall* ball, bool rightHand)
	{
		PhysicHand& hand = rightHand? mState->mPhysicEngine->rightHand : mState->mPhysicEngine->leftHand;
		return hand.isBallInHand(ball);
	}
	
	void deleteBall(PhysicBall* ball)
	{
		mState->mPhysicEngine->deleteBall(ball);
	}
	
	void addScriptController(PhysicBall* ball, asIScriptObject* obj)
	{
		ball->controllers.push_back(ScriptController::Create(obj, mCallbackContext, mState->mChamber->getEngine()));
	}
	
	void addExplosionScriptController(PhysicBall* ball, asIScriptObject* obj)
	{
		ball->explosion_controllers.push_back(ScriptController::Create(obj, mCallbackContext, mState->mChamber->getEngine()));
	}
	
	
	class ScriptController: public PhysicBall::Controller
	{
	public:
		ScriptController(asIScriptObject* obj, asIScriptFunction* func, asIScriptContext* context)
			: mObj(obj)
			, mFunc(func)
			, mContext(context)
		{
			assert(obj && func);
		}
		
		~ScriptController()
		{
			mObj->Release();
		}
		
		void apply(PhysicBall* ball, double dt)
		{
			mContext->Prepare(mFunc);
			mContext->SetObject(mObj);
			
			mContext->SetArgObject(0, ball);
			mContext->SetArgFloat(1, dt);
			
			mContext->Execute();
		}
		
		static std::shared_ptr<ScriptController> Create(asIScriptObject* obj, asIScriptContext* context, asIScriptEngine* engine)
		{
			assert(obj && engine);
			auto objType = engine->GetObjectTypeById(engine->GetTypeIdByDecl("IBallController"));
			auto func = objType->GetMethodByDecl("void apply(Ball@ node, float dt)");
			return std::make_shared<ScriptController>(obj, func, context);
		}
		
	private:
		asIScriptObject* mObj;
		asIScriptFunction* mFunc;
		asIScriptContext* mContext;
	};
	
	Ogre::SceneNode* createParticleSystem(const std::string& name, const std::string& templ)
	{
		auto* ps = mState->mSceneMgr->createParticleSystem(name, templ);
		//ps->setEmitting(false);
		Ogre::SceneNode* ps_node = mState->mSceneMgr->getRootSceneNode()->createChildSceneNode();
		ps_node->attachObject(ps);
		
		for(int i = 0; i < ps->getNumEmitters(); ++i)
		{
			ps->getEmitter(i)->setEnabled(false);
		}
		
		return ps_node;
	}
	
	void fireParticelSystem(Ogre::SceneNode* node)
	{
		auto entity = dynamic_cast<Ogre::ParticleSystem*>(node->getAttachedObject(0));
		assert(entity);
		for(int i = 0; i < entity->getNumEmitters(); ++i)
		{
			entity->getEmitter(i)->setEnabled(true);
		}
	}
	
	
	
	
	
	
	struct BoolAction: Action
	{		
		virtual bool update(double dt) override
		{
			return true	;
		}
	};
	
	ActionToken openCustomAction()
	{
		return chamber()->add_action(new BoolAction());
	}
	
	void closeCustomAction(ActionToken tk)
	{
		chamber()->stop_action(tk);
	}
	
	
	struct PlayMusicAction : Action
	{
		PlayMusicAction(const std::string& _file, float _volume)
		{
			if(!mMusic.openFromFile("media/sounds/" + _file))
			{
				throw std::logic_error(_file + " does not exist!");
			}
			mMusic.play();
			mMusic.setVolume(_volume);
		}
		
		virtual bool update(double dt) override
		{
			return mMusic.getStatus() == sf::SoundSource::Playing;
		}
		
		sf::Music mMusic;
	};
	
	struct PlaySoundAction : Action
	{
		PlaySoundAction(sf::SoundBuffer& buf, float _volume)
			: mSound(buf)
		{
			mSound.play();
			mSound.setVolume(_volume);
		}
		
		virtual bool update(double dt) override
		{
			return mSound.getStatus() == sf::SoundSource::Playing;
		}
		
		sf::Sound mSound;
	};
	
	std::unordered_map<std::string, sf::SoundBuffer> mBufferdSounds;
	
	
	ActionToken playSound(const std::string& _file, float _volume)
	{
		auto it = mBufferdSounds.find(_file);
		if(it == mBufferdSounds.end())
			return chamber()->add_action(new PlayMusicAction(_file, _volume));
		else
		{
			return chamber()->add_action(new PlaySoundAction(it->second, _volume));
		}
	}
	
	struct SceneNodeMover: Action
	{
		Ogre::SceneNode* const node;
		const double duration;
		double progress;
		Ogre::Vector3 start, direction;
	
		SceneNodeMover(Ogre::SceneNode* node, const Ogre::Vector3& _target, const ch::milliseconds& _duration)
			: node(node)
			, start(node->getPosition())
			, duration(ch::duration_cast<ch::duration<double>>(_duration).count())
			, progress(0)
		{
			direction = _target - start;
		}
		
		virtual bool update(double dt) override
		{
			bool done = false;
			progress += dt;
			if(progress >= duration)
			{
				done = true;
				progress = duration;
			}
			
			node->setPosition(start + direction * (progress / duration));
			return !done;
		}
	};
	
	ActionToken moveOgreEntity(Ogre::SceneNode* node, const Ogre::Vector3& _position, unsigned int _duration)
	{
		return chamber()->add_action(new SceneNodeMover(node, _position, ch::milliseconds(_duration)));
	}
	
	
	struct SceneNodeRotator: Action
	{
		Ogre::SceneNode* const node;
		const double duration;
		double progress;
		Ogre::Degree degree;
		const Ogre::Vector3 axis;
	
		SceneNodeRotator(Ogre::SceneNode* node, const Ogre::Degree& _degree, const Ogre::Vector3& _axis, const ch::milliseconds& _duration)
			: node(node)
			, duration(ch::duration_cast<ch::duration<double>>(_duration).count())
			, progress(0)
			, axis(_axis)
			, degree(_degree)
		{
		}
		
		virtual bool update(double dt) override
		{
			progress += dt;	
			node->rotate(axis, degree * (dt / duration));
			return progress < duration;
		}
	};
	
	ActionToken rotateOgreEntity(Ogre::SceneNode* node, const Ogre::Degree& _degree, const Ogre::Vector3& _axis, unsigned int _duration)
	{
		return chamber()->add_action(new SceneNodeRotator(node, _degree, _axis, ch::milliseconds(_duration)));
	}
	
	void node_lookAt(Ogre::SceneNode* node, const Ogre::Vector3& pos)
	{
		node_setDirection(node, node->getPosition() - pos);
	}
	
	void node_setDirection(Ogre::SceneNode* node, const Ogre::Vector3& dir)
	{
		node->setDirection(dir);
	}
	
	const Ogre::Vector3& player_head()
	{
		auto skel = mState->mSkeleton;
		return skel->getHead().pos;
	}
	
	float player_height()
	{
		const auto& rightFoot = mState->mSkeleton->getJoint(XN_SKEL_RIGHT_FOOT).pos;
		const auto& leftFoot = mState->mSkeleton->getJoint(XN_SKEL_LEFT_FOOT).pos;
		
		return player_head().y - 0.5 * (rightFoot.y + leftFoot.y);
	}
	
	const Ogre::Vector3& player_hand(bool right = true)
	{
		PhysicHand& hand = right? mState->mPhysicEngine->rightHand : mState->mPhysicEngine->leftHand;
		return hand.pos;
	}
	
	const Ogre::Vector3& body_orientation()
	{
		const auto& rightShoulder = mState->mSkeleton->getJoint(XN_SKEL_RIGHT_SHOULDER).pos;
		const auto& leftShoulder = mState->mSkeleton->getJoint(XN_SKEL_LEFT_SHOULDER).pos;
		
		static auto result = (rightShoulder - leftShoulder).crossProduct(Ogre::Vector3::UNIT_Y);
		return result;
	}
	
	void setAssistanceMode(ThrowAssistence::AjustmentMode mode)
	{
		mState->mThrowAssistence.ajust.mode = mode;
	}
	
	ThrowAssistence::AjustmentMode getAssistanceMode()
	{
		return mState->mThrowAssistence.ajust.mode;
	}
	
	int random()
	{
		return rand();
	}
};


void CourseState::init_bindings()
{
	assert(mChamber);
	
	asIScriptEngine* engine = mChamber->getEngine();
	mController = new ChamberController(this);
	
	static_assert(std::is_same<Ogre::Real, float>::value, "Ogre::Real must be float");
	engine->RegisterTypedef("real", "float");
	
	engine->RegisterGlobalFunction("void Quit(bool _force = true)", asMETHOD(ScriptChamber, quit), asCALL_THISCALL_ASGLOBAL, mChamber.get());
	
	// Kinect
	engine->RegisterGlobalFunction("ActionToken InitKinect()", asMETHOD(ChamberController, initKinect), asCALL_THISCALL_ASGLOBAL, mController);
	
	// Action helpers
	engine->RegisterGlobalFunction("ActionToken OpenCustomAction()", asMETHOD(ChamberController, openCustomAction), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("void CloseCustomAction(ActionToken token)", asMETHOD(ChamberController, closeCustomAction), asCALL_THISCALL_ASGLOBAL, mController);
	
	// ogre objects
	registerOgreObjects(engine);
	
	// Scene node
	engine->RegisterObjectType("SceneNode", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterGlobalFunction("SceneNode@ GetSceneNode(const string& in name)", asMETHOD(ChamberController, getSceneNodeByName), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterObjectMethod("SceneNode", "void visible(bool, bool cascade = true)", asMETHOD(Ogre::SceneNode, setVisible), asCALL_THISCALL);
	engine->RegisterObjectMethod("SceneNode", "void position(real x, real y, real z)", asMETHODPR(Ogre::SceneNode, setPosition, (Ogre::Real, Ogre::Real, Ogre::Real), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("SceneNode", "void position(const vector &in pos)", asMETHODPR(Ogre::SceneNode, setPosition, (const Ogre::Vector3&), void), asCALL_THISCALL);
	engine->RegisterObjectMethod("SceneNode", "const vector& position() const", asMETHOD(Ogre::SceneNode, getPosition), asCALL_THISCALL);
	
	engine->RegisterObjectMethod("SceneNode", "ActionToken move(const vector &in pos, uint milli)", asMETHOD(ChamberController, moveOgreEntity), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("SceneNode", "ActionToken rotate(const degree &in deg, const vector &in axis, uint milli)", asMETHOD(ChamberController, rotateOgreEntity), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("SceneNode", "void lookAt(const vector &in pos)", asMETHOD(ChamberController, node_lookAt), asCALL_THISCALL_OBJFIRST, mController);
	
	// mesh creation
	engine->RegisterGlobalFunction("void CreateSphereMesh(const string &in name, float radius)", asMETHOD(ChamberController, createSphereMesh), asCALL_THISCALL_ASGLOBAL, mController);
	
	//object creation
	engine->RegisterGlobalFunction("SceneNode@ CreateGround(const string &in name, const string &in material)", asMETHOD(ChamberController, createGround), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("SceneNode@ CreateMesh(const string &in name, const string &in mesh)", asMETHOD(ChamberController, createMesh), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("SceneNode@ CreateLight(const string &in name)", asMETHOD(ChamberController, createLight), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("SceneNode@ CreateLineEntity(const string &in name, array<vector>@ lines, const string &in material)", asMETHOD(ChamberController, createLineEntity), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("SceneNode@ CreateText(const string &in name, const string &in font, bool _static = false)", asMETHOD(ChamberController, createText), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("SceneNode@ CreateParticleSystem(const string &in name, const string &in templ)", asMETHOD(ChamberController, createParticleSystem), asCALL_THISCALL_ASGLOBAL, mController);
	
	// object manipulation
	engine->RegisterObjectMethod("SceneNode", "void setText(const string &in txt)", asMETHOD(ChamberController, changeText), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("SceneNode", "void setTextSize(float size)", asMETHOD(ChamberController, changeTextSize), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterEnum("TextVAlignment");
	engine->RegisterEnumValue("TextVAlignment", "TVA_Above", Ogre::MovableText::V_ABOVE);
	engine->RegisterEnumValue("TextVAlignment", "TVA_Center", Ogre::MovableText::V_CENTER);
	engine->RegisterEnumValue("TextVAlignment", "TVA_Below", Ogre::MovableText::V_BELOW);
	engine->RegisterEnum("TextHAlignment");
	engine->RegisterEnumValue("TextHAlignment", "THA_Center", Ogre::MovableText::H_CENTER);
	engine->RegisterEnumValue("TextHAlignment", "THA_Left", Ogre::MovableText::H_LEFT);
	engine->RegisterObjectMethod("SceneNode", "void setTextAlignment(TextHAlignment ha, TextVAlignment va = TVA_Center)", asMETHOD(ChamberController, changeAlign), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("SceneNode", "void setMaterialName(const string &in name)", asMETHOD(ChamberController, changeMaterialName), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("SceneNode", "void firePS()", asMETHOD(ChamberController, fireParticelSystem), asCALL_THISCALL_OBJFIRST, mController);
	
	engine->RegisterGlobalFunction("void DeleteSceneNode(SceneNode@ node)", asMETHOD(ChamberController, deleteSceneNode), asCALL_THISCALL_ASGLOBAL, mController);
	
	// Arrow
	engine->RegisterObjectType("Arrow", sizeof(Arrow), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CAK);
	engine->RegisterObjectBehaviour("Arrow", asBEHAVE_CONSTRUCT, "void f(const string &in name, bool is3D = true)", asMETHOD(ChamberController, constructArrow), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("Arrow", "void update(bool force = true)", asMETHOD(Arrow, update), asCALL_THISCALL);
	engine->RegisterObjectProperty("Arrow", "string material", asOFFSET(Arrow, materialName));
	engine->RegisterObjectProperty("Arrow", "vector faceDirection", asOFFSET(Arrow, faceDirection));
	engine->RegisterObjectProperty("Arrow", "float widthFactor", asOFFSET(Arrow, widthFactor));
	engine->RegisterObjectMethod("Arrow", "SceneNode@ getSceneNode() const", asMETHOD(ChamberController, arrow_getSceneNode), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("Arrow", "void point_straight(const vector &in from, const vector &in to)", asMETHOD(ChamberController, arrow_linegen), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("Arrow", "void point_circle(const vector &in from, const vector &in to, float height = 0.0f, const vector &in arch = vector(0, 1, 0), float middle = 0.5f)", asMETHOD(ChamberController, arrow_circle_gen), asCALL_THISCALL_OBJFIRST, mController);
	
	// Ball
	engine->RegisterObjectType("Ball", 0, asOBJ_REF | asOBJ_NOCOUNT);
	engine->RegisterGlobalFunction("Ball@ CreateBall(SceneNode@ node, float radius)", asMETHOD(ChamberController, createBall), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("void DeleteBall(Ball@ ball)", asMETHOD(ChamberController, deleteBall), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterObjectProperty("Ball", "vector movement", asOFFSET(PhysicBall, movement));
	engine->RegisterObjectProperty("Ball", "vector position", asOFFSET(PhysicBall, pos));
	engine->RegisterObjectProperty("Ball", "float radius", asOFFSET(PhysicBall, radius));
	engine->RegisterObjectMethod("Ball", "void explode()", asMETHOD(PhysicBall, explode), asCALL_THISCALL);
	engine->RegisterObjectMethod("Ball", "void toHand(bool rightHand = true)", asMETHOD(ChamberController, ballToHand), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("Ball", "bool inHand(bool rightHand = true)", asMETHOD(ChamberController, isBallInHand), asCALL_THISCALL_OBJFIRST, mController);
	
	// ball controller
	engine->RegisterInterface("IBallController");
	engine->RegisterInterfaceMethod("IBallController", "void apply(Ball@ node, float dt)");
	engine->RegisterObjectMethod("Ball", "void addController(IBallController@ controller)", asMETHOD(ChamberController, addScriptController), asCALL_THISCALL_OBJFIRST, mController);
	engine->RegisterObjectMethod("Ball", "void addExplosionController(IBallController@ controller)", asMETHOD(ChamberController, addExplosionScriptController), asCALL_THISCALL_OBJFIRST, mController);	
	
	// physics
	engine->RegisterGlobalProperty("vector Gravitation", &mPhysicEngine->gravitation);
	engine->RegisterGlobalProperty("float TimeFactor", &mTimeFactor);
	
	// some skeleton informations
	engine->RegisterGlobalFunction("const vector& GetPlayerHead()", asMETHOD(ChamberController, player_head), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("float GetPlayerHeight()", asMETHOD(ChamberController, player_height), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("const vector& GetPlayerOrientation()", asMETHOD(ChamberController, body_orientation), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("const vector& GetPlayerHand(bool right = true)", asMETHOD(ChamberController, player_hand), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalProperty("vector PlayerOffset", &mSkeleton->offset);
	
	// throw assistance
	engine->RegisterEnum("ThrowAssistMode");
	engine->RegisterEnumValue("ThrowAssistMode", "TAM_None", ThrowAssistence::None);
	engine->RegisterEnumValue("ThrowAssistMode", "TAM_Axis", ThrowAssistence::Axis);
	engine->RegisterEnumValue("ThrowAssistMode", "TAM_Plane", ThrowAssistence::Plane);
	engine->RegisterEnumValue("ThrowAssistMode", "TAM_Full", ThrowAssistence::Parable);
	engine->RegisterGlobalFunction("void SetAssistanceMode(ThrowAssistMode mode)", asMETHOD(ChamberController, setAssistanceMode), asCALL_THISCALL_ASGLOBAL, mController);
	engine->RegisterGlobalFunction("ThrowAssistMode GetAssistanceMode()", asMETHOD(ChamberController, getAssistanceMode), asCALL_THISCALL_ASGLOBAL, mController);
	
	// Sound
	engine->RegisterGlobalFunction("ActionToken PlaySound(const string &in file, float volume = 100.0f)", asMETHOD(ChamberController, playSound), asCALL_THISCALL_ASGLOBAL, mController);
	
	// Random
	engine->RegisterGlobalFunction("int Rand()", asMETHOD(ChamberController, random), asCALL_THISCALL_ASGLOBAL, mController);
	srand(time(nullptr));
}


void CourseState::ball_catched(PhysicBall* ball, PhysicHand* hand)
{
	mController->playSound("catch.ogg", 100.0f);
}




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// creates a manual sphere
void CourseState::createSphere(const std::string& strName, const float r, const Ogre::ColourValue& c, const int nRings, const int nSegments)
 {
	 using namespace Ogre;
     MeshPtr pSphere = MeshManager::getSingleton().createManual(strName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
     SubMesh *pSphereVertex = pSphere->createSubMesh();
 
     pSphere->sharedVertexData = new VertexData();
     VertexData* vertexData = pSphere->sharedVertexData;
 
     // define the vertex format
     VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
     size_t currOffset = 0;
     // positions
     vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_POSITION);
     currOffset += VertexElement::getTypeSize(VET_FLOAT3);
     // normals
     vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_NORMAL);
     currOffset += VertexElement::getTypeSize(VET_FLOAT3);
     // two dimensional texture coordinates
     vertexDecl->addElement(0, currOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
     currOffset += VertexElement::getTypeSize(VET_FLOAT2);
	 // color
     vertexDecl->addElement(0, currOffset, VET_COLOUR_ABGR, VES_DIFFUSE, 0);
     currOffset += VertexElement::getTypeSize(VET_COLOUR_ABGR);
 
     // allocate the vertex buffer
     vertexData->vertexCount = (nRings + 1) * (nSegments+1);
     HardwareVertexBufferSharedPtr vBuf = HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
     VertexBufferBinding* binding = vertexData->vertexBufferBinding;
     binding->setBinding(0, vBuf);
     float* pVertex = static_cast<float*>(vBuf->lock(HardwareBuffer::HBL_DISCARD));
 
     // allocate index buffer
     pSphereVertex->indexData->indexCount = 6 * nRings * (nSegments + 1);
     pSphereVertex->indexData->indexBuffer = HardwareBufferManager::getSingleton().createIndexBuffer(HardwareIndexBuffer::IT_16BIT, pSphereVertex->indexData->indexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
     HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
     unsigned short* pIndices = static_cast<unsigned short*>(iBuf->lock(HardwareBuffer::HBL_DISCARD));
 
     float fDeltaRingAngle = (Math::PI / nRings);
     float fDeltaSegAngle = (2 * Math::PI / nSegments);
     unsigned short wVerticeIndex = 0 ;
 
     // Generate the group of rings for the sphere
     for( int ring = 0; ring <= nRings; ring++ ) {
         float r0 = r * sinf (ring * fDeltaRingAngle);
         float y0 = r * cosf (ring * fDeltaRingAngle);
 
         // Generate the group of segments for the current ring
         for(int seg = 0; seg <= nSegments; seg++) {
             float x0 = r0 * sinf(seg * fDeltaSegAngle);
             float z0 = r0 * cosf(seg * fDeltaSegAngle);
 
             // Add one vertex to the strip which makes up the sphere
             *pVertex++ = x0;
             *pVertex++ = y0;
             *pVertex++ = z0;
 
             Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
             *pVertex++ = vNormal.x;
             *pVertex++ = vNormal.y;
             *pVertex++ = vNormal.z;
 
             *pVertex++ = (float) seg / (float) nSegments;
             *pVertex++ = (float) ring / (float) nRings;
 
			*reinterpret_cast<ABGR*>(pVertex++) = c.getAsABGR();
			
             if (ring != nRings) {
                                // each vertex (except the last) has six indices pointing to it
                 *pIndices++ = wVerticeIndex + nSegments + 1;
                 *pIndices++ = wVerticeIndex;               
                 *pIndices++ = wVerticeIndex + nSegments;
                 *pIndices++ = wVerticeIndex + nSegments + 1;
                 *pIndices++ = wVerticeIndex + 1;
                 *pIndices++ = wVerticeIndex;
                 wVerticeIndex ++;
             }
         }; // end for seg
     } // end for ring
 
     // Unlock
     vBuf->unlock();
     iBuf->unlock();
     // Generate face list
     pSphereVertex->useSharedVertices = true;
 
     // the original code was missing this line:
     pSphere->_setBounds( AxisAlignedBox( Vector3(-r, -r, -r), Vector3(r, r, r) ), false );
     pSphere->_setBoundingSphereRadius(r);
         // this line makes clear the mesh is loaded (avoids memory leaks)
         pSphere->load();
  }