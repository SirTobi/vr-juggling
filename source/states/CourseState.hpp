/*
 * 
 */

#ifndef COURSESTATE_H
#define COURSESTATE_H

#  include <OISEvents.h>
#  include <OISInputManager.h>
#  include <OISKeyboard.h>
#  include <OISMouse.h>
 
#  include <SdkTrays.h>
#  include <SdkCameraMan.h>
#include <rapidxml.hpp>

#include "framework/AppState.hpp"
#include "script_chamber/ScriptChamber.hpp"
#include "physics/ThrowAssistence.hpp"

class Skeleton;
class PhysicEngine;
class Oculus;
class PhysicBall;
class PhysicHand;

class CourseState : public AppState
{
	struct ChamberController;
public:
	DECLARE_APPSTATE_CLASS(CourseState)
	
	CourseState(rapidxml::xml_node<>& config);
	~CourseState();

	void enter();
	void createScene();
	void exit();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void buttonHit(OgreBites::Button* button);

	void update(double timeSinceLastFrame);
	
private:
	void init_bindings();
	void toggleGrab(const OIS::MouseButtonID id, bool grab);
	void destroyAllAttachedMovableObjects( Ogre::SceneNode* i_pSceneNode );
	void ball_released(PhysicBall* ball, PhysicHand* hand);
	void ball_catched(PhysicBall* ball, PhysicHand* hand);
	void createSphere(const std::string& strName, const float r, const Ogre::ColourValue& c = Ogre::ColourValue::White, const int nRings = 16, const int nSegments = 16);
private:
	Ogre::Camera* mCamera;
	std::unique_ptr<Oculus> mOculus;
	OgreBites::SdkCameraMan* mCameraMan;
	Ogre::SceneManager*		mSceneMgr;
	OgreFramework*			mFramework;
	
	ChamberController* mController;
	Skeleton* mSkeleton;
	ThrowAssistence mThrowAssistence;
	float mTimeFactor;
	
	rapidxml::xml_node<>& mConfig;
	std::unique_ptr<ScriptChamber> mChamber;
	std::unique_ptr<PhysicEngine> mPhysicEngine;
};

#endif // COURSESTATE_H
