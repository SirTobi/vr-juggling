/*
 * 
 */

#ifndef CALIBRATIONSTATE_H
#define CALIBRATIONSTATE_H

#include "framework/AppState.hpp"


class CalibrationState : public AppState
{
public:
	DECLARE_APPSTATE_CLASS(CalibrationState)
	
	CalibrationState();
	~CalibrationState();

	void enter();
	void createScene();
	void exit();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void buttonHit(OgreBites::Button* button);

	void update(double timeSinceLastFrame);
};

#endif // CALIBRATIONSTATE_H
