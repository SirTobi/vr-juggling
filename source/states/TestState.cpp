/*
 * 
 */
#include <ctime>
#include <OgreTechnique.h>
#include <OgreMeshManager.h>
#include <OgreParticleSystem.h>
#include <OgreHardwareBuffer.h>
#include <OgreMesh.h>
#include <OgreSubMesh.h>
#include <OgreSkeletonInstance.h>
#include "utils/LexicalCasts.hpp"
#include "utils/MovableText.hpp"
#include <rxml/value.hpp>
#include "TestState.hpp"
#include "Skeleton.hpp"
#include "MainApp.hpp"
#include "KinectManager.hpp"
#include "SkeletonProvider.hpp"

#include "physics/controller/SimpleControllers.hpp"
#include "physics/controller/ExplosionController.hpp"

template<typename T>
T TestState::config_value(const std::string& path)
{
	return boost::lexical_cast<T>(rxml::value(mStateConfig, path));
}


TestState::TestState(rapidxml::xml_node<>& config)
	: mFramework(OgreFramework::getSingletonPtr())
	, mCamera(nullptr)
	, mCameraMan(nullptr)
	, mSceneMgr(nullptr)
	, mStateConfig(config)
	, throwAssist(rxml::getnode(config, "throw_assistance"))
{
}

TestState::~TestState()
{

}

/**********************************************************/

void TestState::enter()
{
    mSceneMgr = mFramework->root()->createSceneManager(Ogre::ST_GENERIC, "TestSceneMgr");
    OgreFramework::getSingletonPtr()->log()->logMessage("Enter TestState...");

	physicEngine = new PhysicEngine(mSceneMgr, rxml::getnode(mStateConfig, "environment/physics"));
	
    createScene();
}

/**********************************************************/

void TestState::createScene()
{
	using Ogre::Vector3;
	typedef Ogre::ColourValue Color;
 	//mSceneMgr->setFog(Ogre::FOG_LINEAR, Ogre::ColourValue(0, 0, 0), 0.002, 30, 100);
	
	Ogre::Light* light = mSceneMgr->createLight("tstLight");
    light->setType(Ogre::Light::LT_POINT);
    light->setPosition(2, 2, 0); 	
    light->setDiffuseColour(Ogre::ColourValue(0, 0.5, 0));
    light->setSpecularColour(Ogre::ColourValue(1, 0, 0));
	
 
    mSceneMgr->setAmbientLight(Color(0.6, 0.6, 0.6));
	
	bool use_oculus_rift = config_value<BoolAlpha>("oculus_rift:enabled");
	mOculus.reset(new Oculus());
 	if(use_oculus_rift && mOculus->setupOculus())
	{
		mOculus->setupOgre(mSceneMgr, mFramework->renderWindow());
		mOculus->getCameraNode()->setPosition(1, 2, -0.3);
	}else{
		mOculus.reset();
		// Camera
		mCamera = mSceneMgr->createCamera("FreeLookCam");
		mCamera->setPosition(-2, 3, -1);
		mCamera->setNearClipDistance(0.0000001);
		mCamera->setFarClipDistance(45);
		mCamera->lookAt(0, 3, 4);
		mCamera->setAspectRatio(Ogre::Real(OgreFramework::getSingletonPtr()->viewport()->getActualWidth()) /
			Ogre::Real(mFramework->viewport()->getActualHeight()));
		
		mCameraMan = new OgreBites::SdkCameraMan(mCamera);
		mCameraMan->setStyle(OgreBites::CS_FREELOOK);
		mCameraMan->setTopSpeed(1);
	}
	
	
    mFramework->viewport()->setCamera(mCamera);
	// Environment
	mLines = new DynamicLines();
	mLines->setMaterial("Pointcloud");
	//mLines->getTechnique()->setFog(true, Ogre::FOG_LINEAR, Ogre::ColourValue(0, 1, 0), 0, 30, 100);
	mLines->addLine(Vector3(2, 0, 0), Vector3(-2, 0, 0));
	mLines->addLine(Vector3(2, 0, 1), Vector3(-2, 0, 1));
	mLines->addLine(Vector3(2, 0, 2), Vector3(-2, 0, 2));
	mLines->addLine(Vector3(2, 0, 3), Vector3(-2, 0, 3));
	mLines->addLine(Vector3(2, 0, 4), Vector3(-2, 0, 4));
	
	mLines->addLine(Vector3(2, 4, 0), Vector3(2, 0, 0));
	mLines->addLine(Vector3(-2, 4, 0), Vector3(-2, 0, 0));
	mLines->addLine(Vector3(2, 4, 4), Vector3(2, 0, 4));
	mLines->addLine(Vector3(-2, 4, 4), Vector3(-2, 0, 4));
	
	mLines->addLine(Vector3(-2, 0, 0), Vector3(-2, 0, 4));
	mLines->addLine(Vector3(-1, 0, 0), Vector3(-1, 0, 4));
	mLines->addLine(Vector3(0, 0, 0), Vector3(0, 0, 4));
	mLines->addLine(Vector3(1, 0, 0), Vector3(1, 0, 4));
	mLines->addLine(Vector3(2, 0, 0), Vector3(2, 0,4));
	
	mLines->update();
	Ogre::SceneNode* linesNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Environment_Lines");
	linesNode->attachObject(mLines);
	
	// Ground
	Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
 
    Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        plane, 500, 500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
 
    Ogre::Entity* entGround = mSceneMgr->createEntity("GroundEntity", "ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(entGround);
	entGround->setMaterialName("Test/Ground");	
    entGround->setCastShadows(false);
	
	
	
	// skeleton
	mSkeleton = new Skeleton(rxml::getnode(mStateConfig, "skeleton"));
	mSkeleton->color = Color::Blue;
	
	mSkeleton->update();
	Ogre::SceneNode* skeletonNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Skeleton");
	skeletonNode->attachObject(mSkeleton);
	
	// Ball
	float radius = config_value<float>("balls:radius");
	int num_balls = config_value<int>("balls:count");
	
	std::vector<Color> colors;
	for(int c = 1; c < 8; ++c)
		colors.push_back(Color(c & 1, (c & 2) >> 1, (c & 4) >> 2));
	
	srand(time(nullptr));
	
	int col_idx = rand();
	while(num_balls--)
	{
		std::string sphere_name = "juggleball-" + std::to_string(num_balls);
		createSphere(sphere_name, radius, colors[col_idx++ % colors.size()]);
		Ogre::Entity* sphere = mSceneMgr->createEntity("juggleball " + std::to_string(num_balls), sphere_name);
		sphere->setMaterialName("BaseWhiteNoLighting");
		auto* ball_node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		ball_node->attachObject(sphere);
		ball_node->setPosition(0, num_balls + 10, num_balls);
		auto* ball = physicEngine->createBall(ball_node, radius);
		ball->update_interval = config_value<double>("balls:update_interval");
		
		// contoller
		ball->controllers.push_back(std::make_shared<BallAtGroundController>(0));
		ball->explosion_controllers.push_back(std::make_shared<BallToHandController>(&(physicEngine->rightHand.pos), Vector3(0, 2, 0)));
		
		// every ball gets its own particle system
		auto* ps = mSceneMgr->createParticleSystem("expolsion_for_ball_" + std::to_string(num_balls), "explosionTemplate");
		//ps->setEmitting(false);
		Ogre::SceneNode* ps_node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		ps_node->attachObject(ps);
		ball->explosion_controllers.push_front(std::make_shared<ExplosionController>(ps));
	}
	
	// hands
	{
		// left hand
		auto e = mSceneMgr->createEntity("left hand", "Hand.mesh");
		auto sn = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(e);
		sn->setScale(Vector3(config_value<float>("hand_size:value")));
		sn->scale(-1, -1, 1);
		physicEngine->leftHand.setSceneNode(sn, e);
		
		// right hand
		e = mSceneMgr->createEntity("right hand", "Hand.mesh");
		sn = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		sn->attachObject(e);
		sn->setScale(Vector3(config_value<float>("hand_size:value")));
		sn->scale(1, -1, 1);
		physicEngine->rightHand.setSceneNode(sn, e);
	}
	
    using std::placeholders::_1;
    using std::placeholders::_2;
	physicEngine->rightHand.release_event = std::bind(&TestState::ball_released, this, std::placeholders::_1, std::placeholders::_2);
	physicEngine->leftHand.release_event = std::bind(&TestState::ball_released, this, std::placeholders::_1, std::placeholders::_2);
	
	// add some text
	{
		auto text = new Ogre::MovableText("text-entity", "Hallo\nich bin test\n222", "DescriptionFont2", 0.3);
		auto text_node = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		text_node->attachObject(text);
		text->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER);
		text->setStaticOrientation(true);
		text_node->setPosition(0, 0, 0);
	}
	
}

/**********************************************************/

void TestState::exit()
{
    mFramework->log()->logMessage("Leaving TestState...");

	if(mCameraMan)	
		delete mCameraMan;
	if(mCamera)
		mSceneMgr->destroyCamera(mCamera);
    if(mSceneMgr)
        mFramework->root()->destroySceneManager(mSceneMgr);

    mFramework->trayMgr()->clearAllTrays();
    mFramework->trayMgr()->destroyAllWidgets();
    mFramework->trayMgr()->setListener(0);
}

/**********************************************************/

bool TestState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	if(mCameraMan)
		mCameraMan->injectKeyDown(keyEventRef);
    return OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
}

/**********************************************************/

bool TestState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	if(mCameraMan)
	{
		mCameraMan->injectKeyUp(keyEventRef);
		mCameraMan->manualStop();
	}
	
	switch(keyEventRef.key)
	{
	case OIS::KC_ESCAPE:
		shutdown();
		break;
	case OIS::KC_M:
		throwAssist.ajust.nextMode();
		break;
	case OIS::KC_SPACE:
		if(mOculus)
			mOculus->resetOrientation();
		break;
	default:
		return OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
	};
	
	return true;
}

/**********************************************************/

bool TestState::mouseMoved(const OIS::MouseEvent &evt)
{
	if(mCameraMan)
		mCameraMan->injectMouseMove(evt);
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseMove(evt))
		return true;
    return true;
}

/**********************************************************/


bool TestState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	{
		PhysicHand* hand = nullptr;
		if(id == OIS::MouseButtonID::MB_Left)
			hand = &(physicEngine->leftHand);
		
		if(id == OIS::MouseButtonID::MB_Right)
			hand = &(physicEngine->rightHand);
		
		
		if(hand)
		{
			if(config_value<BoolAlpha>("controls/flip_mouse_buttons:enabled"))
				hand = hand->other;
			hand->grab(true);
		}
	}
	
	std::cout << "Pressed " <<  id << std::endl;
	
    if(mFramework->trayMgr()->injectMouseDown(evt, id))
		return true;
    return true;
}

/**********************************************************/

bool TestState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	
	{
		PhysicHand* hand = nullptr;
		if(id == OIS::MouseButtonID::MB_Left)
			hand = &(physicEngine->leftHand);
		
		if(id == OIS::MouseButtonID::MB_Right)
			hand = &(physicEngine->rightHand);
		
		
		if(hand)
		{
			if(config_value<BoolAlpha>("controls/flip_mouse_buttons:enabled"))
				hand = hand->other;
			
			hand->grab(false);
		}
	}
	
	
	std::cout << "Released " <<  id << std::endl;
	
	
    if(mFramework->trayMgr()->injectMouseUp(evt, id))
		return true;
    return true;
}

/**********************************************************/

void TestState::update(double timeSinceLastFrame)
{
	auto& kinect = MainApp::getSingletonPtr()->kinectManager();
	auto& sk_provider = kinect->skeleton_provider();
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
	
	// skeleton
	bool sk_updated = false; //sk_provider.update();
	if(sk_updated)
	{
		mSkeleton->apply(sk_provider);
	}
	
	// update camera
	if(!mOculus)
	{
		mCameraMan->frameRenderingQueued(m_FrameEvent);
	} else {
		Ogre::SceneNode* cam_node = mOculus->getCameraNode();
		
		//if(mSkeleton->getHead().confidence > 0.0f)
		cam_node->setPosition(mSkeleton->getHead().pos);
		if(mOculus->isOculusReady())
			cam_node->setOrientation(mOculus->getOrientation());
	}
	
	// physics
	physicEngine->rightHand.pos = mSkeleton->getHand(Skeleton::Right).pos;
	physicEngine->rightHand.direction = physicEngine->rightHand.pos - mSkeleton->getJoint(XN_SKEL_RIGHT_ELBOW).pos;
	physicEngine->rightHand.direction.normalise();
	physicEngine->leftHand.pos = mSkeleton->getHand(Skeleton::Left).pos;
	physicEngine->leftHand.direction = physicEngine->leftHand.pos - mSkeleton->getJoint(XN_SKEL_LEFT_ELBOW).pos;
	physicEngine->leftHand.direction.normalise();
	physicEngine->update(timeSinceLastFrame);
	
	
	// tray
    OgreFramework::getSingletonPtr()->trayMgr()->frameRenderingQueued(m_FrameEvent);
}

/**********************************************************/

void TestState::buttonHit(OgreBites::Button *button)
{
}

void TestState::ball_released(PhysicBall* ball, PhysicHand* hand)
{
	auto planeDir = mSkeleton->getHand(Skeleton::Right).pos - mSkeleton->getHand(Skeleton::Left).pos;
	throwAssist.apply(ball, planeDir, hand->other->pos);
}

void TestState::createSphere(const std::string& strName, const float r, const Ogre::ColourValue& c, const int nRings, const int nSegments)
 {
	 using namespace Ogre;
     MeshPtr pSphere = MeshManager::getSingleton().createManual(strName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
     SubMesh *pSphereVertex = pSphere->createSubMesh();
 
     pSphere->sharedVertexData = new VertexData();
     VertexData* vertexData = pSphere->sharedVertexData;
 
     // define the vertex format
     VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
     size_t currOffset = 0;
     // positions
     vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_POSITION);
     currOffset += VertexElement::getTypeSize(VET_FLOAT3);
     // normals
     vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_NORMAL);
     currOffset += VertexElement::getTypeSize(VET_FLOAT3);
     // two dimensional texture coordinates
     vertexDecl->addElement(0, currOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
     currOffset += VertexElement::getTypeSize(VET_FLOAT2);
	 // color
     vertexDecl->addElement(0, currOffset, VET_COLOUR_ABGR, VES_DIFFUSE, 0);
     currOffset += VertexElement::getTypeSize(VET_COLOUR_ABGR);
 
     // allocate the vertex buffer
     vertexData->vertexCount = (nRings + 1) * (nSegments+1);
     HardwareVertexBufferSharedPtr vBuf = HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
     VertexBufferBinding* binding = vertexData->vertexBufferBinding;
     binding->setBinding(0, vBuf);
     float* pVertex = static_cast<float*>(vBuf->lock(HardwareBuffer::HBL_DISCARD));
 
     // allocate index buffer
     pSphereVertex->indexData->indexCount = 6 * nRings * (nSegments + 1);
     pSphereVertex->indexData->indexBuffer = HardwareBufferManager::getSingleton().createIndexBuffer(HardwareIndexBuffer::IT_16BIT, pSphereVertex->indexData->indexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
     HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
     unsigned short* pIndices = static_cast<unsigned short*>(iBuf->lock(HardwareBuffer::HBL_DISCARD));
 
     float fDeltaRingAngle = (Math::PI / nRings);
     float fDeltaSegAngle = (2 * Math::PI / nSegments);
     unsigned short wVerticeIndex = 0 ;
 
     // Generate the group of rings for the sphere
     for( int ring = 0; ring <= nRings; ring++ ) {
         float r0 = r * sinf (ring * fDeltaRingAngle);
         float y0 = r * cosf (ring * fDeltaRingAngle);
 
         // Generate the group of segments for the current ring
         for(int seg = 0; seg <= nSegments; seg++) {
             float x0 = r0 * sinf(seg * fDeltaSegAngle);
             float z0 = r0 * cosf(seg * fDeltaSegAngle);
 
             // Add one vertex to the strip which makes up the sphere
             *pVertex++ = x0;
             *pVertex++ = y0;
             *pVertex++ = z0;
 
             Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
             *pVertex++ = vNormal.x;
             *pVertex++ = vNormal.y;
             *pVertex++ = vNormal.z;
 
             *pVertex++ = (float) seg / (float) nSegments;
             *pVertex++ = (float) ring / (float) nRings;
 
			*reinterpret_cast<ABGR*>(pVertex++) = c.getAsABGR();
			
             if (ring != nRings) {
                                // each vertex (except the last) has six indices pointing to it
                 *pIndices++ = wVerticeIndex + nSegments + 1;
                 *pIndices++ = wVerticeIndex;               
                 *pIndices++ = wVerticeIndex + nSegments;
                 *pIndices++ = wVerticeIndex + nSegments + 1;
                 *pIndices++ = wVerticeIndex + 1;
                 *pIndices++ = wVerticeIndex;
                 wVerticeIndex ++;
             }
         }; // end for seg
     } // end for ring
 
     // Unlock
     vBuf->unlock();
     iBuf->unlock();
     // Generate face list
     pSphereVertex->useSharedVertices = true;
 
     // the original code was missing this line:
     pSphere->_setBounds( AxisAlignedBox( Vector3(-r, -r, -r), Vector3(r, r, r) ), false );
     pSphere->_setBoundingSphereRadius(r);
         // this line makes clear the mesh is loaded (avoids memory leaks)
         pSphere->load();
  }