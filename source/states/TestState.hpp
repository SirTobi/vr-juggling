/*
 * 
 */

#ifndef TESTSTATE_H
#define TESTSTATE_H

#  include <OISEvents.h>
#  include <OISInputManager.h>
#  include <OISKeyboard.h>
#  include <OISMouse.h>
 
#  include <SdkTrays.h>
#  include <SdkCameraMan.h>

#include "rapidxml.hpp"
#include "framework/AppState.hpp"
#include "utils/DynamicLines.hpp"
#include "Skeleton.hpp"
#include "physics/PhysicEngine.hpp"
#include "OgreOculus.hpp"
#include "physics/ThrowAssistence.hpp"


class TestState : public AppState
{
public:
	DECLARE_APPSTATE_CLASS(TestState)
	
	TestState(rapidxml::xml_node<>& config);
	~TestState();

	void enter();
	void createScene();
	void exit();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void buttonHit(OgreBites::Button* button);

	void update(double timeSinceLastFrame);
	
private:
	template<typename T>
	T config_value(const std::string& path);
	
	void ball_released(PhysicBall* ball, PhysicHand* hand);
	
	void createSphere(const std::string& strName, const float r, const Ogre::ColourValue& c, const int nRings = 16, const int nSegments = 16);
private:
	rapidxml::xml_node<>& mStateConfig;
	
	Ogre::Camera* mCamera;
	OgreBites::SdkCameraMan* mCameraMan;
	Ogre::SceneManager*		mSceneMgr;
	DynamicLines* mLines;
	Skeleton* mSkeleton;
	std::unique_ptr<Oculus> mOculus;
	OgreFramework* mFramework;
	PhysicEngine* physicEngine;
	ThrowAssistence throwAssist;
};

#endif // CALIBRATIONSTATE_H
