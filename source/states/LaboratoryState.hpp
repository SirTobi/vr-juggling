/*
 * 
 */

#ifndef LABORATORYSTATE_H
#define LABORATORYSTATE_H

#  include <OISEvents.h>
#  include <OISInputManager.h>
#  include <OISKeyboard.h>
#  include <OISMouse.h>
 
#  include <SdkTrays.h>
#  include <SdkCameraMan.h>

#include "framework/AppState.hpp"
#include <utils/Arrow.hpp>
#include "rapidxml.hpp"


class LaboratoryState : public AppState
{
public:
	DECLARE_APPSTATE_CLASS(LaboratoryState)
	
	LaboratoryState(rapidxml::xml_node<>& config);
	~LaboratoryState();

	void enter();
	void createScene();
	void exit();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &evt);
	bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

	void buttonHit(OgreBites::Button* button);

	void update(double timeSinceLastFrame);
	
private:
	Ogre::Camera* mCamera;
	OgreBites::SdkCameraMan* mCameraMan;
	Ogre::SceneManager*		mSceneMgr;
	OgreFramework*			mFramework;
	
	std::unique_ptr<Arrow>	mArrow;
	std::shared_ptr<ArrowFromCircle> mArrowGen;
};

#endif // CALIBRATIONSTATE_H
