/*
 * 
 */
#include <OgreMeshManager.h>
#include "utils/Arrow.hpp"
#include "LaboratoryState.hpp"

LaboratoryState::LaboratoryState(rapidxml::xml_node<>& config)
	: mFramework(OgreFramework::getSingletonPtr())
{

}

LaboratoryState::~LaboratoryState()
{

}

/**********************************************************/

void LaboratoryState::enter()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Enter LaboratoryState...");

    mSceneMgr = mFramework->root()->createSceneManager(Ogre::ST_GENERIC, "LaboratorySceneMgr");
    createScene();
}

/**********************************************************/

void LaboratoryState::createScene()
{
	using Ogre::Vector3;
	typedef Ogre::ColourValue Color;
	
	Ogre::Light* light = mSceneMgr->createLight("tstLight");
    light->setType(Ogre::Light::LT_POINT);
    light->setPosition(2, 0.4, 2); 	
    light->setDiffuseColour(Ogre::ColourValue(0.6, 0.6, 1.0));
    light->setSpecularColour(Ogre::ColourValue(1, 0, 0));
	
    mSceneMgr->setAmbientLight(Color(0.6, 0.6, 0.6));
	
	// Camera
	mCamera = mSceneMgr->createCamera("FreeLookCam");
	mCamera->setPosition(0, 2, -3);
	mCamera->lookAt(0, 0, 0);
	mCamera->setNearClipDistance(0.0001);
	mCamera->setAspectRatio(Ogre::Real(OgreFramework::getSingletonPtr()->viewport()->getActualWidth()) /
		Ogre::Real(mFramework->viewport()->getActualHeight()));
	
	mCameraMan = new OgreBites::SdkCameraMan(mCamera);
	mCameraMan->setStyle(OgreBites::CS_FREELOOK);
	mCameraMan->setTopSpeed(5);
    mFramework->viewport()->setCamera(mCamera);
	
	
	Ogre::ManualObject* m = mSceneMgr->createManualObject("test arrow");
	
	
	mArrow.reset(new Arrow(m));
	
	mArrowGen = std::make_shared<ArrowFromCircle>(Vector3(0, 0.3, 0), Vector3(0,1.3,2), Vector3::UNIT_X);
	mArrowGen->height = 2.4;
	mArrow->generator(mArrowGen);
	mArrow->faceDirection = (Vector3::UNIT_X);
	mArrow->update();
	
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(m);
	
	// Ground
	Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
 
    Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        plane, 500, 500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
 
    Ogre::Entity* entGround = mSceneMgr->createEntity("GroundEntity", "ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(entGround);
	entGround->setMaterialName("Test/SolidGround");	
    entGround->setCastShadows(false);
}

/**********************************************************/

void LaboratoryState::exit()
{
    OgreFramework::getSingletonPtr()->log()->logMessage("Leaving LaboratoryState...");

	if(mCameraMan)	
		delete mCameraMan;
	if(mCamera)
		mSceneMgr->destroyCamera(mCamera);
    if(mSceneMgr)
        mFramework->root()->destroySceneManager(mSceneMgr);


    OgreFramework::getSingletonPtr()->trayMgr()->clearAllTrays();
    OgreFramework::getSingletonPtr()->trayMgr()->destroyAllWidgets();
    OgreFramework::getSingletonPtr()->trayMgr()->setListener(0);
}

/**********************************************************/

bool LaboratoryState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	mCameraMan->injectKeyDown(keyEventRef);
	
    return OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
}

/**********************************************************/

bool LaboratoryState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	using Ogre::Vector3;
	mCameraMan->injectKeyUp(keyEventRef);
	
	switch(keyEventRef.key)
	{
		case OIS::KC_1:
		{
			mArrowGen->middle += 0.1;
			mArrow->update(true);
		}
		break;
	case OIS::KC_2:
		{
			mArrowGen->middle -= 0.1;
			mArrow->update(true);
		}
		break;
	case OIS::KC_ESCAPE:
		shutdown();
		break;
	default:
		return OgreFramework::getSingletonPtr()->keyReleased(keyEventRef);
	};
	
	return true;
}

/**********************************************************/

bool LaboratoryState::mouseMoved(const OIS::MouseEvent &evt)
{
	mCameraMan->injectMouseMove(evt);
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseMove(evt))
		return true;
    return true;
}

/**********************************************************/


bool LaboratoryState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Pressed " <<  id << std::endl;
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseDown(evt, id))
		return true;
    return true;
}

/**********************************************************/

bool LaboratoryState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	std::cout << "Released " <<  id << std::endl;
	
	
    if(OgreFramework::getSingletonPtr()->trayMgr()->injectMouseUp(evt, id))
		return true;
    return true;
}

/**********************************************************/

void LaboratoryState::update(double timeSinceLastFrame)
{
    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
	mCameraMan->frameRenderingQueued(m_FrameEvent);
    OgreFramework::getSingletonPtr()->trayMgr()->frameRenderingQueued(m_FrameEvent);
}

/**********************************************************/

void LaboratoryState::buttonHit(OgreBites::Button *button)
{
}
