/*
 * 
 */
#include <string>
#include <boost/lexical_cast.hpp>
#include <OgreQuaternion.h>
#include "KinectManager.hpp"
#include "KinectError.hpp"
#include "SkeletonProvider.hpp"
#include "rxml/value.hpp"
#include "rxml/traits.hpp"

KinectUnitConverter::KinectUnitConverter(const rapidxml::xml_node<>& settings)
	: mPositionScale(boost::lexical_cast<float>(rxml::value(settings, "position_scale:value")))
	, mXRotate(boost::lexical_cast<float>(rxml::value(settings, "x-rotate:value")))
{

}


Ogre::Vector3 KinectUnitConverter::convert(const XnVector3D& old) const
{
	auto v = Ogre::Vector3(old.X, old.Y, old.Z) * mPositionScale;
	return Ogre::Quaternion(Ogre::Degree(mXRotate), Ogre::Vector3::UNIT_X) * v;
}



KinectManager::KinectManager(const rapidxml::xml_node<>& settings)
	: mConverter(rxml::getnode(settings, "unit_converter"))
{
    xn::EnumerationErrors errors;
	
	auto res = mContext.InitFromXmlFile(rxml::value(settings, "config:file").c_str(), mScriptNode, &errors);
    check_result(res, "Failed to init context!", errors);
	
    res = mContext.FindExistingNode(XN_NODE_TYPE_DEPTH, mDepthGenerator);
    check_result(res, "No depth-generator");
	
	
    res = mContext.FindExistingNode(XN_NODE_TYPE_IMAGE, mImageGenerator);
    check_result(res, "No depth-generator");
	
	mImageGenerator.GetMetaData(mImageMetaData);
	mDepthGenerator.GetMetaData(mDepthMetaData);
	
	// Hybrid mode isn't supported in this sample
	if (mImageMetaData.FullXRes() != mDepthMetaData.FullXRes() || mImageMetaData.FullYRes() != mDepthMetaData.FullYRes())
	{
		throw new std::runtime_error("Image- and Depthgenerator do not have the same resolution!");
	}

	// RGB is the only image format supported.
	if (mImageMetaData.PixelFormat() != XN_PIXEL_FORMAT_RGB24)
	{
		throw new std::runtime_error("Image-Generator has not the right format!");
	}
	
	mSkeltonProvider.reset(new SkeletonProvider(*this));
	
    res = mContext.StartGeneratingAll();
    check_result(res, "Failed to start skelet-generation!");
}

KinectManager::~KinectManager()
{
	mSkeltonProvider.reset();
	mDepthGenerator.Release();
	mImageGenerator.Release();
	mScriptNode.Release();
	mContext.Release();
}

SkeletonProvider& KinectManager::skeleton_provider()
{
	return *mSkeltonProvider;
}

bool KinectManager::loadKinectSettings()
{
	return false;
}
