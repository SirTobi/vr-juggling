#include <iostream>
#include <stdexcept>
#include "KinectError.hpp"
#include "SkeletonProvider.hpp"
#include "KinectManager.hpp"

// Callback: New user was detected
void SkeletonProvider::Callback_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pthis)
{
	static_cast<SkeletonProvider*>(pthis)->onNewUser(generator, nId);
}

// Callback: An existing user was lost
void SkeletonProvider::Callback_UserLost(xn::UserGenerator& generator, XnUserID nId, void* pthis)
{
	static_cast<SkeletonProvider*>(pthis)->onUserLost(generator, nId);
}

// Callback: Detected a pose
void SkeletonProvider::Callback_UserPose(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId, void* pthis)
{
	static_cast<SkeletonProvider*>(pthis)->onUserPose(capability, strPose, nId);
}
// Callback: Started calibration
void SkeletonProvider::Callback_CalibrationStart(xn::SkeletonCapability& capability, XnUserID nId, void* pthis)
{
	static_cast<SkeletonProvider*>(pthis)->onCalibrationStart(capability, nId);
}

void SkeletonProvider::Callback_CalibrationComplete(xn::SkeletonCapability& capability, XnUserID nId, XnCalibrationStatus eStatus, void* pthis)
{
	static_cast<SkeletonProvider*>(pthis)->onCalibrationComplete(capability, nId, eStatus);
}



SkeletonProvider::SkeletonProvider(KinectManager& kinect_mgr)
	: mNeedPoseDetection(false)
	, mCurrentUser(0)
	, mKinectMgr(kinect_mgr)
	, mContext(kinect_mgr.context())
{
    XnStatus res = mContext.FindExistingNode(XN_NODE_TYPE_USER, mUserGenerator);
    if (res != XN_STATUS_OK)
    {
        res = mUserGenerator.Create(mContext);
        check_result(res, "Failed to find user-generator!");
    }
    
    
    XnCallbackHandle hUserCallbacks, hCalibrationStart, hCalibrationComplete, hPoseDetected;
    if (!mUserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
    {
        throw_error("Supplied user generator doesn't support skeleton!");
    }

    res = mUserGenerator.RegisterUserCallbacks(SkeletonProvider::Callback_NewUser, Callback_UserLost, this, hUserCallbacks);
    check_result(res, "Failed to register user callbacks");
    res = mUserGenerator.GetSkeletonCap().RegisterToCalibrationStart(SkeletonProvider::Callback_CalibrationStart, this, hCalibrationStart);
    check_result(res, "Failed to register calibration start");
    res = mUserGenerator.GetSkeletonCap().RegisterToCalibrationComplete(SkeletonProvider::Callback_CalibrationComplete, this, hCalibrationComplete);
    check_result(res, "Failed to register calibration complete");
	
	if (mNeedPoseDetection = mUserGenerator.GetSkeletonCap().NeedPoseForCalibration())
    {
		std::cout << "Need Pose Detection!" << std::endl;
        if (!mUserGenerator.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
        {
            throw_error("Pose required, but not supported!");
        }
        res = mUserGenerator.GetPoseDetectionCap().RegisterToPoseDetected(SkeletonProvider::Callback_UserPose, NULL, hPoseDetected);
        check_result(res, "Failed to register 'Pose Detected'!");
        mUserGenerator.GetSkeletonCap().GetCalibrationPose(mCurrentPose);
    } 
    mUserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);
}

SkeletonProvider::~SkeletonProvider()
{
	mUserGenerator.Release();
}

#define MAX_NUM_USERS 16
bool SkeletonProvider::update()
{
	if(mContext.WaitOneUpdateAll(mUserGenerator) != XN_STATUS_OK)
	{
		return false;
	}
	
	XnUserID aUsers[MAX_NUM_USERS];
    XnUInt16 nUsers = MAX_NUM_USERS;
	mUserGenerator.GetUsers(aUsers, nUsers);
	for(XnUInt16 i = 0; i < nUsers; i++)
	{
		if(mUserGenerator.GetSkeletonCap().IsTracking(aUsers[i]))
		{
			mCurrentUser = aUsers[i];
			return true;
		}
	}
	
	return false;
}

bool SkeletonProvider::get_joint(XnSkeletonJoint joint, XnSkeletonJointTransformation& data)
{
	auto cap = mUserGenerator.GetSkeletonCap();
	bool ok = false;
	
	if(cap.IsTracking(mCurrentUser))
	{
		auto status = cap.GetSkeletonJoint(mCurrentUser, joint, data);
		ok = (status == XN_STATUS_OK);
	}
	
	return ok;
}


int SkeletonProvider::get_user_count()
{
	return mUserGenerator.GetNumberOfUsers();
}


void SkeletonProvider:: onNewUser(xn::UserGenerator& generator, XnUserID nId)
{
	std::cout << "Kinect: New User detected!" << std::endl;
	if (mNeedPoseDetection)
    {
        mUserGenerator.GetPoseDetectionCap().StartPoseDetection(mCurrentPose, nId);
    }
    else
    {
		mUserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
	}
}

void SkeletonProvider:: onUserLost(xn::UserGenerator& generator, XnUserID nId)
{
	std::cout << "Kinect: User lost!" << std::endl;
}

void SkeletonProvider:: onUserPose(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId)
{
    mUserGenerator.GetPoseDetectionCap().StopPoseDetection(nId);
    mUserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
}

void SkeletonProvider:: onCalibrationStart(xn::SkeletonCapability& capability, XnUserID nId)
{
}

void SkeletonProvider:: onCalibrationComplete(xn::SkeletonCapability& capability, XnUserID nId, XnCalibrationStatus eStatus)
{
	if (eStatus == XN_CALIBRATION_STATUS_OK)
    {
        mUserGenerator.GetSkeletonCap().StartTracking(nId);
    }
    else
    {
        // Calibration failed
		std::cerr << "Calibration failed for user" << nId << std::endl;
        if(eStatus==XN_CALIBRATION_STATUS_MANUAL_ABORT)
        {
            std::cerr << "Manual abort occured, stop attempting to calibrate!" << std::endl;
            return;
        }
        if (mNeedPoseDetection)
        {
            mUserGenerator.GetPoseDetectionCap().StartPoseDetection(mCurrentPose, nId);
        }
        else
        {
            mUserGenerator.GetSkeletonCap().RequestCalibration(nId, TRUE);
        }
    }
}
