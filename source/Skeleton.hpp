#ifndef SKELETON_H
#define SKELETON_H

#include <vector>
#include <unordered_map>
#include <XnOpenNI.h>
#include <OgreColourValue.h>
#include "rapidxml.hpp"
#include "SkeletonProvider.hpp"
#include "utils/DynamicLines.hpp"

namespace std
{
	template <> 
	struct hash<XnSkeletonJoint>
	{
		size_t operator()(const XnSkeletonJoint& v) const
		{
			return hash<int>()(static_cast<int>(v));
		}
	};
}

class Skeleton: public DynamicLines
{
public:
	enum Hands
	{
		Right,
		Left
	};
	
	struct Joint
	{
		Ogre::Vector3 pos;
		float confidence;
	};
	
	typedef std::pair<XnSkeletonJoint, XnSkeletonJoint> Limb;
public:
	Skeleton();
	Skeleton(rapidxml::xml_node<>& config);
	~Skeleton();
	
	void apply(SkeletonProvider& provider);
	
	const Skeleton::Joint& getJoint(XnSkeletonJoint j);
	const Skeleton::Joint& getHand(Hands hand);
	const Skeleton::Joint& getHead();
	Ogre::ColourValue color;
	
	
	Ogre::Vector3 offset;
	
private:
	void init();
private:
	std::unordered_map<XnSkeletonJoint, Joint> mJoints;
	std::vector<Limb> mLimbs;
};

#endif // SKELETON_H
