/*
 * 
 */
#include <cassert>
#include <iostream>
#include <stdexcept>
#include "ScriptManager.hpp"
#include "ScriptChamber.hpp"
#include <scriptstdstring/scriptstdstring.h>
#include <scriptarray/scriptarray.h>

struct Tester
{
	Tester() {}
	
	int operator % (int ret)
	{
		assert(ret >= 0);
		if(ret < 0)
			throw std::runtime_error("Angelscript error!");
		return ret;
	}
};

#define AS_TEST Tester()

ScriptManager::ScriptManager(std::function<void ()> quit_func)
	: mModule(nullptr)
	, mQuitFunction(quit_func)
{
	init_engine();
}

ScriptManager::~ScriptManager()
{
	if(mEngine)
		mEngine->Release();
}



int IncludeCallback(const char *include, const char *from, CScriptBuilder *builder, const char* include_path)
{
	std::string incl_file = include;
	std::string base_path = include_path;
	std::string file = base_path + "/" + incl_file;
	return builder->AddSectionFromFile(file.c_str());
}


void ScriptManager::load_script(const std::string& file, const std::string& include_path)
{
	assert(!mModule);
	
	CScriptBuilder builder;
	AS_TEST % builder.StartNewModule(mEngine, "test_driver");
	AS_TEST % builder.AddSectionFromFile((include_path + "/" + file).c_str());
	builder.SetIncludeCallback((INCLUDECALLBACK_t)&IncludeCallback, (void*)include_path.c_str());
	AS_TEST % builder.BuildModule();

	mModule = builder.GetModule();
}


void ScriptManager::start_script(const std::string& function_name)
{
	assert(mModule);
	mContext = mEngine->CreateContext();
	assert(mContext);
	
	asIScriptFunction* func = mModule->GetFunctionByDecl("void main()");
	
	if(!func)
	{
		throw std::runtime_error("Failed to find main function!");
	}
	
	AS_TEST % mContext->Prepare(func);
	//mContext->SetArgAddress(chamber);
	
	std::cout << "Call main function..." << std::endl;
	resume();
}

void ScriptManager::resume()
{
	assert(mContext);
	int r = mContext->Execute();
	
	switch(r)
	{
	default:
	case asEXECUTION_ERROR:
		assert(!"Not implemented");
		
	case asEXECUTION_FINISHED:
		mQuitFunction();
		break;
		
	case asEXECUTION_SUSPENDED:
		break;
	}
}


void MessageCallback(const asSMessageInfo *msg, void *param)
{
	std::string type = "ERR ";
	if( msg->type == asMSGTYPE_WARNING ) 
		type = "WARN";
	else if( msg->type == asMSGTYPE_INFORMATION ) 
		type = "INFO";

	std::cout << msg->section << "(" << msg->row << ", "  << msg->col << ")[" <<  type << "]" << msg->message << std::endl;
	
	if(msg->type == asMSGTYPE_ERROR)
		throw std::runtime_error("Error in script engine!");
}


void ScriptManager::init_engine()
{
	mEngine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
	assert(mEngine);
	
	mEngine	->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);
	
	init_bindings();
}

void SimpleLogFunc(const std::string& _msg)
{
	std::cout << "ScriptLog: " << _msg << std::endl;
}

void ScriptManager::init_bindings()
{
	// string
	RegisterStdString(mEngine);
	RegisterScriptArray(mEngine, true);
	//RegisterStdStringUtils(mEngine);
	
	// log
	mEngine->RegisterGlobalFunction("void log(const string& in msg)", asFUNCTION(SimpleLogFunc), asCALL_CDECL);
}
