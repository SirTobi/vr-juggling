/*
 * 
 */

#include "ScriptChamber.hpp"
#include "SkeletonProvider.hpp"
#include "KinectManager.hpp"
#include "MainApp.hpp"
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreMeshManager.h>
#include <OgreEntity.h>
#include <scripthelper/scripthelper.h>

ScriptChamber::ScriptChamber(Ogre::SceneManager* scmgr)
	: mQuit(false)
	, mScriptManager(std::bind(&ScriptChamber::quit, this, false))
	, mNextToken(1)
	, mFirstValidToken(0)
{
	init_bindings();
}

ScriptChamber::~ScriptChamber()
{
}


void ScriptChamber::start(const std::string& path, const std::string& include_path)
{	
	mScriptManager.load_script(path, include_path);
	mScriptManager.start_script("main");
}

bool ScriptChamber::update(double dt)
{
	for(auto it = mActiveActions.begin(); it != mActiveActions.end();)
	{
		if(!(*it)->update(dt))
		{
			mWaitSet.erase((*it)->action_id);
			it = mActiveActions.erase(it);
		}else
			++it;
	}
	
	mCurrentTime += ch::duration_cast<ch::milliseconds>(ch::duration<double>(dt));
	
	while(mPendingActions.size() && mPendingActions.top()->trigger_time <= mCurrentTime)
	{
		// hack, because bad std design
		ActionPtr action = std::move(const_cast<ActionPtr&>(mPendingActions.top()));
		mPendingActions.pop();
		
		if(!mCancelSet.erase(action->action_id))
		{
			if(action->trigger())
				mActiveActions.push_back(std::move(action));
			else
				mWaitSet.erase(action->action_id);
		}
	}
	
	if(mWaitSet.empty())
		resume();
	
	return !mQuit || mPendingActions.size() || mActiveActions.size();
}


void ScriptChamber::quit(bool _force)
{
	mQuit = true;
	if(_force)
	{
		while(!mPendingActions.empty())
			mPendingActions.pop();
		mActiveActions.clear();
	}
}

ActionToken ScriptChamber::add_action(ActionPtr action, ch::milliseconds mill)
{
	action->action_id = mNextToken;
	if(mill == ch::milliseconds(0))
	{
		mActiveActions.push_back(std::move(action));
	}else{
		action->trigger_time = mill + mCurrentTime;
		mPendingActions.push(std::move(action));
	}
	
	return mNextToken++;
}

ActionToken ScriptChamber::add_action(Action* action, std::chrono::milliseconds mill)
{
	return add_action(ActionPtr(action), mill);
}

ActionToken ScriptChamber::add_action(std::function< void() > func, std::chrono::milliseconds mill)
{
	return add_action(new FunctionAction(func), mill);
}

void ScriptChamber::resume()
{
	mWaitSet.clear();
	mScriptManager.resume();
}

void ScriptChamber::suspend()
{
	mFirstValidToken = mNextToken;
	asGetActiveContext()->Suspend();
}

void ScriptChamber::wait_for(ActionToken _at)
{
	if(_at >= mNextToken || _at < mFirstValidToken)
		throw std::logic_error("Action Token is not valid!");
	mWaitSet.insert(_at);
}


void ScriptChamber::wait(unsigned int _milli)
{
	if(_milli)
		wait_for(add_action([](){}, ch::milliseconds(_milli)));
	suspend();
}

void ScriptChamber::clear_wait()
{
	mWaitSet.clear();
}

void ScriptChamber::stop_action(ActionToken _at)
{
	mActiveActions.remove_if([_at](ActionPtr& p) { return p->action_id == _at;});
	mCancelSet.insert(_at);
	mWaitSet.erase(_at);
}


void ScriptChamber::init_bindings()
{
	asIScriptEngine* engine = mScriptManager.getEngine();
	
	engine->RegisterTypedef("ActionToken", "uint");
	engine->RegisterGlobalFunction("void WaitFor(ActionToken _tk)", asMETHOD(ScriptChamber, wait_for), asCALL_THISCALL_ASGLOBAL, this);
	engine->RegisterGlobalFunction("void Wait(uint millisec = 0)", asMETHOD(ScriptChamber, wait), asCALL_THISCALL_ASGLOBAL, this);
	engine->RegisterGlobalFunction("void ClearWait()", asMETHOD(ScriptChamber, clear_wait), asCALL_THISCALL_ASGLOBAL, this);
	engine->RegisterGlobalFunction("void StopAction(ActionToken _tk)", asMETHOD(ScriptChamber, stop_action), asCALL_THISCALL_ASGLOBAL, this);
	
	//engine->RegisterGlobalFunction("void Wait(int millisec)", asMETHOD(ChamberController, wait), asCALL_THISCALL_ASGLOBAL, mController);
	//engine->RegisterGlobalFunction("void WaitForKinect()", asMETHOD(ChamberController, waitForKinect), asCALL_THISCALL_ASGLOBAL, mController);
	
}
