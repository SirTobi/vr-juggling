/*
 * 
 */

#ifndef SCRIPTMANAGER_H
#define SCRIPTMANAGER_H

#include <functional>
#include <string>
#include <angelscript.h>
#include <scriptbuilder/scriptbuilder.h>

class ScriptChamber;

class ScriptManager
{
public:
	ScriptManager(std::function<void ()> quit_func);
	~ScriptManager();

	void load_script(const std::string& file, const std::string& include_path);
	void start_script(const std::string& function_name);
	
	void resume();
	
	asIScriptEngine* getEngine() const { return mEngine; }
	asIScriptContext* getContext() const { return mContext; }
private:
	void init_engine();
	void init_bindings();
	
private:
	asIScriptEngine* mEngine;
	asIScriptModule* mModule;
	asIScriptContext* mContext;
	std::function<void ()> mQuitFunction;
};

#endif // SCRIPTMANAGER_H
