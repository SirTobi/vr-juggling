/*
 * 
 */

#ifndef SCRIPTCHAMBER_H
#define SCRIPTCHAMBER_H

#include "ScriptManager.hpp"
#include <memory>
#include <queue>
#include <list>
#include <chrono>
#include <unordered_set>

namespace ch = std::chrono;

namespace Ogre {
	class SceneManager;
}

class Action
{
public:
	struct ActionComp
	{
		bool operator ()(const std::unique_ptr<Action>& f, const std::unique_ptr<Action>& s)
		{
			return f->trigger_time < s->trigger_time;
		}
	};
	
	virtual ~Action() {}
	virtual bool trigger() { return true; }
	virtual bool update(double dt) { return false; }
	
	ch::milliseconds trigger_time = ch::milliseconds(0);
	unsigned int action_id = 0;
};


class FunctionAction: public Action
{
public:
	FunctionAction(std::function<void()> func):mFunc(func) {}
	
	virtual bool trigger() override
	{
		mFunc();
		return false;			
	}
private:
	std::function<void()> mFunc;
};


typedef unsigned int ActionToken;

class ScriptChamber
{
	typedef std::unique_ptr<Action> ActionPtr;

public:
	ScriptChamber(Ogre::SceneManager* scmgr);
	~ScriptChamber();

	ActionToken add_action(ActionPtr action, ch::milliseconds mill = ch::milliseconds(0));
	ActionToken add_action(Action* action, ch::milliseconds mill = ch::milliseconds(0));
	ActionToken add_action(std::function<void()> func, ch::milliseconds mill = ch::milliseconds(0));
	void start(const std::string& path, const std::string& include_path);
	bool update(double dt);
	
	asIScriptEngine* getEngine() const { return mScriptManager.getEngine(); }
	asIScriptContext* getContext() const { return mScriptManager.getContext(); }
	void resume();
	void suspend();

	void wait_for(ActionToken _at);
	void wait(unsigned int _milli = 0);
	void clear_wait();
	void stop_action(ActionToken _at);
	
	void quit(bool _force = false);
private:
	void init_bindings();
private:
	ScriptManager mScriptManager;
	bool mQuit;
	ch::milliseconds mCurrentTime;
	
	// action stuff
	std::list<ActionPtr> mActiveActions;
	std::priority_queue<ActionPtr, std::vector<ActionPtr>, Action::ActionComp> mPendingActions;
	unsigned int mNextToken;
	unsigned int mFirstValidToken;
	std::unordered_set<ActionToken> mWaitSet;
	std::unordered_set<ActionToken> mCancelSet;
};

#endif // SCRIPTCHAMBER_H
