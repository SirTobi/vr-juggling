/*
 * 
 */

#ifndef KINECTMANAGER_H
#define KINECTMANAGER_H

#include <memory>
#include <XnCppWrapper.h>
#include <OgreVector3.h>
#include "rapidxml.hpp"

class SkeletonProvider;


class KinectUnitConverter
{
public:
	KinectUnitConverter(const rapidxml::xml_node<>& settings);
	
	Ogre::Vector3 convert(const XnVector3D& old) const;
private:
	float mPositionScale;
	float mXRotate;
};


class KinectManager
{
public:
	KinectManager(const rapidxml::xml_node<>& settings);
	~KinectManager();
	
	bool loadKinectSettings();
	
	SkeletonProvider& skeleton_provider();
	
	inline xn::Context& context() { return mContext; }
	inline xn::DepthGenerator& depthGenerator() { return mDepthGenerator; }
	inline xn::ImageGenerator& imageGenerator() { return mImageGenerator; }
	
	const KinectUnitConverter& converter() const { return mConverter; }
private:
	xn::DepthMetaData mDepthMetaData;
	xn::ImageMetaData mImageMetaData;
	xn::Context mContext;
	xn::ScriptNode mScriptNode;
	xn::DepthGenerator mDepthGenerator;
	xn::ImageGenerator mImageGenerator;
	std::unique_ptr<SkeletonProvider> mSkeltonProvider;
	KinectUnitConverter mConverter;
};

#endif // KINECTMANAGER_H
