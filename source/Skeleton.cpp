#include "Skeleton.hpp"
#include "utils/LexicalCasts.hpp"
#include "rxml/value.hpp"

Skeleton::Skeleton()
	: offset(0.0f)
{
	init();
}


Skeleton::Skeleton(rapidxml::xml_node<>& config)
{
	offset = boost::lexical_cast<Ogre::Vector3>(rxml::value(config, "offset:value"));
	init();
}

Skeleton::~Skeleton()
{

}

void Skeleton::init()
{
	color = Ogre::ColourValue::White;
	mLimbs = {{XN_SKEL_HEAD, XN_SKEL_NECK },
				{ XN_SKEL_NECK, XN_SKEL_LEFT_SHOULDER },
				{ XN_SKEL_LEFT_SHOULDER, XN_SKEL_LEFT_ELBOW },
				{ XN_SKEL_LEFT_ELBOW, XN_SKEL_LEFT_HAND },
				{ XN_SKEL_NECK, XN_SKEL_RIGHT_SHOULDER },
				{ XN_SKEL_RIGHT_SHOULDER, XN_SKEL_RIGHT_ELBOW },
				{ XN_SKEL_RIGHT_ELBOW, XN_SKEL_RIGHT_HAND },
				{ XN_SKEL_LEFT_SHOULDER, XN_SKEL_TORSO },
				{ XN_SKEL_RIGHT_SHOULDER, XN_SKEL_TORSO },
				{ XN_SKEL_TORSO, XN_SKEL_LEFT_HIP },
				{ XN_SKEL_LEFT_HIP, XN_SKEL_LEFT_KNEE },
				{ XN_SKEL_LEFT_KNEE, XN_SKEL_LEFT_FOOT },
				{ XN_SKEL_TORSO, XN_SKEL_RIGHT_HIP },
				{ XN_SKEL_RIGHT_HIP, XN_SKEL_RIGHT_KNEE },
				{ XN_SKEL_RIGHT_KNEE, XN_SKEL_RIGHT_FOOT },
				{ XN_SKEL_LEFT_HIP, XN_SKEL_RIGHT_HIP }};
	
	for(auto& limb : mLimbs)
	{
		mJoints[limb.first].confidence
			= mJoints[limb.second].confidence
				= 0.0f;
	}
}


const Skeleton::Joint& Skeleton::getJoint(XnSkeletonJoint j)
{
	return mJoints[j];
}


const Skeleton::Joint& Skeleton::getHand(Skeleton::Hands hand)
{
	return mJoints[hand == Right? XN_SKEL_RIGHT_HAND : XN_SKEL_LEFT_HAND];
}

const Skeleton::Joint& Skeleton::getHead()
{
	return mJoints[XN_SKEL_HEAD];
}

void Skeleton::apply(SkeletonProvider& provider)
{
	const KinectUnitConverter& converter = provider.converter();
	bool updated = false;
	for(auto& jp : mJoints)
	{
		XnSkeletonJointTransformation trans;
		if(provider.get_joint(jp.first, trans))
		{
			jp.second.pos = converter.convert(trans.position.position) + offset;
			jp.second.confidence = trans.position.fConfidence;
			updated = true;
		}
	}
	
	if(updated)
	{
		clear();
		for(auto& limb : mLimbs)
		{
			addLine(mJoints[limb.first].pos,
					mJoints[limb.second].pos,
					color);
		}
		update();
	}
}