/*
 * 
 */

#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <OgreHardwareVertexBuffer.h>

class PointCloud
{
	friend class PCPointUpdater;
	friend class PCColorUpdater;
public:

   PointCloud(const std::string& name, const std::string& resourcegroup, const int numpoints, float *parray, float *carray);
   virtual ~PointCloud();

   /// Update a created pointcloud with size points.
   void updateVertexPositions(int size, float *points);

   /// Update vertex colours
   void updateVertexColours(int size, float *colours);


private:
   int mSize;
   Ogre::HardwareVertexBufferSharedPtr vbuf;
   Ogre::HardwareVertexBufferSharedPtr cbuf;
};

class PCPointUpdater
{
public:
	PCPointUpdater(PointCloud& pc);
	~PCPointUpdater();
	
	void push(float x, float y, float z);
	void push(const Ogre::Vector3& vec);
private:
	Ogre::HardwareVertexBufferSharedPtr& mBuffer;
	float* mContent;
};


class PCColorUpdater
{
public:
	PCColorUpdater(PointCloud& pc);
	~PCColorUpdater();
	
	void push(float red, float green, float blue);
	void push(const Ogre::ColourValue& color);
private:
	Ogre::HardwareVertexBufferSharedPtr& mBuffer;
	float* mContent;
};

#endif // POINTCLOUD_H
