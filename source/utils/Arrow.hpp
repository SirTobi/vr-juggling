/*
 * 
 */

#ifndef ARROW_H
#define ARROW_H
#include <OgreVector3.h>
#include <OgreColourValue.h>
#include <OgreManualObject.h>
#include <memory>

class ArrowGenerator
{
public:
	typedef Ogre::Vector3 Vec3;
	typedef Ogre::ColourValue Color;
public:
	virtual const Vec3& target() = 0;
	virtual const Vec3& source() = 0;
	
	virtual bool supportColor() = 0;
	virtual void reset() = 0;
	virtual bool next(Vec3* pos, Color* col) = 0;
};

template<typename Container = std::vector<Ogre::Vector3>>
class ArrowFromList: public ArrowGenerator
{
public:
	ArrowFromList(Container container): mContainer(container) { assert(mContainer.size() >= 2); }
	
	virtual const Vec3& target() override { return mContainer.back(); }
	virtual const Vec3& source() override { return mContainer.front(); }
	
	virtual bool supportColor() override { return false; }
	virtual void reset() override { mIt = mContainer.begin(); }
	virtual bool next(Vec3* pos, Color* col) override
	{
		if(mIt == mContainer.end())
			return false;
		*pos = *mIt++;
		return true;
	}
	
private:
	typename Container::const_iterator mIt;
	Container mContainer;
};


class ArrowFromCircle : public ArrowGenerator
{
public:
	ArrowFromCircle(const Vec3& source, const Vec3& target, const Vec3& arch_dir, float middle = 0.5f, double _step_dist = 0.1f);
	
	virtual const Vec3& target() override;
	virtual const Vec3& source() override;
	
	virtual bool supportColor() override;
	virtual void reset() override;
	virtual bool next(Vec3* pos, Color* col);
	
	void step_size(double _step_dist);
	
	double radius() const;
	Vec3 middle_point() const;
	
	Vec3 _source;
	Vec3 _target;
	double middle;
	double height;
	Vec3 arch_dir;
	
private:
	double mProgress;
	double mStepSize;
};


class Arrow
{
public:
	typedef Ogre::Vector3 Vec3;
	typedef Ogre::ColourValue Color;
public:
	
	Arrow(Ogre::ManualObject* obj, bool _3d = true);
	~Arrow();
	
	void simple_gen(const Vec3& source, const Vec3& target);
	template<typename Container>
	void container_gen(Container c) { generator(std::make_shared<ArrowFromList<Container>>(c)); }
	void generator(std::shared_ptr<ArrowGenerator> gen);
	void color(const Color& _color);
	void face_direction(const Vec3& dir);
	
	Ogre::ManualObject* getObject() const;
	
	void update(bool _force = false);
	
	float widthFactor = 1.0f;
	Vec3 faceDirection;
	std::string materialName;
private:
	void createMaterial();
	
	static const Ogre::String MaterialName;
private:
	Ogre::ManualObject* obj;
	bool mDirty;
	bool m3d;
	
	std::shared_ptr<ArrowGenerator> mGenerator;
	Color mColor;
};

#endif // ARROW_H
