#ifndef _LEXICAL_CASTS_HPP
#define _LEXICAL_CASTS_HPP

#include <istream>
#include <OgreVector3.h>
namespace Ogre {
template<typename T>
std::basic_istream<T>& operator >>(std::basic_istream<T>& is, Vector3& v)
{
	auto flags = is.flags();
	is >> std::skipws >> v.x >> v.y >> v.z;
	is.flags(flags);
	return is;
}
}
#include <boost/lexical_cast.hpp>

#endif