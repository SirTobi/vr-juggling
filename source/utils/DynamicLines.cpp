#include "DynamicLines.hpp"
#include <Ogre.h>
#include <cassert>
#include <cmath>
 
using namespace Ogre;
 
enum {
	POSITION_BINDING,
	NORMAL_BINDING,
	DEFUSELIGHT_BINDING
};
 
DynamicLines::DynamicLines()
{
	initialize(Ogre::RenderOperation::OT_LINE_LIST, false);
	setMaterial("BaseWhiteNoLighting");
	mDirty = true;
}
 
DynamicLines::~DynamicLines()
{
}
 
void DynamicLines::addStraight(const Vector3& p, const Vector3& offset, const Color& color)
{
	Vector3 mul(10000);
	addLine(p * mul + offset, -p * mul + offset, color);
}

void DynamicLines::addLine(const Vector3 &p1, const Vector3 &p2, const Color& color)
{
	Vector3 normal1 = (p1 - p2).normalisedCopy();
	Vector3 normal2 = -normal1;
	mLines.push_back(Line(Point(p1, normal1, color), Point(p2, normal2, color)));
	mDirty = true;
}

unsigned short DynamicLines::getNumPoints(void) const
{
	return (unsigned short)mLines.size() * 2;
}


void DynamicLines::clear()
{
	mLines.clear();
	mDirty = true;
}
 
void DynamicLines::update()
{
	if (mDirty) fillHardwareBuffers();
}
 
void DynamicLines::createVertexDeclaration()
{
	VertexDeclaration *decl = mRenderOp.vertexData->vertexDeclaration;
	int offset = 0;
	decl->addElement(0, offset, VET_FLOAT3, VES_POSITION);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
	decl->addElement(0, offset, VET_FLOAT3, VES_NORMAL);
	offset += Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
	decl->addElement(0, offset, VET_COLOUR_ABGR, VES_DIFFUSE);
}
 
void DynamicLines::fillHardwareBuffers()
{
	int points = mLines.size() * 2;

	prepareHardwareBuffers(points,0);

	if (!points) { 
		mBox.setExtents(Vector3::ZERO,Vector3::ZERO);
		mDirty=false;
		return;
	}

	Vector3 vaabMin = mLines.front().points[0].pos;
	Vector3 vaabMax = vaabMin;

	HardwareVertexBufferSharedPtr vbuf = mRenderOp.vertexData->vertexBufferBinding->getBuffer(0);

	Real *prPos = static_cast<Real*>(vbuf->lock(HardwareBuffer::HBL_DISCARD));
	{
		for(Line& line : mLines)
		{
			for(Point& p : line.points)
			{
				*prPos++ = p.pos.x;
				*prPos++ = p.pos.y;
				*prPos++ = p.pos.z;
				
				*prPos++ = p.normal.x;
				*prPos++ = p.normal.y;
				*prPos++ = p.normal.z;
				
				*reinterpret_cast<ABGR*>(prPos++) = p.color.getAsABGR();

				if(p.pos.x < vaabMin.x)
					vaabMin.x = p.pos.x;
				if(p.pos.y < vaabMin.y)
					vaabMin.y = p.pos.y;
				if(p.pos.z < vaabMin.z)
					vaabMin.z = p.pos.z;

				if(p.pos.x > vaabMax.x)
					vaabMax.x = p.pos.x;
				if(p.pos.y > vaabMax.y)
					vaabMax.y = p.pos.y;
				if(p.pos.z > vaabMax.z)
					vaabMax.z = p.pos.z;
			}
		}
	}
	vbuf->unlock();

	mBox.setExtents(vaabMin, vaabMax);
	auto* node = getParentNode();
	if(node)
		node->_update(true, false);

	mDirty = false;
}
 
/*
void DynamicLines::getWorldTransforms(Matrix4 *xform) const
{
   // return identity matrix to prevent parent transforms
   *xform = Matrix4::IDENTITY;
}
*/
/*
const Quaternion &DynamicLines::getWorldOrientation(void) const
{
   return Quaternion::IDENTITY;
}
 
const Vector3 &DynamicLines::getWorldPosition(void) const
{
   return Vector3::ZERO;
}
*/