/*
 * 
 */
#include <queue>
#include <OgreMaterialManager.h>
#include <OgrePass.h>
#include <OgreTechnique.h>
#include "Arrow.hpp"


const double pi = 3.14159265358;
const Ogre::String Arrow::MaterialName = "Arrow/Normal";

Arrow::Arrow(Ogre::ManualObject* obj, bool _3d)
	: obj(obj)
	, mColor(Color::Red)
	, mDirty(true)
	, faceDirection(Vec3::UNIT_Y)
	, m3d(_3d)
	, materialName(MaterialName)
{
	assert(obj);
	createMaterial();
}

Arrow::~Arrow()
{

}


class SimpleGenerator : public ArrowGenerator
{
public:
	SimpleGenerator(const Arrow::Vec3& source, const Arrow::Vec3& target)
		: mSource(source)
		, mTarget(target)
	{
	}

	virtual const Vec3& source() override
	{
		return mSource;
	}

	virtual const Vec3& target() override
	{
		return mTarget;
	}

	virtual bool next(Vec3* pos, Color* col) override
	{
		return false;
	}

	virtual void reset() override
	{
	}

	virtual bool supportColor() override
	{
		return false;
	}
	
private:
	const Arrow::Vec3 mSource;
	const Arrow::Vec3 mTarget;
};


void Arrow::simple_gen(const Arrow::Vec3& source, const Arrow::Vec3& target)
{
	generator(std::make_shared<SimpleGenerator>(source, target));
}


void Arrow::generator(std::shared_ptr< ArrowGenerator > gen)
{
	mDirty = true;
	mGenerator = std::move(gen);
}


void Arrow::color(const Arrow::Color& _color)
{
	mColor = _color;
	mDirty = true;
}


void Arrow::update(bool _force)
{
	if(!mDirty && !_force)
		return;
	
	assert(mGenerator);
	mGenerator->reset();	

	const bool use_gen_color = mGenerator->supportColor();
	const Vec3 source = mGenerator->source();
	const Vec3 target = mGenerator->target();
	const Vec3 global_direction = target - source;
	const float global_distance = global_direction.length();
	const float arrow_width = std::sqrt(std::sqrt(global_distance)) * 0.1 * widthFactor;
	const float arrow_head_size = arrow_width * 3.0f;
	
	
	obj->clear();
	obj->begin(materialName, Ogre::RenderOperation::OT_TRIANGLE_LIST);
	
	bool first = true;
	std::array<Vec3, 2> lastPos = { source, Vec3() };
	
	Ogre::uint32 idx = 0;
	Vec3 next;
	Color color;
	
	auto orthogonal = 
		[&]() {
			if(!first)
			{
				// from 3
				return (next - lastPos[1]).crossProduct(faceDirection).normalisedCopy();
			}else{
				// from 2
				return (next - lastPos[0]).crossProduct(faceDirection).normalisedCopy();
			}
		};
	
	auto add_position_and_faces = 
		[&]() {
			
			const Vec3& curpos = lastPos.front();
			const Vec3& offset = orthogonal() * arrow_width;
			
			const Color& c = use_gen_color? color : mColor;
			obj->position(curpos + offset);
			obj->colour(c);
			obj->position(curpos - offset);
			obj->colour(c);
			idx += 2;
			
			if(!first)
			{
				assert(idx >= 4);
				obj->triangle(idx - 1, idx - 2, idx - 3);
				obj->triangle(idx - 4, idx - 2, idx - 3);
			}else{
				first = false;
			}
		};
	
	
	while(mGenerator->next(&next, &color) && next.distance(target) > arrow_head_size)
	{
		add_position_and_faces();
		lastPos.back() = lastPos.front();
		lastPos.front() = next;
	}
	
	if(first)
	{
		// generator never returned true
		// add source postion
		next = target;
		add_position_and_faces();
	}
	
	// add arrow head
	{
		assert(!first);
		const Vec3 head_dir = (target - lastPos.front()).normalisedCopy();
		const Vec3 offset_dir = head_dir.crossProduct(faceDirection).normalisedCopy();
		const Vec3 head_back_pos = target - head_dir * arrow_head_size;
		const Vec3 head_inner_offset = offset_dir * arrow_width;
		const Vec3 head_outer_side_offset = offset_dir * 2.0f * arrow_width;
		const Vec3 head_outer_back_offset = head_dir * 0.2f * arrow_head_size;
		obj->position(head_back_pos + head_inner_offset);
		obj->position(head_back_pos - head_inner_offset);
		obj->position(head_back_pos + head_outer_side_offset - head_outer_back_offset);
		obj->position(head_back_pos - head_outer_side_offset - head_outer_back_offset);
		obj->position(target);
		idx += 5;
		
		// connection to head
		obj->triangle(idx - 4, idx - 5, idx - 6);
		obj->triangle(idx - 5, idx - 7, idx - 6);
		
		// head
		obj->triangle(idx - 1, idx - 3, idx - 5);
		obj->triangle(idx - 1, idx - 5, idx - 4);
		obj->triangle(idx - 1, idx - 4, idx - 2);
	}
	
	obj->end();
	mDirty = false;
}


void Arrow::createMaterial()
{
	using namespace Ogre;
    auto mat = MaterialManager::getSingleton().getByName(MaterialName);
    if (mat.isNull())
    {
        mat = MaterialManager::getSingleton().create(MaterialName, ResourceGroupManager::INTERNAL_RESOURCE_GROUP_NAME);
        Pass* p = mat->getTechnique(0)->getPass(0);
        p->setLightingEnabled(false);
        p->setPolygonModeOverrideable(false);
        p->setSceneBlending(SBT_TRANSPARENT_ALPHA);
        p->setCullingMode(CULL_NONE);
        p->setDepthWriteEnabled(true);
		p->setVertexColourTracking(TVC_AMBIENT | TVC_DIFFUSE  | TVC_EMISSIVE | TVC_SPECULAR);
    }
}

Ogre::ManualObject* Arrow::getObject() const
{
	return obj;
}



/*********************************************** CircleGenerator **************************************************/

ArrowFromCircle::ArrowFromCircle(const ArrowGenerator::Vec3& source, const ArrowGenerator::Vec3& target, const ArrowGenerator::Vec3& arch_dir, float middle, double _step_dist)
	: _source(source)
	, _target(target)
	, middle(middle)
	, arch_dir(arch_dir)
	, mProgress(0.0f)
	, height(1)
{
	step_size(_step_dist);
}

const ArrowGenerator::Vec3& ArrowFromCircle::source()
{
	return _source;
}

const ArrowGenerator::Vec3& ArrowFromCircle::target()
{
	return _target;
}

void ArrowFromCircle::reset()
{
	mProgress = 0;
}

bool ArrowFromCircle::next(ArrowGenerator::Vec3* pos, ArrowGenerator::Color* col)
{
	mProgress += mStepSize;
	if(mProgress >=  1.0)
		return false;
	
	const Vec3 global_dir = _target - _source;
	const Vec3 side_dir = global_dir.normalisedCopy();
	const Vec3 up_dir = side_dir.crossProduct(arch_dir).crossProduct(side_dir).normalisedCopy();
	
	const double r = radius();
	const double up_fac = r * std::sin(std::pow(mProgress, 2.0 * middle)* pi) * height;
	const double side_fac = r * -std::cos(mProgress * pi);
	
	*pos = middle_point()
			+ up_dir * up_fac
			+ side_dir * side_fac;
	
	return true;
}

bool ArrowFromCircle::supportColor()
{
	return false;
}

void ArrowFromCircle::step_size(double _step_dist)
{
	const double distance = pi * radius();
	mStepSize = _step_dist / distance;
}

ArrowGenerator::Vec3 ArrowFromCircle::middle_point() const
{
	return _source + (_target - _source) * 0.5;
}

double ArrowFromCircle::radius() const
{
	return _source.distance(_target) / 2.0;
}
