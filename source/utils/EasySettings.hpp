#pragma once
#ifndef _EASY_SETTINGS_HPP
#define _EASY_SETTINGS_HPP


#include "rapidxml.hpp"
#include "rxml/value.hpp"
#include "LexicalCasts.hpp"


struct BoolAlpha {
    bool data = false;
    BoolAlpha() {}
    BoolAlpha( bool data ) : data(data) {}
    operator bool() const { return data; }
    friend std::ostream & operator << ( std::ostream &out, BoolAlpha b ) {
        out << std::boolalpha << b.data;
        return out;
    }
    friend std::istream & operator >> ( std::istream &in, BoolAlpha &b ) {
        in >> std::boolalpha >> b.data;
        return in;
    }
};

template<typename T>
T SettingsValue(rapidxml::xml_node<>& node, std::string path)
{
	assert(rxml::get(&node, path));
	return boost::lexical_cast<T>(rxml::value(node, path));
}


#endif