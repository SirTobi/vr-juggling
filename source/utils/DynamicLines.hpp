#ifndef _DYNAMIC_LINES_H_
#define _DYNAMIC_LINES_H_
 
#include "DynamicRenderable.hpp"
#include <vector>
 
class DynamicLines : public DynamicRenderable
{
  typedef Ogre::Vector3 Vector3;
  typedef Ogre::ColourValue Color;
  typedef Ogre::Quaternion Quaternion;
  typedef Ogre::Camera Camera;
  typedef Ogre::Real Real;
  typedef Ogre::RenderOperation::OperationType OperationType;
 
  struct Point
  {
	Point() {}
	Point(Vector3 pos, Vector3 normal, Color color)
		: pos(pos)
		, normal(normal)
		, color(color)
	{}

	  Vector3 pos;
	  Vector3 normal;
	  Color color;
  };
  
  struct Line
  {
		Line() {}
	Line(const Point& p1, const Point& p2)
	{
		points[0] = p1;
		points[1] = p2;
	}

	Point points[2];
  };
public:
  /// Constructor - see setOperationType() for description of argument.
  DynamicLines();
  virtual ~DynamicLines();
 
  /// Add a point to the point list
  void addStraight(const Vector3 &p, const Vector3& offset, const Color& color = Color::White);
 
  /// Add a point to the point list
  void addLine(const Vector3 &p1, const Vector3 &p2, const Color& color = Color::White);
  
  /// Return the total number of points in the point list
  unsigned short getNumPoints(void) const;
 
  /// Remove all points from the point list
  void clear();
 
  /// Call this to update the hardware buffer after making changes.  
  void update();
 
protected:
  /// Implementation DynamicRenderable, creates a simple vertex-only decl
  virtual void createVertexDeclaration();
  /// Implementation DynamicRenderable, pushes point list out to hardware memory
  virtual void fillHardwareBuffers();
 
private:
	
  /// Add a point to the point list
  void addPoint(const Vector3 &p);
  
private:
  std::vector<Line> mLines;
  bool mDirty;
};
 
#endif