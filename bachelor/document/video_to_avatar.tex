
\section{Vom Video-Stream zum virtuellen Avatar}

% einleitung
Wie oben erwähnt, ist es die Aufgabe der Anwendung den Avatar und seine Umgebung darzustellen und dabei die Bewegungen des Benutzers genau auf eben diesen abzubilden. Hebt der Anwender das rechte Bein, soll auch der Avatar das rechte Bein heben. Mit der Oculus Rift soll es für den Benutzer so aussehen, als würde das Bein des Avatars in der VR genau seinem Bein in der Realität entsprechen. Nur so kann sich der Anwender als voll in die Simulation integriert empfinden. Dafür müssen zunächst die Bewegungen des Benutzers durch das Programm registriert und anschließend auf den Avatar übertragen werden. In diesem Projekt wird dazu eine Kinect verwendet. Wie eingangs erwähnt, nimmt diese sowohl Farbbilder als auch Tiefenbilder auf. Mit dem OpenNI SDK\footnote{Quellcode vom OpenNI SDK auf Github: https://github.com/OpenNI/OpenNI} werden diese Bilder ausgelesen. OpenNI ist ein Framework, welches es Entwicklern ermöglicht allgemein für Natural User Interfaces zu entwickeln, unabhängig von den Geräten, die zum Schluss tatsächlich verwendet werden. Das Framework selber ist quelloffen und kann kostenlos verwendet werden. Zusätzlich ermöglicht das OpenNI SDK den Zugriff auf verschiedene Middlewares, also Software die nicht selber zum Framework gehört, aber die Funktionalität erweitert, wenn sie entsprechend hinzugefügt wird. So kann die Middleware NiTE\footnote{NiTE wurde von PrimeSense entwickelt und stand kostenlos aber nicht offen zur Verfügung. Seid dem Kauf von PrimeSense durch Apple wird NiTE nicht mehr offiziell verteilt.} verwendet werden, um aus Farb- und Tiefenbildern das Skelett des Benutzers zu generieren. Genau auf diesen Skelettdaten basiert der Avatar in diesem Projekt.

% kinect koordinatensystem

Um ein Skelett zu beschreiben, liefert NiTE eine Reihe von Vektoren, die jeweils einem Gelenk des menschlichen Körpers entsprechen. In Figur \ref{fig:skeleton_joints} sind die 15 Gelenke abgebildet. Die Vektoren liegen dabei in einem Koordinatensystem, welches die Kinect als Ursprung hat und in Millimetern angegeben wird. Die Z-Achse entspricht im Positiven dem, was vor der Kinect liegt (also der Richtung in die die Kamera zeigt), und im Negativen dem, was hinter der Kinect liegt. Die Y-Achse gibt die vertikale Verschiebung an (Positiv entspricht Oben und Negativ entspricht Unten) und die X-Achse die horizontale Verschiebung zur Seite (Positiv entspricht Links und Negativ entspricht Rechts von der Kamera). Figur \ref{fig:kinect_coord} zeigt das Koordinatensystem der Kinect.


\begin{figure}
	\subfigure[NiTE-Skelett mit den 15 Gelenken aus der Sicht der Kinect.]{

		\includegraphics[width=0.49\textwidth]{images/skeleton.png}
		\label{fig:skeleton_joints}
	}
	\hfill
	\subfigure[Koordinatensystem der Kinect.]{

		\includegraphics[width=0.49\textwidth]{images/kinect_coord.png}
		\label{fig:kinect_coord}
	}
	\caption{Kinect}
\end{figure}

% ogre koordinatensystem
% wie ogre funktioniert


\begin{wrapfigure}{r}{0.49\textwidth}
	\includegraphics[width=0.49\textwidth]{images/kinect_orientation.png}
	\caption{Mögliche Neigung der Kinect um einen Winkel $\alpha$}
	\label{fig:kinect_rotation}
\end{wrapfigure}


Zum Rendern der Szene inklusive Avatar und Umgebung, wird die quelloffene Bibliothek Ogre3D\footnote{Internetseite von Ogre3D: http://www.ogre3d.org/} verwendet. Ogre3D verwaltet alle zu zeichnenden Objekte in einer so genannten Szene. Alles was die Anwendung tun muss, ist die Objekte, die angezeigt werden sollen, zu der Szene hinzuzufügen und zu bewegen. Jedes Objekt hat dabei eine Position und eine Ausrichtung. Ogre3D stellt viele vordefinierte Objekte zur Verfügung. Neben 3D-Modellen, Lichtern, Partikelsystemen und Texten, können auch selbst definierte Objekte benutzt werden. Auch der Avatar wird als solch ein Objekt hinzugefügt und mithilfe der Skelettdaten animiert. Als Aussehen für den Avatar wurde in diesem Projekt eine einfache Darstellung des Skelettes gewählt: die Vektoren des Skelettes, welche NiTE liefert, müssen also lediglich in das Koordinatensystem von Ogre3D umgerechnet und entsprechend mit Linien verbunden werden. Da Ogre3D für das Koordinatensystem keine Einheit vorschreibt, wurde für das Projekt die Einheit Meter festgesetzt. Zwei Ortsvektoren, die sich in einer ihrer Komponenten um eins von einander unterscheiden, wären demnach genau einen Meter von einander entfernt. Somit entspricht das Verhältnis von Kinect-Koordinatensystem zu Ogre3D-Koordinatensystem genau $0,001$. Zusätzlich zu den verschiedenen Einheiten kommt noch eine eventuelle Neigung um die X-Achse durch das Drehgelenk der Kinect, wie in Figur \ref{fig:kinect_rotation} gezeigt. Jeder Vektor muss deshalb noch um die X-Achse rotiert werden. In Listing \ref{lst:kinect_transform} ist die komplette Transformation gelistet.

\begin{lstlisting}[caption=Transformation eines Vektors aus dem Kinect- zum Ogre3D-Koordinatensystem.,literate={µ}{$\alpha$}1 {ä}{{\"a}}1,label=lst:kinect_transform]
// Neigungswinkel der Kinect
rotation = µ

// Verhältnis zwischen den beiden Koordinatensystemen
skalierung = 0.001

// Rotationsmatrix, welche einen Vektor um µ um die X-Achse dreht
R = RotationsMatrix(rotation, vector(1, 0, 0))

// bildet einen Vector aus dem Kinect-Koordinatensystem
// auf einen Vector aus dem Ogre3D-Koordinatensystem ab
vector transform(vector v)
{
	v' = v * skalierung
	return R * v';
}
\end{lstlisting}


Zusätzlich zum Skelett wird der Avatar noch durch zwei Hände dargestellt. Dabei handelt es sich um ein herkömmliches 3D-Modell, das für die rechte Hand gespiegelt wird. Das Modell ist so animiert, dass es eine geschlossene sowie offene Hand und die Bewegung dazwischen darstellen kann. Dadurch kann der Benutzer in der Simulation sehen, ob er gerade zugreift oder nicht.

\begin{figure}[b!]
	\centering
	\includegraphics[width=1\textwidth]{images/stereoskopic.png}
	\caption{Stereoskopische Sicht des Avatars von der Simulation. Beide Bildausschnitte sind leicht versetzt und ermöglichen so das räumliche Sehen.}
	\label{fig:stereo}
\end{figure}

Sind alle Objekte aktualisiert, übernimmt Ogre3D das Rendern der Szene. Dies geschieht aus der Sicht eines zuvor erzeugten Kamera-Objekts. Dieses wird genau an der Position in der Simulation gehalten, welche der des Kopfes des Avatars entspricht. Um die stereoskopische Sicht der Oculus Rift zu unterstützen, müssen in diesem Fall sogar zwei Kameras erzeugt werden, die dann im Abstand der Augen die Szene jeweils aus verschiedenen Blickwinkeln aufzeichnen und dann auf je einer Bildhälfte anzeigen.  Der Benutzer wird dann mit jeweils einem Auge eines der beiden Bildausschnitte (wie in Abbildung \ref{fig:stereo}) sehen und somit das Gefühl von räumlichem Sehen haben. Je nachdem wie der Benutzer den Kopf dreht, werden auch beide Kameras in der VR gedreht, so dass die Kopfbewegung auch genau simuliert wird. Da die erste Entwicklerversion der Oculus Rift die Kopfbewegung nur mit Beschleunigungssensoren und 3-Achsen-Gyrometer misst, kann es bei heftigen Bewegungen seitens des Benutzers zu Drehungen der Sicht kommen. Schaut der Benutzer zum Beispiel initial sowohl in der VR als auch in der Realität gerade nach vorne, macht dann heftige Bewegungen mit dem Körper und dreht dabei die Sicht, kann das dazu führen, dass bei erneutem nach vorne Schauen (in der Realität) die Sicht in der VR nun nicht mehr geradeaus gerichtet ist, sondern leicht nach rechts oder links gedreht ist. Um die Sicht wieder zurückzusetzen, muss sich der Benutzer gerade zur Kinect ausrichten und dann die Leertaste drücken oder diese von einem Helfer drücken lassen. Dieses Problem kommt ab der zweiten Entwicklerversion der Oculus Rift nicht mehr vor, da eine zusätzliche Kamera von außen diese Ungenauigkeit ausgleicht.
% umwandlung



% kalibrierung des skelettes ?