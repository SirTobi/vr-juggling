\section{Aufbau des Simulationsprogramms}

Den Hauptteil dieser Arbeit stellt das in C++ geschriebene Simulationsprogramm dar. In Abbildung \ref{fig:arch} ist die grobe Architektur gezeigt. Den Mittelpunkt bildet dabei die CourseState-Klasse. Sie verbindet und steuert alle anderen Komponente. Sie selber ist ein Zustand im Advanced Ogre Framework\footnote{Advanced Ogre Framework: http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Advanced+Ogre+Framework}. Dabei handelt es sich um ein Framework, welches die Verwendung von Ogre3D erleichtert. Es stellt dazu verschiedene Werkzeuge zum Initialisieren und Verwalten von Ogre3D zur Verfügung. Zur Verwaltung gehört unter anderem das Konzept von Zuständen. Zu jedem Zeitpunkt kann ein Zustand aktiv sein, der die momentane Logik des Programms vorgibt. Soll zum Beispiel ein Menü geöffnet werden, wird einfach ein neuer Menü-Zustand erzeugt und als aktiv markiert. Soll das Menü wieder geschlossen werden, wird der ursprüngliche Zustand wiederhergestellt. Genau um so einen Zustand handelt es sich auch bei CourseState.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{images/arch.png}
	\caption{Architektur des Simulationsprogramms. Grüne Boxen sind interne Komponenten; Blaue sind Externe.}
	\label{fig:arch}
\end{figure}


Damit die Simulation mit der Oculus Rift benutzt werden kann, müssen einige zusätzliche Schritte durchgeführt werden. Dazu gehört zum Beispiel das Initialisieren und die Kommunikation mit der Oculus Rift. Außerdem muss das Rendern der Szene einige besondere Eigenschaften aufweisen. So muss diese aus verschiedenen Blickwinkeln mit einem Linseneffekt gerendert werden, um die stereoskopische Sicht zu erhalten. Um diese Funktionen zu realisieren, bietet sich OgreOculus\footnote{OgreOculus: http://www.ogre3d.org/forums/viewtopic.php?f=5\&t=76970} an. Diese kleine Bibliothek kapselt die Funktionalität und bindet Ogre3D und Oculus Rift zusammen.

Um die Skelettdaten von OpenNI zu erhalten, werden der KinectManager und der SkeletonProvider benutzt. Ersterer kümmert sich um die Initialisierung und die Verwaltung von OpenNI im Allgemeinen und Letzterer um den Zugriff auf die Skelett-Komponente im Besonderen. Auch die Transformation der Koordinatensysteme wird hier durchgeführt.

Eine weitere wichtige Komponente stellt die PhysicEngine dar. Sie verwaltet und berechnet die geworfenen Bälle. Auch das Fangen und Werfen gehört zu ihren Aufgaben. Werden Bälle geworfen, verändert in bestimmten Fällen die ThrowAssistance  deren Flugbahn.


Wie oben beschrieben enthält das Simulationsprogramm den Jonglierkurs nicht selber. Dieser wird außerhalb in Form von Skriptdatein gespeichert. Die Skripte sind in Angelscript\footnote{Webseite von Angelscript: http://www.angelcode.com/angelscript/} geschrieben. Dabei handelt es sich um eine Programmiersprache, die von einem Interpreter ausgeführt wird. Dieser Interpreter wird mittels Bibliothek in das Simulationsprogramm eingebunden. Um eine Interaktion mit dem Programm zu ermöglichen, können C++ Klassen und Funktionen bei Angelscript registriert werden und dann aus einem Skript verwendet bzw. aufgerufen werden. Die Kapselung übernimmt die ScriptEngine. Sie stellt insbesondere Funktionen zur Ablaufsteuerung des Kurses bereit.
\begin{wrapfigure}{r}{0.50\textwidth}
	\centering
	\includegraphics[scale=1]{images/frame_flow.png}
	\caption{Ablauf eines Frames. Grüne Boxen werden interne behandelt, Blaue externe.}
	\label{fig:loop_proc}
\end{wrapfigure}

Wird das Programm gestartet, werden zunächst alle Komponenten initialisiert. Danach wird immer wieder die selbe Schleife durchlaufen, um die Simulation zu rendern und zu aktualisieren. In Abbildung \ref{fig:loop_proc} ist der Ablauf dargestellt. Ein Durchlauf der Schleife wird als ein Frame bezeichnet. Dabei werden zunächst die neusten Daten der Kinect verarbeitet und das Skelett aktualisiert. Danach werden die virtuellen Kameras auf Grund von Daten der Oculus Rift neu ausgerichtet. Im Anschluss werden alle physikalischen Objekte aktualisiert. Dabei spielt die Zeit seit dem letzten Frame eine entscheidende Rolle: Ist seit dem letzten Frame doppelt so viel Zeit vergangen, wie im Frame davor, müssen auch alle Objekte doppelt so weit gesetzt werden. Zuletzt müssen nur noch alle Ereignisse an die Skriptkomponente  weitergereicht werden. Nachdem alle Anpassungen abgeschlossen sind, wird Ogre3D die Szene automatisch rendern. Danach beginnt der nächste Frame.



\subsection{Physik-Engine}

In der Simulation werden alle Bälle zentral von der Physik-Engine verwaltet. Außerdem gehören das Fangen und Werfen zu ihren Aufgaben. Die Physik selber findet in einem euklidischen Raum ohne Kollision statt und stellt somit nur eine Approximation der Wirklichkeit dar. Diese ist aber ausreichend, um beim Benutzer einen realistischen Eindruck zu erwecken.

Ein einzelner Ball kann in diesem Raum durch drei Eigenschaften ausreichend für die benötigten physikalische Vorgänge beschrieben werden. Diese sind

\begin{enumerate}
	\item ein Radius,
	\item eine Position als dreidimensionaler Ortsvektor,
	\item eine Bewegung als dreidimensionaler Bewegungsvektor, also ein Richtungsvektor, der in seiner Länge die Geschwindigkeit enthält.
\end{enumerate}

Eine Masse benötigen die Bälle in der VR nicht, da keine Kollision untereinander oder mit anderen Objekten stattfindet. Alle Bälle werden diskret, also schrittweise, aktualisiert. Da die Zeit eine erhebliche Rolle bei der Bewegung der Bälle spielt, würde deren Bewegung direkt von den Framezeiten der Simulation abhängen, wenn jeder Ball pro Frame einmal aktualisiert werden würde. Um diese Abhängigkeit aufzulösen, wird jeder Ball, unabhängig von der Dauer eines Frames, 100 mal pro Sekunde aktualisiert. Dies wird mit einem Zeitkonto umgesetzt. In Listing \ref{lst:time_balance} wird die genaue Funktionsweise verdeutlicht.

\begin{lstlisting}[caption=Funktionsweise des Zeitkontos für die Berechnung der Bälle.,label=lst:time_balance,literate={ü}{{\"u}}1 {ä}{{\"a}}1]
aktualisierungen_pro_sekunde = 100;
ball_aktualisierungs_zeit = 1 / aktualisierungen_pro_sekunde;

...

// Befüllen des Zeitkontos
zeitkonto += zeit_seit_letztem_frame;

// Solange das Zeitkonto genug Zeit enthält ...
while(zeitkonto >= ball_aktualisierungs_zeit)
{
	zeitkonto -= ball_aktualisierungs_zeit;
	
	// ... aktualisiere alle Bälle
	...
}

\end{lstlisting}

Jeder Ball wird dann in zwei Schritten aktualisiert. Um die neue Position des Balls zu erhalten, wird die Bewegung, unter Berücksichtigung der Zeit, addiert. Damit der Eindruck von Gravitation entsteht, wird zuvor die Gravitation als Bewegungsvektor auf die Bewegung addiert. Die Berechnung ist in \ref{lst:ball_update} verdeutlicht.


\begin{lstlisting}[caption=Aktualisierung eines Balls.,label=lst:ball_update,literate={ü}{{\"u}}1 {ä}{{\"a}}1]
aktualisierungen_pro_sekunde = 100;
ball_aktualisierungs_zeit = 1 / aktualisierungen_pro_sekunde;

gravitation = 9.81; // m/(s^2)
dt = ball_aktualisierungs_zeit;
...
// aktualisiere Bewegung
ball.bewegung -= dt * gravitation;

// aktualisiere Position
ball.position += dt * ball.bewegung;
\end{lstlisting}



\subsubsection{Werfen}

Bevor ein Ball geworfen werden kann, muss er sich in einer der beiden Hände befinden. Er wird dabei in einer zusätzlichen Liste geführt und jeden Frame an eine bestimmte Position auf der Hand gesetzt. Lässt der Benutzer den Ball los, also wechselt seine Hand von Geschlossen zu Offen, beginnt der Wurf-Prozess. Der Ball kann nicht sofort losfliegen, wenn das Öffnen der Hand registriert wird, da der Ball beim echten Wurf meist erst später die Hand wirklich verlässt. Normalerweise öffnet man die Hand mit dem Ball, beschleunigt dabei aber noch die Aufwärtsbewegung der Hand. Der Ball bleibt auf der Handfläche und wird weiter beschleunigt. Erst wenn man die Richtung, in die sich die Hand bewegt, ändert oder die Geschwindigkeit der Hand verringert, hebt der Ball ab und fliegt selbstständig. Um dies in der Simulation nachzubilden, wird der Ball beim Loslassen in einen Wurfzustand versetzt. Erst wenn die Hand langsamer wird oder die Richtung grob ändert, verlässt der Ball die Hand.

Löst ein Ball sich von der Hand, weil er geworfen wurde, muss seine neue Flugrichtung und Geschwindigkeit berechnet werden. Wegen Ungenauigkeiten seitens der Kinect bei schnellen Bewegungen, wie dem Werfen, hat es sich als nicht praktikabel herausgestellt, lediglich die zwei letzten Handpositionen zu betrachten, um die Geschwindigkeit zu errechnen. Stattdessen werden alle Positionen der Hände in den letzten 600 Millisekunden gespeichert und die maximal aufgetretene Geschwindigkeit als endgültige Geschwindigkeit des Balls gesetzt. Die Wurfrichtung wird aus den zwei letzten zwei Positionen der Hand berechnet. In Listing \ref{lst:throw_proc} ist der Ablauf gelistet.

\begin{lstlisting}[caption=Berechnung des Bewegungsvektors beim Wurf.,label=lst:throw_proc,literate={ü}{{\"u}}1 {ä}{{\"a}}1]
vector wurfvektor(list<vector> hand_verlauf)
{
	vector letzte_pos = letztes_element(hand_verlauf);
	vector vorletzte_pos = vorletztes_element(hand_verlauf);

	vector richtung = letzte - vorletzte;	
	richtung = normalisiere(richtung);
	
	return richtung * max_geschwindigkeit(hand_verlauf);
}
\end{lstlisting}



\subsubsection{Fangen}

Fliegt der Ball in der Luft, kann er durch Fangen in eine der beiden Hände genommen werden. Greift der Benutzer zu, wird überprüft, ob sich ein Ball in der Umgebung der Hand befindet. Ist das der Fall, wird der Ball an die Hand geheftet. Um das Fangen zu vereinfachen, wird die Überprüfung noch in jedem Frame nach dem Zugreifen durchgeführt, bis 800 Millisekunden vergangen sind.


\subsection{Hilfestellung für den Wurf}

Zwar unternimmt das Programm einige Anstrengungen, die Richtung und Geschwindigkeit beim Wurf so zu bestimmen, dass es sich realistisch anfühlt, allerdings ist es ohne die genaue Verfolgung der Hand selber schwierig, Bälle präzise zu werfen. Das gilt unter anderem auch für den Wurf eines Jonglierballs von einer zur anderen Hand. Hierbei spielt die Bewegung der Finger und der Mittelhand eine entscheidende Rolle. Der Benutzer soll sich aber nicht darauf konzentrieren müssen, den Ball innerhalb der abstrakten Physik richtig zu werfen, sondern seine Aufmerksamkeit ganz auf die Bewegungsabläufe und den Rhythmus beim Werfen richten. Deshalb wurden drei verschiedene Hilfen in das Programm eingebaut, die im Falle eines Wurfes hinzu geschaltet werden können. Alle drei Hilfen verändern lediglich den Bewegungsvektor des geworfenen Balls. Im Folgenden werden die drei Hilfsmechanismen vorgestellt.



\subsubsection{Achsen-Multiplikator}

Der Einfachste, der drei Hilfsmechanismen, ist der Achsen-Multiplikator. Dabei wird jede Komponente des Bewegungsvektors eines Balls mit den Komponenten eines vorgegeben Vektors multipliziert:

\newcount\colveccount
\newcommand*\colvec[1]{
        \global\colveccount#1
        \begin{pmatrix}
        \colvecnext
}
\def\colvecnext#1{
        #1
        \global\advance\colveccount-1
        \ifnum\colveccount>0
                \\
                \expandafter\colvecnext
        \else
                \end{pmatrix}
        \fi
}

\[
	f(\vec{v}) = \colvec{3}
					{\vec{v}.x \times \vec{w}.x}
					{\vec{v}.y \times \vec{w}.y}
					{\vec{v}.z \times \vec{w}.z}
\]

wobei $\vec{v}$ der Bewegungsvektor des Balls und $\vec{w}$ der vorgegebene Vektor ist. Mit dieser Methode kann zum Beispiel der Ball beim Wurf auf einer Achse festgehalten werden. Setzt man bei $\vec{w}$ nur die Y-Komponente auf Eins und die anderen Beiden auf Null, wird sich der Ball nach dem Wurf nur auf der Y-Achse bewegen und ist dann leichter zu fangen, da die Hand nicht seitlich bewegt werden muss.

\subsubsection{Ebenen-Ausrichtung}

Um den Wurf des Balls von einer Hand zur anderen zu vereinfachen, soll der Ball sich nach dem Wurf nur auf einer Ebene bewegen, die entlang der Gravitation durch beide Hände verläuft. So muss der Benutzer sich nicht darauf konzentrieren, dass der Ball zu ihm hin, oder von ihm weg fliegt. Die Funktion $f$, zur Berechnung des neuen Bewegungsvektors, sieht wie folgt aus:

\[
	p(\vec{h}) = \cfrac{(\vec{g} \times \vec{h}) \times \vec{g}}
						{\|(\vec{g} \times \vec{h}) \times \vec{g}\|}
\]
\[
	f(\vec{v}, \vec{h}) = 
		\left[
			\left(
				\vec{v}
				\cdot
				p(\vec{h})
			\right)
			p(\vec{h})
		\right]
		+
		\left[
			\left(
				\vec{v}
				\cdot
				\cfrac{
						\vec{g}
					}{
						\|\vec{g}\|
					}		
			\right)
			\times \cfrac{\vec{g}}{\|\vec{g}\|}
		\right]
\]

wobei $\vec{v}$ der Bewegungsvektor des Balls, $\vec{h}$ der Vektor von einer zur anderen Hand, $\vec{g}$ die Gravitation und $p(\vec{h})$ eine Hilfsfunktion ist, die $\vec{h}$ auf eine normalisierte Senkrechte zu $\vec{g}$ projiziert, ohne dabei die Rotation um $\vec{g}$ zu verändern.




\subsubsection{Auto-Parabel}

Die vorherigen Hilfsmechanismen haben den Flug des Balls nur leicht abgewandelt. Das folgende Verfahren ersetzt den Flug des Balls vollständig durch einen errechneten Wert. Beim Loslassen des Balls mit der einen Hand soll die Flugbahn des Balls so gesetzt werden, dass er automatisch zur anderen Hand fliegt und dabei eine Parabel von mindestens $h$ Meter Höhe beschreibt. Dadurch muss der Benutzer sich nicht mehr konzentrieren, wie er den Ball wirft, sondern nur noch wann er ihn wirft. Die Berechnung des Bewegungsvektors durch die Funktion $f$ sieht wie folgt aus:

\[
	p(\vec{v}) = \cfrac{(\vec{g} \times \vec{v}) \times \vec{g}}
						{\|(\vec{g} \times \vec{v}) \times \vec{g}\|}
\]

\[
	t(x) = 
		\left(
			\sqrt{2\cfrac{x}{\|g\|}}
		\right)
\]

\[
	f(\vec{v}) =
		t(h)
		\vec{g}
		+
		\left(
			\cfrac{
				(\vec{v} \cdot p(\vec{v})) p(\vec{v})
			}{
			t(h)
			+
			t(h - \|
					\cfrac{\vec{v} \cdot \vec{g}}{\vec{g}\cdot \vec{g}} \vec{g}			
				\|)}
		\right)
		p(\vec{v})				
\]

wobei $\vec{v}$ der Vektor vom Ball zur fangenden Hand, $h$ die gewünschte Höhe der Parabel, $\vec{g}$ die Gravitation und $p(\vec{v})$ eine Hilfsfunktion ist, die $\vec{v}$ auf eine normalisierte Senkrechte zu $\vec{g}$ projiziert, ohne dabei die Rotation um $\vec{g}$ zu verändern. Eine Herleitung der Formel ist im Anhang unter \ref{appdx:auto_parabel} zu finden.

Da bei dieser Methode die Flugbahn unabhängig von der Wurfbewegung ist, führen alle Bewegungen zum gleichen Ergebnis. Zum Beispiel wird der Ball perfekt zur anderen Hand fliegen, auch wenn der Benutzer den Ball nur loslässt und die Hand gar nicht bewegt. Um dem vorzubeugen, werden zwei Bedingungen für den Wurf geprüft. Zunächst wird geprüft, ob der Ball beim Werfen eine gewisse Mindestgeschwindigkeit aufweist. Danach wird der Winkel des Wurfes zur Gravitationsrichtung gemessen. Ist der Ball zu langsam oder der gemessene Winkel zu klein, wird keine Anpassung der Bewegung des Balls vorgenommen.

 
\subsection{Kursschnittstelle}

Um den Simulationskurs selber zu programmieren wird die Skriptsprache Angelscript verwendet. So ist es möglich den Inhalt des Kurses vom eigentlichen Programm zu trennen. Interaktion findet über vom Programm registrierte Funktionen und Klassen statt. Dabei ruft das Simulationsprogramm nach der Initialisierung und dem Laden aller Skripte die Funktion „main()“ auf. Das Programm wird dabei so lange unterbrochen, bis das Skript entweder selber zurückkehrt oder eine entsprechende Funktion aufruft, die die Ausführung des Skriptes unterbricht. So unterbricht die Funktion „Quit()“ nicht nur die Ausführung des Programms, sondern beendet es.

Um vom Skript aus auf ein Ereignis zu warten, werden so genannte „ActionToken“ unterstützt. Ruft man eine Funktion auf, die nicht sofort ausgeführt werden kann oder für die Bedingungen erfüllt sein müssen, kann man auf die Erfüllung warten. Dazu geben die entsprechenden Funktionen jeweils ein „ActionToken“ zurück. Dieses kann man mit \mbox{„WaitFor(ActionToken tk)“} registrieren und dann die Ausführung des Skriptes mit „Wait()“ so lange unterbrechen, bis alle registrierten Aktionen ausgeführt sind. In Listing \ref{lst:action_token_perv} ist ein Beispielskript, welches eine Sound-Datei abspielt und dann auf das Ende des Sounds wartet.

\begin{lstlisting}[caption=Warten auf das Abspielen einer Sound-Datei.,label=lst:action_token_perv,literate={ü}{{\"u}}1 {ä}{{\"a}}1]

// Sound abspielen und ActionToken speichern
ActionToken tk = PlaySound("sounds/test.ogg");

// ActionToken registrieren
WaitFor(tk);

// Auf Ende des Sounds warten
Wait();

...
\end{lstlisting}

Wird die Ausführung des Skriptes unterbrochen, läuft die normale Programm-Schleife solange weiter, bis die Ausführung des Skripts fortgesetzt werden kann. Kehrt das Skript aus der „main()“-Funktion zurück, wird das Programm beendet.

Der Skriptsprache werden verschiedenste Funktionen zur Verfügung gestellt, damit möglichst viele Situationen für die Simulation erstellt werden können. So können 3D-Modelle, Partikelsysteme, Jonglierbälle, 3D-Pfeile, Lichter und Texte erzeugt, bewegt, gedreht, verändert und entfernt werden. Außerdem kann auf verschiedene Daten, wie das Skelett des Spielers, zugegriffen werden. Im Listing \ref{lst:example_user_angelscript} ist eine beispielhafte Funktion gelistet, die solange wartet, bis der Benutzer seine beiden Hände über den Kopf gehoben hat und danach Konfetti schießt.
\newpage
\begin{lstlisting}[caption={Beispielfunktion, die darauf wartet, dass der Benutzer beide Hände über den Kopf hebt.},label=lst:example_user_angelscript,literate={ü}{{\"u}}1 {ä}{{\"a}}1 {ß}{ss}1]

void WaitForUserInput()
{
	// Erzeuge ein Partikelsystem mit dem Namen "Konfetti-PS",
	// welches Konfetti feuert
	@ps = CreateParticleSystem("Konfetti-PS", "Konfetti");
	ps.position(-2, 4, 0);		// <- Setzte Position
	ps.lookAt(vector(0, 1, 0));	// <- Setzte Ausrichtung
	
	int times = 0;
	
	// Der Benutzer muss seine Hände mindestens 2 Sekunden
	// über dem Kopf Halten
	while(times < 4)
	{
		// Warte eine halbe Sekunde
		Wait(500);
		
		// Hole Positionen des Kopfes und der Hände
		vector head = GetPlayerHead();	// <- Kopf
		vector r = GetPlayerHand(true);	// <- rechte Hand
		vector l = GetPlayerHand(false);	// <- linke Hand

		// Sind beide Hände über dem Kopf?
		if(head.y <= r.y && head.y <= l.y)
		{
			// Ja
			++times;
		}else{
			// Nein
			times = 0;
		}
	}
	
	// Schieße Konfetti
	ps.fire();
}



\end{lstlisting}






















