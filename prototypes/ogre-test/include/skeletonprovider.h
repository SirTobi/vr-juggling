#ifndef SKELETONPROVIDER_H
#define SKELETONPROVIDER_H


#include <XnCppWrapper.h>
#include <OgreVector3.h>
#include <string>


inline Ogre::Vector3 conv_vec(const XnVector3D& org)
{
	return Ogre::Vector3(org.X, org.Y, org.Z);
}


class SkeletonProvider
{
public:
	SkeletonProvider(const std::string& config_file);
	~SkeletonProvider();
	
	void start();
	void update();
	bool get_joint(XnSkeletonJoint joint, XnSkeletonJointTransformation& data);
	
	int get_user_count();
private:
	void onNewUser(xn::UserGenerator& generator, XnUserID nId);
	void onUserLost(xn::UserGenerator& generator, XnUserID nId);
	void onUserPose(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId);
	void onCalibrationStart(xn::SkeletonCapability& capability, XnUserID nId);
	void onCalibrationComplete(xn::SkeletonCapability& capability, XnUserID nId, XnCalibrationStatus eStatus);
	
	static void Callback_NewUser(xn::UserGenerator& generator, XnUserID nId, void* pthis);
	static void Callback_UserLost(xn::UserGenerator& generator, XnUserID nId, void* pthis);
	static void Callback_UserPose(xn::PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId, void* pthis);
	static void Callback_CalibrationStart(xn::SkeletonCapability& capability, XnUserID nId, void* pthis);
	static void Callback_CalibrationComplete(xn::SkeletonCapability& capability, XnUserID nId, XnCalibrationStatus eStatus, void* pthis);
private:
	xn::Context mContext;
	xn::ScriptNode mScriptNode;
	xn::DepthGenerator mDepthGenerator;
	xn::UserGenerator mUserGenerator;
	bool mNeedPoseDetection;
	XnChar mCurrentPose[20];
	XnUserID mCurrentUser;
};

#endif // SKELETONPROVIDER_H
