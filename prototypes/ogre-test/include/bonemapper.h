#ifndef BONEMAPPER_H
#define BONEMAPPER_H

#include <OgreEntity.h>
#include <OgreBone.h>
#include <XnOpenNI.h>
#include <vector>
#include <tuple>

class SkeletonProvider;


class BoneMapper
{
public:
	typedef std::vector<std::tuple<Ogre::String, XnSkeletonJoint, Ogre::Quaternion>> MappingTemplate;
private:
	struct Mapping
	{
		Ogre::Bone* bone;
		XnSkeletonJoint joint;
	};
public:
	BoneMapper(Ogre::Entity* entity, const MappingTemplate& templ);
	~BoneMapper();
	
	void apply(SkeletonProvider& provider);
private:
	Ogre::Entity* mEntity;
	std::vector<Mapping> mJointMapping;
};

#endif // BONEMAPPER_H
