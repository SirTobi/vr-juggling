#ifndef SKELETONDEBUG_H
#define SKELETONDEBUG_H

#include <Ogre.h>

class SkeletonDebug
{
public:
    SkeletonDebug(Ogre::Entity *entity, Ogre::SceneManager *man);
    ~SkeletonDebug();

    // For future use
    void setUniformScale(bool enable){mEnableUniformScaling = enable;}
    bool isUniformScale(){return mEnableUniformScaling;}

private:
    Ogre::Entity *mEntity;
    Ogre::MaterialPtr mMat;
    Ogre::MeshPtr mMeshPtr;
    Ogre::SceneManager *mSceneMan;

    bool mEnableUniformScaling;
    float mScale;

    void createMaterial();
    void createMesh();
};

#endif