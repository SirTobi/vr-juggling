#include <OgreSkeleton.h>
#include <OgreSkeletonInstance.h>
#include <OgreLogManager.h>
#include "bonemapper.h"
#include <skeletonprovider.h>



BoneMapper::BoneMapper(Ogre::Entity* entity, const MappingTemplate& templ)
	: mEntity(entity)
{
	Ogre::SkeletonInstance* skeleton = entity->getSkeleton();
	
	for(auto& mapping : templ)
	{
		
		if(skeleton->hasBone(std::get<0>(mapping)))
		{
			Ogre::Bone* bone = skeleton->getBone(std::get<0>(mapping));
			bone->setManuallyControlled(true);
			bone->setInheritOrientation(false);
			bone->resetOrientation();
			bone->setOrientation(std::get<2>(mapping));

			bone->setInitialState();
			
			mJointMapping.push_back({bone, std::get<1>(mapping)});
		}else{
			Ogre::LogManager::getSingletonPtr()->logMessage("While mapping joint '" + std::get<0>(mapping) + "': " + entity->getName() + " has no such bone!");
		}
	}
}

BoneMapper::~BoneMapper()
{

}

void BoneMapper::apply(SkeletonProvider& provider)
{
	for(Mapping& m : mJointMapping)
	{
		auto& bone = m.bone;
		
		
		Ogre::Quaternion qI = bone->getInitialOrientation();
		Ogre::Quaternion newQ = Ogre::Quaternion::IDENTITY;
		
		XnSkeletonJointTransformation joint;
		if(provider.get_joint(m.joint, joint))
		{
			auto& jointOri = joint.orientation;
			// transform!!
			Ogre::Matrix3 matOri(jointOri.orientation.elements[0],-jointOri.orientation.elements[1],jointOri.orientation.elements[2],
								-jointOri.orientation.elements[3],jointOri.orientation.elements[4],-jointOri.orientation.elements[5],
								jointOri.orientation.elements[6],-jointOri.orientation.elements[7],jointOri.orientation.elements[8]);
			Ogre::Quaternion q;
			newQ.FromRotationMatrix(matOri);
			bone->resetOrientation(); //in order for the conversion from world to local to work.
			newQ = bone->convertWorldToLocalOrientation(newQ);
			bone->setOrientation(newQ*qI);	
		}else{
			bone->resetToInitialState();
		}
	}
}
