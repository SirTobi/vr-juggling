pkg_check_modules (LIBUSB REQUIRED libusb-1.0)
include_directories (${LIBUSB_INCLUDE_DIRS})
link_directories (${LIBUSB_LIBRARY_DIRS})

FIND_PATH(OPENNI_INCLUDE_DIRS
	  "XnOpenNI.h" "OpenNIConfig.h"
	  HINTS
		"$ENV{OPEN_NI_INCLUDE}"
		"/usr/include/ni"
		"/usr/include/openni"
		"/opt/ros/groovy/include/openni_camera")


FIND_LIBRARY(OPENNI_LIBRARIES
	     NAMES
		OpenNI libOpenNI
	     HINTS
		$ENV{OPEN_NI_LIB}
		"/usr/lib")


LINK_DIRECTORIES($ENV{OPEN_NI_LIB})

FIND_PATH(XN_NITE_INCLUDE
		"libXnVNite.so"
	HINTS
		"$ENV{XN_NITE_INSTALL_PATH}"
		"/usr/include/nite")

FIND_LIBRARY(XN_NITE_LIBRARY
	NAMES
		libXnVNite_1_5_2.so
	HINTS
		$ENV{XN_NITE_INSTALL_PATH}
		"/usr/lib")


#LINK_DIRECTORIES($ENV{XN_NITE_LIB_INSTALL_PATH} "/usr/lib")
#INCLUDE_DIRECTORIES(${XN_NITE_INCLUDE})
#LINK_LIBRARIES(${XN_NITE_LIBRARY})

#target_link_libraries (your_project_name ${SSE_FLAGS} ${OPEN_NI_LIBRARIES} ${XN_NITE_LIBRARIES})
