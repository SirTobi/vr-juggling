from pymouse import PyMouse
import serial

ser = serial.Serial('/dev/ttyACM0', 9600)
m = PyMouse()
while True:
  serialString = ser.readline()
  foo = serialString.split(";");
  if len(foo) != 2:
    print "wrong transmission format. ignoring message: ", serialString
    continue
  pin = int(foo[0])
  status = int(foo[1])
  print "pin: ", pin
  print "status: ", status
  
  mousePos = m.position()
  if pin == 2:
    if status == 0:
      print "releasing left mouse button at pos: ", mousePos
      m.release(mousePos[0], mousePos[1])
    else:
      print "pushing left mouse button at pos: ", mousePos
      m.press(mousePos[0], mousePos[1])
      
  if pin == 3:
    if status == 0:
      print "releasing right mouse button at pos: ", mousePos
      m.release(mousePos[0], mousePos[1],2)
    else:
      print "pushing right mouse button at pos: ", mousePos
      m.press(mousePos[0], mousePos[1],2)