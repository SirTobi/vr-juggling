#./speech "$1".ogg o


count=0

while read line;
do
	if [ "$count" == "0" ]; then
		let count++
		./speech.sh "$count" "$line"
	else
		c=$count
		./speech.sh _"$c" "$line"
		let count++
		oggCat "$count".ogg "$c".ogg _"$c".ogg
		rm "$c".ogg
		rm _"$c".ogg
	fi

done < $1

mv "$count".ogg "$1".ogg
#rm _tmp.ogg
