wget -q -U Mozilla -O _temp.mp3 "http://translate.google.com/translate_tts?ie=UTF-8&tl=de&q=$(tr ' ' + <<< "$2")"

mpg321 _temp.mp3 -q -w - | oggenc - -Q -o "$1".ogg
rm _temp.mp3

echo Output: "$1".ogg
