#include "waiter.as"



namespace Tutorial {
namespace JuggleTwo {

bool Pinned = false;
bool IntroMode = true;
ActionToken GWaitToken;

int CatchesNeeded = 10;

SceneNode@ ps1;
SceneNode@ ps2;
SceneNode@ GText;

SceneNode@ ballnode1;
SceneNode@ ballnode2;

Ball@ rightBall;
Ball@ leftBall;

bool LeftBallThrowingOK = false;

void UpdateText()
{
	if(IntroMode)
		return;

	SetFrontText("Versuche den Ablauf noch " + CatchesNeeded + " mal zu wiederholen!");
}


void CatchedBall(bool right)
{
	if(rightBall.inHand(!GRightHander) && leftBall.inHand(GRightHander))
	{
		if(LeftBallThrowingOK)
		{
			// success
			ps1.firePS();
			ps2.firePS();
			PlayCompliment(0.6);
			PlaySound("ok.ogg", 5);
			
			if(IntroMode)
			{
				IntroMode = false;
				SetTimeFactor(0.5);
			}else{
				SetTimeFactor(TimeFactor + 0.05);
			}
			
			--CatchesNeeded;
			log("Success!");
		}

		LeftBallThrowingOK = false;
	}
	else if(rightBall.position.y < 0 || leftBall.position.y < 0)
	{
		// unsuccess	
		log("Unsuccess!");
	}else {
		// wait... for other call
		return;
	}

	log("reset...");
	Pinned = false;
	CloseCustomAction(GWaitToken);
}

class RightBallController : IBallController
{
	vector pinpos;
	vector pinmov;
	bool ready_to_pin = false;
	bool catched = false;

	void apply(Ball@ ball, float dt)
	{
		if(Pinned)
		{
			ball.position = pinpos;
			ball.movement = pinmov;
		}
	
		if(ball.inHand(!GRightHander) && !catched)
		{
			catched = true;
			log("catched right ball");
			CatchedBall(true);
		}else if(ball.position.y < 0)
		{
			log("lose right ball!");
			CatchedBall(true);
		}else if(IntroMode && ball.movement.y < 0 && ready_to_pin && !Pinned)
		{
			Pinned = true;
			ready_to_pin = false;
			pinpos = ball.position;
			pinmov = ball.movement;
			play_intro2();
		}else if(IntroMode && !ball.inHand(GRightHander) && !ready_to_pin && ball.movement.y > 0)
		{
			ready_to_pin = true;
		}
	}
}

class LeftBallController : IBallController
{
	bool catched = false;
	bool released = false;
	void apply(Ball@ ball, float dt)
	{
		if(!released && !ball.inHand(!GRightHander))
		{
			if(IntroMode)
			{
				log("pinned = false");
				Pinned = false;
			}
			
			// check if the right ball has the requiered properties
			if(rightBall.movement.y < 0)
			{
				// other ball is falling
				LeftBallThrowingOK = true;
			}else{
				PlaySound("fail.ogg", 5);
			}
			
			log("released left ball");
			released = true;
		}else if(ball.inHand(GRightHander) && !catched)
		{
			catched = true;
			log("Catched left ball");
			CatchedBall(false);
		}else if(ball.position.y < 0)
		{
			log("lose left ball!");
			CatchedBall(false);
		}
	}
}


void SetupBalls()
{
	log("Setup balls!");
	SceneNode@ rightBallNode = ballnode1;
	SceneNode@ leftBallNode = ballnode2;

	@rightBall = CreateBall(rightBallNode, GJuggleBallRadius);
	@leftBall = CreateBall(leftBallNode, GJuggleBallRadius);

	rightBall.addController(RightBallController());
	leftBall.addController(LeftBallController());

	PlaySound("teleport.ogg");
	rightBall.toHand(GRightHander);
	leftBall.toHand(!GRightHander);
}


void play_intro()
{
	SetDescription("Werfen Sie den roten Ball in die andere Hand.\nErst wenn er am Scheitelpunkt angekommen ist,\nwerfen Sie den gruenen Ball.\nVersuchen Sie dann beide Baelle zu fangen.\n\nTipp: Denken Sie daran, die Haende\n nicht zu wild auf und ab zu bewegen!");
	SetFrontText("Fangen wir nun mit zwei Baellen an.");
	WaitSound("speech/two_1.ogg", 4000);
	
	SetFrontText("Um Ihnen den Einstieg zu erleichtern,\nlaueft die Simulation nun in einem Zeitraffer.\nDer Zeitfaktor wird oben rechts angezeigt.\nEin Wert von 0.5 bedeutet,\ndass die Zeit nur halb so schnell vergeht.");
	SetTimeFactor(0.3f);
	WaitSound("speech/two_2.ogg");
	
	SetFrontText("Der Zeitraffer gilt nur fuer Baelle die gerade fliegen.\nSie selbst und alle gehaltenen Baelle bewegen\nsich mit normaler Geschwindigkeit.");
	WaitSound("speech/two_3.ogg");
	
	SetFrontText("Ich werde den Zeitfaktor mit der Zeit erhoehen,\num ihn an die Realitaet anzupassen.");
	WaitSound("speech/two_4.ogg");
	
	SetFrontText("Ich werde ihnen die Uebung nun interaktiv erklaeren.\nSollte ihnen waehrend der Uebung ein Ball auf den Boden fallen,\nwird die Ausgangsstellung wiederhergestellt.\nLassen Sie also einfach einen Ball fallen,\nwenn Sie in die Ausgangsstellung zurueck moechten");
	WaitSound("speech/two_5.ogg");
	
	SetFrontText("Werfen Sie zunaechst den roten Ball.");
	WaitSound("speech/two_6.ogg");
}

bool Intro2Played = false;

void play_intro2()
{
	if(Intro2Played)
		return;
		
	Intro2Played = true;
	SetFrontText("Wenn sich der rote Ball am Scheitelpunkt befindet,\nwerfen Sie den gruenen Ball.\nFangen Sie dann beide Baelle mit der jeweils anderen Hand.\nWiederholen Sie diesen Ablauf\n(ohne die Haende zu tauschen)");
	PlaySound("speech/two_7.ogg");
}

void run()
{
	log("Enter juggle two ball tutorial...");
	SetLection("Lektion 3: Zwei Baelle", "");
	
	play_intro();

	// create ball nodes
	@ballnode1 = CreateMesh("JuggleTwoBallTut-JuggleBall-1", "JuggleBallMesh");
	@ballnode2 = CreateMesh("JuggleTwoBallTut-JuggleBall-2", "JuggleBallMesh");
	ballnode1.setMaterialName("Tutorial/RedBall");
	ballnode2.setMaterialName("Tutorial/GreenBall");


	// Create Particle Systems
	@ps1 = CreateParticleSystem("JuggleTwoBall-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("JuggleTwoBall-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));

	// Set ThrowAssistance
	SetAssistanceMode(TAM_Full);

	while(CatchesNeeded > 0)
	{
			SetupBalls();
			log(CatchesNeeded + " catches left!");
			
			UpdateText();
			GWaitToken = OpenCustomAction();
			WaitUntil(GWaitToken);

			LeftBallThrowingOK = false;
			DeleteBall(rightBall);
			DeleteBall(leftBall);
	}

	DeleteBall(rightBall);
	DeleteBall(leftBall);
	ballnode1.visible(false);
	ballnode2.visible(false);

	Wait(2000);
	log("Leave juggle two ball tutorial...");
}


}}
