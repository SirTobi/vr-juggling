

void WaitUntil(ActionToken tk, uint dur = 0)
{
	WaitFor(tk);
	Wait(dur);
}

class WaitForImpl
{
	void opUShr_r(ActionToken tk) const
	{
		WaitFor(tk);
	} 
}

const WaitForImpl qwait;

class WaitImpl
{
	void opUShr_r(ActionToken tk) const
	{
		WaitUntil(tk);
	} 
}

const WaitImpl wait;
