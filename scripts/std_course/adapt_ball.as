#include "waiter.as"



namespace Tutorial {
namespace AdaptBall {

ActionToken GWaitToken;

int CatchesNeeded = 3;
float TargetHeight = 0.4;

bool BallWasOverTarget = false;

SceneNode@ ps1;
SceneNode@ ps2;
SceneNode@ TargetRect;
SceneNode@ GText;


void ResetSuccess()
{
	TargetRect.setMaterialName("AdaptBall/TargetFail");
}

void SetTargetRect()
{
	vector head = GetPlayerHead();
	head.y += TargetHeight;
	TargetRect.position(head);
	TargetHeight *= 1.2;
	ResetSuccess();
}

class ResetController : IBallController
{
	private CatchController@ catchContrl;

	ResetController(CatchController@ _cc)
	{
		@catchContrl = _cc;
	}


	void apply(Ball@ ball, float dt)
	{
		if(ball.position.y < 0)
		{
			// attach to dom hand
			if(BallWasOverTarget)
			{
				//PlaySound("fail.ogg", 5.0f);
				PlayGut();
			}
			
			log("Ball hit the ground... reset to dom hand!");
			ball.toHand(GRightHander);
			PlaySound("teleport.ogg");	
			catchContrl.inHand = true;
			ResetSuccess();
		}else if(ball.position.y >= TargetRect.position().y && !BallWasOverTarget && !ball.inHand(GRightHander))
		{
			PlaySound("up.ogg", 90);
			BallWasOverTarget = true;
			TargetRect.setMaterialName("AdaptBall/TargetSuccess");
		}
	}
}


class CatchController : IBallController
{
	bool inHand = true;
	void apply(Ball@ ball, float dt)
	{
		if(ball.inHand(GRightHander))
		{
			if(!inHand && BallWasOverTarget)
			{
				// Got you
				ps1.firePS();
				ps2.firePS();
				PlaySound("ok.ogg", 5.0f);
				PlayCompliment(0.5);

				--CatchesNeeded;

				if(CatchesNeeded <= 0)
					CloseCustomAction(GWaitToken);
				else
					SetTargetRect();
			}
			inHand = true;
			BallWasOverTarget = false;
		}else{
			inHand = false;
		}
	}
}



void play_intro()
{
	SetDescription("Werfen Sie den Ball bis zur Plattform\nund fangen Sie ihn danach wieder mit der\ngleichen Hand.\n\nTipp: Der Ball bewegt sich nur auf der Y-Achse");
	SetFrontText("Ueber ihnen ist nun eine Plattform erschienen.\nVersuchen Sie den Ball bis zur Plattform zu werfen.\nFangen Sie den Ball danach wieder auf.\nFuer diese Lektion wird sich der Ball\nnur auf der Y-Achse bewegen.");
	WaitSound("speech/adb_1.ogg");
	
	SetFrontText("");
}


void run()
{
	log("Enter BallAdaption Tutorial...");
	SetLection("Lektion 1: Einfuerung", "2) Hochwerfen");

	// Rect
	@TargetRect = CreateMesh("BallAdaption-TargetRect", "Rect.mesh");
	SetTargetRect();
	
	play_intro();
	
	// create ball
	SceneNode@ ballnode = CreateMesh("ThrowBall", "JuggleBallMesh");
	ballnode.setMaterialName("Tutorial/RedBall");
	Ball@ ball = CreateBall(ballnode, GJuggleBallRadius);
	CatchController@ c = CatchController();
	ball.addController(c);
	ball.addController(ResetController(c));
	ball.toHand(GRightHander);

	// Create Particle Systems
	@ps1 = CreateParticleSystem("BallAdaption-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("BallAdaption-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));


	// Set ThrowAssistance
	SetAssistanceMode(TAM_Axis);

	GWaitToken = OpenCustomAction();
	WaitUntil(GWaitToken);
	
	Wait(3000);

	ballnode.visible(false);
	DeleteBall(ball);
	ps1.visible(false);
	ps2.visible(false);
	TargetRect.visible(false);
	

	log("Leave BallAdaption Tutorial...");
}


}}
