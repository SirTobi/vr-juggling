#include "waiter.as"



namespace Tutorial {
namespace JuggleOne {

ActionToken GWaitToken;

int CatchesNeeded = 10;
bool rightIsCurrent = GRightHander;

Arrow ThrowArrow("JuggleOneBall-Arrow");

SceneNode@ ps1;
SceneNode@ ps2;
SceneNode@ GText;


void UpdateText()
{
	SetFrontText("Versuchen Sie den Ball noch " + CatchesNeeded + " mal in\ndie andere Hand zu werfen.");
}


void UpdateArrow()
{
	vector from = GetPlayerHand(rightIsCurrent);
	vector to = GetPlayerHand(!rightIsCurrent);
	if(to.y > from.y)
	{
		from.y = to.y;
	}else{
		to.y = from.y;
	}
	to.y += 0.1;
	from.y += 0.1;

	ThrowArrow.point_circle(from, to, 2.0);
	ThrowArrow.update();
}



class ResetController : IBallController
{
	private CatchController@ catchContrl;

	ResetController(CatchController@ _cc)
	{
		@catchContrl = _cc;
	}


	void apply(Ball@ ball, float dt)
	{
		if(ball.position.y < 0)
		{
			// attach to right hand
			log("Ball hit the ground... reset to right hand!");
			ball.toHand(rightIsCurrent);
			PlaySound("teleport.ogg");
			catchContrl.inHand = true;
		}
	}
}


class CatchController : IBallController
{
	bool inHand = true;
	void apply(Ball@ ball, float dt)
	{
		// update arrow
		if(CatchesNeeded > 6)
		{
			UpdateArrow();
		}

		if(ball.inHand(!rightIsCurrent))
		{
			// Got you
			ps1.firePS();
			ps2.firePS();

			PlaySound("ok.ogg", 5.0f);
			PlayCompliment(0.3);

			--CatchesNeeded;
			UpdateText();

			if(CatchesNeeded <= 6)
			{
				ThrowArrow.getSceneNode().visible(false);
			}

			if(CatchesNeeded <= 0)
				CloseCustomAction(GWaitToken);
			
			rightIsCurrent = !rightIsCurrent;
			inHand = true;
		}else{
			inHand = false;
		}
	}
}


void play_intro()
{
	SetDescription("Werfen Sie den Ball " + CatchesNeeded + " mal in die andere Hand.\n\nTipp: Der Ball fliegt automatisch im Bogen an die Position,\ndie Ihre andere Hand beim Loslassen hatte.\nBewegen Sie also ihre Haende nur minimal auf und ab!");
	SetFrontText("Beginnen wir nun mit dem Jonglieren.\nZunaechst mit nur einem Ball.");
	WaitSound("speech/one_1.ogg");
	
	SetFrontText("Werfen Sie den Ball von ihrer dominanten Hand\n in die Andere und fangen Sie den Ball dort.\nDanach wiederholen Sie den Vorgang mit der anderen Hand.");
	WaitSound("speech/one_2.ogg");
	
	
	SetFrontText("In diesem und allen weiteren Lektionen werden Sie\nvon der Simulation beim Werfen unterstuetzt.\nVersuchen Sie beim Werfen die Arme nur minimal nach oben\nzu bewegen. Der Ball fliegt dann automatisch zur anderen Hand.\nLassen Sie den Ball nur los,\nwird er trotzdem auf den Boden fallen.");
	WaitSound("speech/one_3.ogg");
}



void run()
{
	rightIsCurrent = GRightHander;
	log("Enter juggle one ball tutorial...");
	SetLection("Lektion 2: Ein Ball", "");
	
	play_intro();
	UpdateText();

	// create ball
	SceneNode@ ballnode = CreateMesh("JuggleOneBallTut-JuggleBall", "JuggleBallMesh");
	ballnode.setMaterialName("Tutorial/RedBall");
	Ball@ ball = CreateBall(ballnode, GJuggleBallRadius);
	CatchController@ c = CatchController();
	ball.addController(c);
	ball.addController(ResetController(c));
	ball.toHand(rightIsCurrent);

	// Create Particle Systems
	@ps1 = CreateParticleSystem("JuggleOneBall-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("JuggleOneBall-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));

	// setup arrow
	ThrowArrow.faceDirection = vector(0, 0, 1);
	ThrowArrow.material = "Tutorial/AlphaArrow";
	ThrowArrow.widthFactor = 0.3;

	// Set ThrowAssistance
	SetAssistanceMode(TAM_Full);

	GWaitToken = OpenCustomAction();
	WaitUntil(GWaitToken);
	Wait(3000);

	DeleteBall(ball);
	ballnode.visible(false);
	ThrowArrow.getSceneNode().visible(false);

	log("Leave juggle one ball tutorial...");
}


}}
