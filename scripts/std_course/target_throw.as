namespace Tutorial {
namespace TargetThrow {

float GTargetRadius = 0.4f;
SceneNode@ GTargetNode;

int GHitsNeeded = 6;
ActionToken GWaitToken;

SceneNode@ ps1;
SceneNode@ ps2;

SceneNode@ GText;


void updateText()
{
	SetFrontText("Versuche noch\n" + GHitsNeeded + " mal zu treffen!");
}

class ResetController : IBallController
{
	Arrow arr("show-target-arrow");

	ResetController()
	{
		arr.material = "TargetThrow/Arrow";
		arr.faceDirection = vector(0, 1, 0);
	}

	void apply(Ball@ ball, float dt)
	{
		// Create an arrow
		if(ball.inHand(GRightHander) && GHitsNeeded > 4)
		{
			vector target = GTargetNode.position();
			target.z += GTargetRadius;
			target.y += GTargetRadius /2.0f;
		
			arr.getSceneNode().visible(true);
			arr.point_circle(ball.position, target, 0.5);
			arr.update();
		}else{
			arr.getSceneNode().visible(false);
		}
	
		if(ball.position.y < 0)
		{
			// attach to right hand
			log("Ball hit the ground... reset to right hand!");
			ball.toHand(GRightHander);
			PlaySound("teleport.ogg");
		}
	}
}

class HitController : IBallController
{
	void apply(Ball@ ball, float dt)
	{
		if(ball.position.distance(GTargetNode.position()) < GTargetRadius + GJuggleBallRadius)
		{
			log("Ball hit the target!");
			--GHitsNeeded;
			updateText();
			ps1.firePS();
			ps2.firePS();
			PlaySound("ok.ogg", 8);
			PlayCompliment(0.8f);
			GTargetNode.setMaterialName("TargetThrow/TargetHit");
			ball.position = vector(1000, 1000, 1000);
			CloseCustomAction(GWaitToken);
		}
	}
}


void play_intro()
{
	SetFrontText("Vor sich sehen Sie nun eine Kugel.\nVersuchen Sie den Jonglierball in die Kugel zu werfen.");
	SetDescription("Werfen Sie den roten Jonglierball\nsechs mal in die Zielkugel.\n\nTipp: Werfen Sie von unten.\n(Wie beim Bowlen)");
	WaitSound("speech/tt_1.ogg");
}


void run()
{
	log("Enter Tutorial TargetThrow");
	SetLection("Lektion 1: Einfuerung", "3) Zielwerfen");


	// create target
	CreateSphereMesh("ThrowTargetMesh", GTargetRadius);
	@GTargetNode = @CreateMesh("target", "ThrowTargetMesh");
	GTargetNode.setMaterialName("TargetThrow/TargetNormal");
	GTargetNode.position(0, GTargetRadius, -0.5);
	
	play_intro();
	
	// create ball
	SceneNode@ ballnode = CreateMesh("TargetThrowBall", "JuggleBallMesh");
	ballnode.setMaterialName("Tutorial/RedBall");
	Ball@ ball = CreateBall(ballnode, GJuggleBallRadius);
	ball.addController(HitController());
	ball.addController(ResetController());
	
	// Create Particle Systems
	@ps1 = CreateParticleSystem("target_throw_ps-1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("target_throw_ps-2", "Tutorial/SuccessParticles");
	ps1.position(-2, 3, 0);
	ps2.position(2, 3, 0);
	ps1.lookAt(ps2.position());
	ps2.lookAt(ps1.position());
	
	updateText();
	
	SetAssistanceMode(TAM_None);
	
	// wait...
	while(GHitsNeeded > 0)
	{
		ball.toHand(GRightHander);
		GTargetNode.setMaterialName("TargetThrow/TargetNormal");
		GWaitToken = OpenCustomAction();
		WaitUntil(GWaitToken);
		
		Wait(400);
		PlaySound("teleport.ogg");
	}
	
	Wait(3000);
	
	
	
	log("Tutorial TargetThrow done!");
	
	
	// clear scene
	DeleteBall(ball);
	GTargetNode.visible(false);
	//GText.visible(false);
	ballnode.visible(false);
	
	//DeleteSceneNode(GTargetNode);
	//DeleteSceneNode(ps1);
	//DeleteSceneNode(ps2);
}

}}
