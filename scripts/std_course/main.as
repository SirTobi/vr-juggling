typedef uint AT;

#include "waiter.as"
#include "calibration.as"
#include "introduction.as"
#include "target_throw.as"
#include "adapt_ball.as"
#include "juggle_one_ball.as"
#include "juggle_two_balls.as"
#include "juggle_three_balls.as"
#include "free_training.as"


// settings
bool GRightHander = true;

// global text stuff
SceneNode@ GLectionText;
SceneNode@ GSubLectionText;
SceneNode@ GDescriptionText;
SceneNode@ GFrontText;
SceneNode@ GTimeFactorStatusText;

void CreateMainTexts()
{
	@GLectionText = CreateText("LectionText", "Tutorial/Lection", true);
	GLectionText.position(-2, 3, -1);
	GLectionText.setText("Lektion 1: Anpassung");
	GLectionText.setTextAlignment(THA_Left, TVA_Above);

	@GSubLectionText = CreateText("SubLectionText", "Tutorial/SubLection", true);
	GSubLectionText.position(-2, 2.9, -1);
	GSubLectionText.setText("1) Einfuehrung");
	GSubLectionText.setTextAlignment(THA_Left, TVA_Below);
	GSubLectionText.setTextSize(0.16);

	@GDescriptionText = CreateText("DescriptionText", "Tutorial/Description", true);
	GDescriptionText.position(2, 1.8, 2);
	GDescriptionText.setTextAlignment(THA_Center, TVA_Center);
	GDescriptionText.setTextSize(0.16);
	GDescriptionText.setText("");
	GDescriptionText.lookAt(vector(0, 1.8, 2));

	@GFrontText = CreateText("FrontText", "Tutorial/Front", true);
	GFrontText.position(0, 1.6, -1);
	GFrontText.setText("");
	GFrontText.setTextAlignment(THA_Center, TVA_Center);
	GFrontText.setTextSize(0.20);
	
	@GTimeFactorStatusText = CreateText("TimeFactorText", "Tutorial/Front", true);
	GTimeFactorStatusText.position(-2, 2.5, -1);
	GTimeFactorStatusText.setText("");
	GTimeFactorStatusText.setTextAlignment(THA_Left, TVA_Below);
	GTimeFactorStatusText.setTextSize(0.20);
}

void SetLection(string lection, string subLection)
{
	GLectionText.setText(lection);
	GSubLectionText.setText(subLection);
}

void SetDescription(string desc)
{
	GDescriptionText.setText(desc);
}

void SetFrontText(string desc)
{
	GFrontText.setText(desc);
}

void SetTimeFactor(float f)
{
	TimeFactor = f;
	GTimeFactorStatusText.setText("Zeitfaktor: " + f);
}

void HideTimeFactor()
{
	GTimeFactorStatusText.setText("");
}

void WaitForUserInput()
{
	int times = 0;
	while(times < 4)
	{
		Wait(500);
		vector head = GetPlayerHead();
		vector r = GetPlayerHand(true);
		vector l = GetPlayerHand(false);

		if(head.y <= r.y && head.y <= l.y)
		{
			if(times == 0)
				PlaySound("up.ogg");
			++times;
		}else{
			if(times > 0)
				PlaySound("down.ogg");
			times = 0;
		}
	}
	WaitUntil(PlaySound("ok.ogg", 8.0f));
}

void WaitSound(const string &in soundfile, int waitAfter = 1000)
{
	WaitUntil(PlaySound(soundfile));
	Wait(waitAfter);
}

array<string> Compliments = {
	"speech/lob/gut.ogg",
	"speech/lob/neid.ogg",
	"speech/lob/sgut.ogg",
	"speech/lob/tgut.ogg",
	"speech/lob/wgut.ogg"
}; 

array<string> Guts = {
	"speech/mut/fast.ogg",
	"speech/mut/knapp.ogg",
	"speech/mut/next.ogg",
	"speech/mut/versuch.ogg"
};

array<string> Times = {
	"speech/zeit/schneller.ogg",
	"speech/zeit/1.ogg",
	"speech/zeit/2.ogg",
	"speech/zeit/3.ogg"	
	//"speech/zeit/max.ogg"
};

void PlayCompliment( float p = 1.0)
{
	if(Rand() % 10000 > 10000 * p)
	{
		return;
	}
	
	PlaySound(Compliments[Rand() % Compliments.get_length()]);
}

void PlayGut( float p = 1.0)
{
	if(Rand() % 10000 > 10000 * p)
	{
		return;
	}
	
	PlaySound(Guts[Rand() % Guts.get_length()]);
}

void PlayTimes( float p = 1.0)
{
	if(Rand() % 10000 > 10000 * p)
	{
		return;
	}
	
	PlaySound(Times[Rand() % Times.get_length()]);
}

SceneNode@ makeLineEnv()
{
	array<vector> lines = { 
	
		vector(2, 0, 0), vector(-2, 0, 0),
		vector(2, 0, 1), vector(-2, 0, 1),
		vector(2, 0, 2), vector(-2, 0, 2),
		vector(2, 0, 3), vector(-2, 0, 3),
		vector(2, 0, 4), vector(-2, 0, 4),
	
		vector(2, 4, 0), vector(2, 0, 0),
		vector(-2, 4, 0), vector(-2, 0, 0),
		vector(2, 4, 4), vector(2, 0, 4),
		vector(-2, 4, 4), vector(-2, 0, 4),
	
		vector(-2, 0, 0), vector(-2, 0, 4),
		vector(-1, 0, 0), vector(-1, 0, 4),
		vector(0, 0, 0), vector(0, 0, 4),
		vector(1, 0, 0), vector(1, 0, 4),
		vector(2, 0, 0), vector(2, 0,4)
	};
	
	return CreateLineEntity("env", lines, "BaseWhiteNoLighting");
}


void setup_environment()
{
	//create ground
	SceneNode@ ground = CreateGround("Ground", "Test/Ground");

	// create light
	SceneNode@ light = CreateLight("testlight");
	light.position(1, 2, 2);

	// create environment
	SceneNode@ env = makeLineEnv();
	env.position(0, 0, 0);

	CreateMainTexts();
}


float GJuggleBallRadius = 0.05;

void init_std_meshs()
{
	// create some std meshs
	CreateSphereMesh("JuggleBallMesh", GJuggleBallRadius);
}

void boot()
{
	// wait for an user
	log("Wait for kinect!");
	WaitUntil(InitKinect());
	log("Kinect ok!");
	GetSceneNode("Skeleton").visible(true);

	// calibrate user
	log("Calibrate player...");
	calibrate_player_position();
	log("Calibration done!");
}


void main()
{
	setup_environment();

	// setup std stuff
	init_std_meshs();

	boot();

	WaitForUserInput();
	
	Tutorial::Intro::run();
	Tutorial::AdaptBall::run();
	Tutorial::TargetThrow::run();
	Tutorial::JuggleOne::run();
	Tutorial::JuggleTwo::run();
	Tutorial::JuggleThree::run();
	Tutorial::Free::run();

	
	Wait(1000000);
}
