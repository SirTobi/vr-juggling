#include "waiter.as"



namespace Tutorial {
namespace JuggleThree {

ActionToken GWaitToken;

float YOtherBallSpeed = 0.1;
int CatchesNeeded = 10;

SceneNode@ ps1;
SceneNode@ ps2;
SceneNode@ GText;

SceneNode@ ballnode1;
SceneNode@ ballnode2;
SceneNode@ ballnode3;

Ball@ rightBall;
Ball@ rightBall2;
Ball@ leftBall;

bool LeftBallThrowingOK = false;
bool RightBallThrowingOK = false;


void UpdateText()
{
	SetFrontText("Versuche den Ablauf noch " + CatchesNeeded + "\nmal zu wiederholen!");
}

void CatchedBall()
{
	if(rightBall.inHand(!GRightHander) && leftBall.inHand(GRightHander) && rightBall2.inHand(!GRightHander))
	{
		log("All balls in correct hand!");
		if(LeftBallThrowingOK && RightBallThrowingOK)
		{
			// success
			ps1.firePS();
			ps2.firePS();
			PlaySound("ok.ogg", 5);
			PlayTimes(0.9);
			
			SetTimeFactor(TimeFactor + 0.04);
			--CatchesNeeded;
			UpdateText();
			log("Success!");
		}

		LeftBallThrowingOK = false;
		RightBallThrowingOK = false;
	}
	else if(rightBall.position.y < 0
			|| leftBall.position.y < 0
			|| rightBall2.position.y < 0)
	{
		// unsuccess	
		log("Unsuccess!");
	}else {
		// wait... for other call
		return;
	}

	log("reset...");
	CloseCustomAction(GWaitToken);
}

class RightBallController : IBallController
{
	bool catched = false;

	void apply(Ball@ ball, float dt)
	{
		if(ball.inHand(!GRightHander) && !catched)
		{
			catched = true;
			log("catched right ball");
			CatchedBall();
		}else if(ball.position.y < 0)
		{
			log("lose right ball!");
			CatchedBall();
		}if(catched && !ball.inHand(!GRightHander))
		{
			ball.toHand(!GRightHander);
		}
	}
}

class LeftBallController : IBallController
{
	bool catched = false;
	bool released = false;
	void apply(Ball@ ball, float dt)
	{
		if(!released && !ball.inHand(!GRightHander))
		{
			// check if the right ball has the requiered properties
			if(rightBall.movement.y < YOtherBallSpeed)
			{
				// other ball is falling
				LeftBallThrowingOK = true;
			}else{
				PlaySound("fail.ogg", 8);
			}
			log("released left ball");
			released = true;
		}else if(ball.inHand(GRightHander) && !catched)
		{
			catched = true;
			log("Catched left ball");
			CatchedBall();
		}else if(ball.position.y < 0)
		{
			log("lose left ball!");
			CatchedBall();
		}
	}
}


class RightBall2Controller : IBallController
{
	bool catched = false;
	bool released = false;
	void apply(Ball@ ball, float dt)
	{
		if(!released && !ball.inHand(GRightHander))
		{
			// check if the left ball has the requiered properties
			if(leftBall.movement.y < YOtherBallSpeed)
			{
				// other ball is falling
				RightBallThrowingOK = true;
			}else{
				PlaySound("fail.ogg", 8);
			}
			log("released right ball 2");
			released = true;
		}else if(ball.inHand(!GRightHander) && !catched)
		{
			catched = true;
			log("Catched right ball 2");
			CatchedBall();
		}else if(ball.position.y < 0)
		{
			log("lose right ball 2!");
			CatchedBall();
		}
	}
}



void SetupBalls()
{
	log("Setup balls!");

	@rightBall = CreateBall(ballnode1, GJuggleBallRadius);
	@leftBall = CreateBall(ballnode2, GJuggleBallRadius);
	@rightBall2 = CreateBall(ballnode3, GJuggleBallRadius);

	rightBall.addController(RightBallController());
	leftBall.addController(LeftBallController());
	rightBall2.addController(RightBall2Controller());

	PlaySound("teleport.ogg");
	rightBall2.toHand(GRightHander);
	rightBall.toHand(GRightHander);
	leftBall.toHand(!GRightHander);
}


void play_intro()
{
	SetDescription("Werfen Sie zunaechst den roten Ball\nzur anderen Hand. Hat er seinen Scheitelpunkt\nerreicht, werfen Sie den gruenen Ball.\nHat dieser auch seinen Scheitelpunkt erreicht\nwerfen sie zuletzt den blauen Ball.\nVergessen Sie nicht alle Baelle wieder\nzu fangen.\n\nTipp: Wird ihnen diese Lektion zu schnell,\nhalten Sie die Arme weiter auseinander.");

	SetFrontText("Nun kommt ein letzter Ball hinzu.\nDie folgende Uebung ist der vorherigen sehr aehnlich.");
	WaitSound("speech/three_1.ogg");

	SetFrontText("Werfen Sie den roten und den gruenen Ball\nwie in der Uebung eben. Werfen Sie den\nblauen Ball, wenn der Gruene seinen\nScheitelpunkt erreicht hat. Fangen Sie alle Baelle.\nDer rote und der blaue Ball befinden sich\ndann in einer Hand!");
	WaitSound("speech/three_2.ogg");

	SetFrontText("Faellt ihnen ein Ball auf den Boden,\nwerden alle Baelle in die Ausgangsstellung\nzurueckteleportiert.");
	WaitSound("speech/three_3.ogg");
}

void run()
{
	log("Enter juggle three ball tutorial...");
	SetLection("Lektion 4: Drei Baelle", "");

	play_intro();
	UpdateText();

	// create ball nodes
	@ballnode1 = CreateMesh("JuggleThreeBallTut-JuggleBall-1", "JuggleBallMesh");
	@ballnode2 = CreateMesh("JuggleThreeBallTut-JuggleBall-2", "JuggleBallMesh");
	@ballnode3 = CreateMesh("JuggleThreeBallTut-JuggleBall-3", "JuggleBallMesh");
	ballnode1.setMaterialName("Tutorial/RedBall");
	ballnode2.setMaterialName("Tutorial/GreenBall");
	ballnode3.setMaterialName("Tutorial/BlueBall");


	// Create Particle Systems
	@ps1 = CreateParticleSystem("JuggleThreeBall-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("JuggleThreeBall-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));

	// Set ThrowAssistance
	SetAssistanceMode(TAM_Full);
	SetTimeFactor(0.4);

	while(CatchesNeeded > 0)
	{
			SetupBalls();
			log(CatchesNeeded + " catches left!");

			GWaitToken = OpenCustomAction();
			WaitUntil(GWaitToken);

			LeftBallThrowingOK = false;
			RightBallThrowingOK = false;
			DeleteBall(rightBall);
			DeleteBall(leftBall);
			DeleteBall(rightBall2);
	}

	ballnode1.visible(false);
	ballnode2.visible(false);
	ballnode3.visible(false);
	Wait(2000);
	log("Leave juggle three ball tutorial...");
}


}}
