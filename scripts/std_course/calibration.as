#include "waiter.as"




class Calibration
{
	SceneNode@ skeleton;

	Calibration()
	{
		@skeleton = @GetSceneNode("Skeleton");
	}

	void run()
	{
		calibratePosition();
	}

	private void calibratePosition()
	{
		Wait(1000);
		float tollerance = 0.025f;
		vector target_pos(0, 0, 0);
		do {
			tollerance *= 2.0f;
			vector head = GetPlayerHead();
			target_pos = vector(0, GetPlayerHeight(), 2);
			log("Player height: " + GetPlayerHeight());

			vector mov = target_pos;
			mov -= head;
			PlayerOffset += mov;
			log("Head: " + head.x + ", " + head.y + ", " + head.z);
			log("Target: " + target_pos.x + ", " + target_pos.y + ", " + target_pos.z);
			log("Mov: " + mov.x + ", " + mov.y + ", " + mov.z);

			Wait(1000);
		} while(GetPlayerHead().distance(target_pos) > tollerance);

		log("PlayerOffset: " + PlayerOffset.x + ", " + PlayerOffset.y + ", " + PlayerOffset.z);
		log("with tollerance: " + tollerance);
	}
}




void calibrate_player_position()
{
	Calibration@ calibration = Calibration();
	calibration.run();	
}
