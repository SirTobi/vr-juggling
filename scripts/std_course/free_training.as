#include "waiter.as"



namespace Tutorial {
namespace Free {

ActionToken GWaitToken;


SceneNode@ ps1;
SceneNode@ ps2;

SceneNode@ ballnode1;
SceneNode@ ballnode2;
SceneNode@ ballnode3;

Ball@ rightBall;
Ball@ rightBall2;
Ball@ leftBall;

int Level = 0;
int Record = 0;
int Target = 4;
int CatchChainLength = 0;


void UpdateText()
{
	if(CatchChainLength > Record)
		Record = CatchChainLength;

	if(Level < 0)
	{
		SetFrontText(CatchChainLength + " (Best: " + Record + ")\nFree Training");

	}else{

		string lvl = "Level " + Level;

		if(CatchChainLength < Target)
			SetFrontText(CatchChainLength + "<" + Target + " (Best: " + Record + ")\n" + lvl);
		else
			SetFrontText(CatchChainLength + ">=" + Target + " (Best: " + Record + ")\n" + lvl);
	}

}

void UpdateLevel()
{
	switch(Level)
	{
	case 0:
		SetAssistanceMode(TAM_Full);
		SetTimeFactor(0.4);
		Target = 4;
		break;

	case 1:
		SetAssistanceMode(TAM_Full);
		SetTimeFactor(0.5);
		Target = 5;
		break;

	case 2:
		SetAssistanceMode(TAM_Full);
		SetTimeFactor(0.6);
		Target = 8;
		break;
	case 3:
		SetAssistanceMode(TAM_Full);
		SetTimeFactor(0.7);
		Target = 11;
		break;

	default:
		SetAssistanceMode(TAM_Full);
		Target = 10000;
		SetTimeFactor(0.7);
		Level = -5;
	}
	CatchChainLength = 0;
	UpdateText();
}



class BallController : IBallController
{
	bool released = false;
	bool startInRightHand = false;

	BallController(bool hand)
	{
		startInRightHand = hand;
	}

	void apply(Ball@ ball, float dt)
	{
		bool rightHand = false;
		if(!released && !ball.inHand(true) && !ball.inHand(false))
		{
			// released
			released = true;
		} else if(released && (ball.inHand(false) || (rightHand = ball.inHand(true))))
		{
			// catched
			released = false;

			// check if other ball is in this hand
			bool otherBallInHand = false;
			if(rightHand == startInRightHand)
			{
				otherBallInHand = true;
			}else if(!(rightBall is ball) && rightBall.inHand(rightHand))
			{
				otherBallInHand = true;
			}else if(!(rightBall2 is ball) && rightBall2.inHand(rightHand))
			{
				otherBallInHand = true;
			}else if(!(leftBall is ball) && leftBall.inHand(rightHand))
			{
				otherBallInHand = true;
			}

			startInRightHand = rightHand;

			if(!otherBallInHand)
			{
				// yahooooo
				ps1.firePS();
				ps2.firePS();
				CatchChainLength++;

				UpdateText();
			}


		}else if(ball.position.y < 0)
		{
			log("lose ball!");
			CloseCustomAction(GWaitToken);
		}
	}
}



void SetupBalls()
{
	log("Setup balls!");

	@rightBall = CreateBall(ballnode1, GJuggleBallRadius);
	@leftBall = CreateBall(ballnode2, GJuggleBallRadius);
	@rightBall2 = CreateBall(ballnode3, GJuggleBallRadius);

	rightBall.addController(BallController(true));
	leftBall.addController(BallController(false));
	rightBall2.addController(BallController(true));

	PlaySound("teleport.ogg");
	leftBall.toHand(!GRightHander);
	rightBall2.toHand(GRightHander);
	rightBall.toHand(GRightHander);
}

void play_intro()
{
	SetDescription("Werfen Sie zunaechst den roten Ball.\nIst dieser am Scheitelpunkt angekommen,\nwerfen Sie den gruenen Ball.\nIst dieser widerum am Scheitelpunkt werfen Sie den Blauen.\nDa Sie den roten inzwischen gefangen haben,\nkoennen Sie diesen wieder werfen,\nwenn der blaue Ball seinen Scheitelpunkt erreicht hat.\n\nWenn Sie fertig sind, melden Sie sich\nbei ihrem Supervisor!");

	SetFrontText("Im letzten Schritt muessen Sie das gelernte zusammenfuegen.");
	WaitSound("speech/free_1.ogg");

	SetFrontText("Wiederholen sie dazu die letzte Uebung.\nHat der blaue Ball dabei seinen Scheitelpunkt\nerreicht, werfen Sie den roten Ball erneut!\nWiederholen Sie dies fuer alle Baelle\nund schon jonglieren Sie.");
	WaitSound("speech/free_2.ogg");
}

bool EndingPlayed = false;
void play_ending()
{
	if(EndingPlayed)
		return;
	EndingPlayed = true;
	SetFrontText("Herzlichen Glueckwunch");
	WaitSound("speech/gratulation.ogg");
	
	SetFrontText("Sie haben das Trainingsprogramm erfolgreich abgeschlossen.\nSie koennen nun noch weiter ueben,\noder das Programm beenden, indem Sie\neinen Mitarbeiter benachrichtigen.\nVersuchen Sie das gelernte in der Realitaet\nanzuwenden. Ueben Sie zuerst mit einem, dann mit zwei\n und erst zum Schluss mit drei Baellen.\n\nViel Glueck");
	WaitSound("speech/free_3.ogg");
	PlaySound("StillAlive.ogg", 40);
}

void run()
{
	log("Enter free training...");
	SetLection("Lektion 5: Jonglieren", "");

	UpdateLevel();
	play_intro();
	UpdateText();


	// create ball nodes
	@ballnode1 = CreateMesh("FreeTraining-JuggleBall-1", "JuggleBallMesh");
	@ballnode2 = CreateMesh("FreeTraining-JuggleBall-2", "JuggleBallMesh");
	@ballnode3 = CreateMesh("FreeTraining-JuggleBall-3", "JuggleBallMesh");
	ballnode1.setMaterialName("Tutorial/RedBall");
	ballnode2.setMaterialName("Tutorial/GreenBall");
	ballnode3.setMaterialName("Tutorial/BlueBall");


	// Create Particle Systems
	@ps1 = CreateParticleSystem("FreeTraining-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("FreeTraining-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));


	while(true)
	{
			SetupBalls();
			CatchChainLength = 0;
			UpdateText();

			GWaitToken = OpenCustomAction();
			WaitUntil(GWaitToken);

			if(CatchChainLength >= Target)
			{
				++Level;
				UpdateLevel();
			}

			DeleteBall(rightBall);
			DeleteBall(leftBall);
			DeleteBall(rightBall2);
			
			if(Level < 0)
			{
				play_ending();
			}
	}

	log("Leave free training...");
}


}}
