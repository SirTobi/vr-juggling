#include "waiter.as"



namespace Tutorial {
namespace Intro {

ActionToken GWaitToken;

class ResetController : IBallController
{

	void apply(Ball@ ball, float dt)
	{
		if(ball.position.y < 0)
		{
			// attach to right hand
			log("Ball hit the ground... reset to right hand!");
			PlaySound("teleport.ogg");
			ball.toHand(GRightHander);
		}
	}
}

void play_intro()
{
	SetDescription("");
	SetFrontText("Willkommen");
	PlaySound("intro.ogg");
	Wait(6000);
	WaitSound("speech/willkommen.ogg", 3000);

	SetFrontText("Ich werde Sie durch dieses Tutorial fuehren\n und Ihnen alle Schritte einzeln erklaeren.");
	WaitSound("speech/intro_1.ogg");
	
	SetFrontText("Versuchen Sie die Aufgaben ohne Nachfragen\nzu loesen. Sollten Sie koerperliche\noder technische Probleme feststellen,\nbenachrichtigen Sie bitte einen Mitarbeiter!");
	WaitSound("speech/intro_1_2.ogg");
	
	SetFrontText("Wissen Sie zwischendurch nicht weiter,\nblicken Sie nach rechts.\n Dort sind alle aktuellen Aufgaben noch einmal erklaert.");
	SetDescription("Hier werden die jeweiligen Aufgaben\nnoch einmal erklaert.");
	WaitSound("speech/intro_2.ogg");
	SetDescription("");
	
	if(GRightHander)
	{
		SetFrontText("Dieser Kurs ist zur Zeit fuer Rechtshaender eingestellt.\nSollte ihre dominante Hand nicht die Rechte sein,\nbenachrichtigen Sie umgehend einen Mitarbeiter.");
		WaitSound("speech/intro_3_r.ogg");
	}else{
		SetFrontText("Dieser Kurs ist zur Zeit fuer Linkshaender eingestellt.\nSollte ihre dominante Hand nicht die Linke sein,\nbenachrichtigen Sie umgehend einen Mitarbeiter.");
		WaitSound("speech/intro_3_l.ogg");
	}

	SetFrontText("Dieser Kurs ist in mehrere Lektionen eingeteilt.\nOben links koennen Sie sehen\nin welcher Lektion sie sich zur Zeit befinden.");
	WaitSound("speech/intro_4.ogg");

	SetFrontText("Am Ende des Kurses sollten Sie die Bewegungsablaeufe\n des Jonglierens beherrschen.");
	WaitSound("speech/intro_5.ogg");

	SetFrontText("Bevor wir mit dem Kurs beginnen,\nnehmen Sie sich kurz Zeit\nund gewoehnen Sie sich an dir VR.");
	WaitSound("speech/intro_6.ogg");

	SetFrontText("Wenn Sie bereit sind,\nheben Sie beide Haende ueber den Kopf.");
	SetDescription("Gewoehnen Sie sich an die VR\nund heben Sie dann\nbeide Haende ueber den Kopf");
	WaitSound("speech/intro_7.ogg");

	WaitForUserInput();
	
	SetDescription("Gewoehnen Sie sich an den Ball\nund heben Sie dann\nbeide Haende ueber den Kopf");
	SetFrontText("In diesem Kurs geht es hauptsaechlich darum,\nBaelle zu werfen. Nehmen Sie diesen Ball und\n versuchen Sie ihn ein paar Mal zu werfen.");

	// create ball
	SceneNode@ ballnode = CreateMesh("TestingBall", "JuggleBallMesh");
	ballnode.setMaterialName("Tutorial/RedBall");
	Ball@ ball = CreateBall(ballnode, GJuggleBallRadius);
	ball.addController(ResetController());
	ball.toHand(GRightHander);
	
	WaitSound("speech/intro_8.ogg");
	

	SetFrontText("Faellt Ihnen der Ball aus der Hand,\nwird er automatisch zurueck in Ihre dominante Hand teleportiert.\n\nBei mehreren Baellen werden die Baelle\nauf Ihre beiden Haende verteilt");
	WaitSound("speech/intro_9.ogg");
	
	SetFrontText("Wenn Sie bereit sind,\nheben Sie beide Haende ueber den Kopf.");
	WaitSound("speech/intro_10.ogg");

	WaitForUserInput();

	DeleteBall(ball);
	ballnode.visible(false);

	SetFrontText("");
	SetDescription("");
}


void run()
{
	log("Enter introduction tutorial...");
	SetLection("Lektion 1: Einfuerung", "1) Willkommen");

	SetAssistanceMode(TAM_None);
	play_intro();

	// create ball
	/*SceneNode@ ballnode = CreateMesh("ThrowBall", "JuggleBallMesh");
	ballnode.setMaterialName("Tutorial/RedBall");
	Ball@ ball = CreateBall(ballnode, GJuggleBallRadius);
	CatchController@ c = CatchController();
	ball.addController(c);
	ball.addController(ResetController(c));
	ball.toHand();

	// Create Particle Systems
	@ps1 = CreateParticleSystem("BallAdaption-ps1", "Tutorial/SuccessParticles");
	@ps2 = CreateParticleSystem("BallAdaption-ps2", "Tutorial/SuccessParticles");
	ps1.position(-2, 4, 0);
	ps2.position(2, 4, 0);
	ps1.lookAt(vector(0, 1, 0));
	ps2.lookAt(vector(0, 1, 0));*/



	log("Leave introduction tutorial...");
}


}}
